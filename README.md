This project serves as repository for the O2 system configuration recipes/playbooks/...
It is based on the the work done in [ALICEDevOps/ali-marathon](https://gitlab.cern.ch/ALICEDevOps/ali-marathon).

Ansible configs should go under `ansible`.
Foreman tooling and documentation should go under `foreman`.
