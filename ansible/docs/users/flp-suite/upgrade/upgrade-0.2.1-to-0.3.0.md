# Upgrading FLP Suite from v0.2.1 to v0.3.0
This page provides instructions on how to upgrade from FLP Suite v0.2.1 to v0.3.0.    
In case you have any questions or require support, please contact us at [alice-o2-flp-support@cern.ch](mailto:alice-o2-flp-support@cern.ch) or in the [ALICE talk forum (O2/FLP category)](https://alice-talk.web.cern.ch/c/o2/flp).

## Known breaking changes
This section describes all known breaking changes that might require user intervention.

### Existing packages

Due to a [known issue](https://alice.its.cern.ch/jira/browse/O2-877) involving the `environment-modules` package provided by CERN CentOS 7 and the way FLP Suite RPM packages are produced, the installation tool ensures that all previous versions of the following packages are removed from the target system before deploying and configuring the new version of FLP Suite:
  - `alisw-coconut`
  - `alisw-Configuration`
  - `alisw-Control` and related
  - `alisw-Monitoring`
  - `alisw-PDA` and `alisw-ReadoutCard`
  - `alisw-QualityControl`
  - `alisw-Readout`

You are free to manually (re)install any version of any of these packages, but be aware that this might cause issues when running FLP Suite software.

#### ReadoutCard: changes in configuration parameters

**roc-config configuration file**   
The parameters specified in the CRU configuration files have been updated to follow the camelCase convention. As a result
previous configuration files are no longer compatible. The previous configuration residing in
`/etc/flp.d/readoutcard/roc_config_startup.cfg` will be backed up in the same directory and replaced with a working version.   
More information can be found on the ReadoutCard github
[doc](https://github.com/AliceO2Group/ReadoutCard/blob/master/README.md#configuration-file) along with an
example of the [new scheme](https://github.com/AliceO2Group/ReadoutCard/blob/master/cru_template.cfg).

**roc-bench-dma argument change**   
The `roc-bench-dma --loopback` argument has been renamed to `--data-source`

**roc-config argument change**   
The `roc-config --config-file` argument has been renamed to `--config-uri`

#### Readout: changes in configuration parameters
There have been some changes in some configuration parameter names.
Existing configuration files might need to be updated accordingly.   
See [Readout release notes](https://github.com/AliceO2Group/Readout/blob/master/doc/releaseNotes.md) between v1.0.9 to v1.1.1.

## Upgrade instructions

### Before upgrading
No actions required.

### Upgrade procedure
To upgrade the FLP Suite from v0.2.1 to v0.3.0, simply follow the [standard installation procedure](../O2_INSTALL_FLP_STANDALONE.md).

### After upgrading

#### ReadoutCard: hugepages configuration
Files `hugeapges-1GiB.conf` and `hugepages-2GiB.conf` under `/etc/flp.d/readoutcard/` will be backed up (in the same directory) and updated, with contents `4` and `128` (corresponding to allocating `4x1GB + 128*2MB` hugepages at boot). 
In case the default values are not compatible with your needs, you will have to:

1. update the default values by
   1. reverting to the backup files (`mv [backup1GB].conf hugepages-1GiB.conf; mv [backup2MB].conf hugepages-2MiB.conf`) OR 
   2. manually editing the files
2. restart the hugepages service (`systemctl restart roc_hugepages_config.service`)
3. reboot.

