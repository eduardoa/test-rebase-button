# Upgrading FLP Suite from v0.1.0 to v0.2.0
This page provides instructions on how to upgrade from FLP Suite v0.1.0 to v0.2.0.  
In case you have any questions or require support, please contact us at [alice-o2-flp-support@cern.ch](mailto:alice-o2-flp-support@cern.ch) or in the [ALICE talk forum (O2/FLP category)](https://alice-talk.web.cern.ch/c/o2/flp).

## Known breaking changes
This section describes all known breaking changes that might require user intervention.

### Existing packages

Due to a [known issue](https://alice.its.cern.ch/jira/browse/O2-877) involving the `environment-modules` package provided by CERN CentOS 7 and the way FLP Suite RPM packages are produced, the installation tool ensures that all previous versions of the following packages are removed from the target system before deploying and configuring the new version of FLP Suite:
  - `alisw-coconut`
  - `alisw-Configuration`
  - `alisw-Control` and related
  - `alisw-Monitoring`
  - `alisw-PDA` and `alisw-ReadoutCard`
  - `alisw-QualityControl`
  - `alisw-Readout`

You are free to manually (re)install any version of any of these packages, but be aware that this might cause issues when running FLP Suite software.


#### AliECS: new git-based workflow configuration

The workflow configuration format has changed with respect to FLP Suite v0.1.0.

Instead of a single YAML file split into two sections, `tasks` and `workflows`, the new mechanism takes advantage of Git for version control, and keeps all configuration information in a directory structure with multiple YAML files, one for each workflow template, and one for each task descriptor.

**If you only used the provided default configuration file with no changes, no intervention is required on your part.** The version of AliECS shipped in FLP Suite v0.2.0 already references [the default configuration repository](https://github.com/AliceO2Group/ControlWorkflows).

**If you made changes** to the provided default configuration file, by modifying existing tasks or workflows, or adding new ones, you will have to **migrate your changes to the new Git-based configuration mechanism**.

This change is conceptually simple, and boils down to:

0. create a Git repository for your configuration;
1. create the directories `tasks` and `workflows` (these sections now become directories instead of YAML nodes);
2. inside each directory, add the contents of the former `tasks` and `workflows` sections as YAML files, one entry each (e.g. if the "readout" task used to be a list item under `tasks` in the old config file, now it becomes a file `readout.yaml` in the `tasks` directory, with the same contents as before);
3. commit and push all of the above to Git;
4. load the `coconut` module (e.g. `aliswmod enter coconut`), and use `coconut repo add <your git repository>` to add your custom configuration repository to your AliECS instance;
5. your workflow and task configuration is now available in the AliECS GUI ("Create" page, see repository picker at the top) and can be used to launch environments.

For an example, see [the default configuration repository](https://github.com/AliceO2Group/ControlWorkflows), which contains a direct port of the former configuration file.

For more information on the AliECS workflow configuration mechanism, see [the `coconut repository` section](https://github.com/AliceO2Group/Control/blob/master/coconut/doc/coconut_repository.md) in [the `coconut` command reference on GitHub](https://github.com/AliceO2Group/Control/blob/master/coconut/doc/coconut.md).

If you require support, please contact us at [alice-o2-flp-support@cern.ch](mailto:alice-o2-flp-support@cern.ch).

#### Quality Control: data queries configuration

Data Sampling and Quality Control have a new way of describing what data should it subscribe to. The following JSON structure in Data Sampling configuration files:
```
  "dataHeaders": [
    {
      "binding": "sum",
      "dataOrigin": "TST",
      "dataDescription": "SUM"
    },
    {
      "binding": "param",
      "dataOrigin": "TST",
      "dataDescription": "PARAM"
    }
  ],
  "subSpec": "2",
```
Can now be simplified to:
```
"query" : "sum:TST/SUM/2;param:TST/PARAM/2",
```
Similarly, when choosing a direct data source for a QC task, this:
```
"dataSource": {
  "type": "direct",
  "binding": "its-rawdata",
  "dataOrigin": "ITS",
  "dataDescription": "RAWDATA",
  "subSpec": "0"
},
```
becomes this:
```
"dataSource": {
  "type": "direct",
  "query" : "its-rawdata:ITS/RAWDATA/0"
},
```
If you use your own QC configuration files, please update them accordingly. All the default configuration files will be updated automatically.

#### Quality Control: Data format and storage 
The QC objects are stored in a different way starting with v0.18. The QC framework 
is, in general, able to read back the data stored previously. However, it has been 
noted that in some cases it fails. It is linked to the change of ROOT version. 
The move to TFile-based storage will avoid
such problem in the future as we store the StreamerInfos along with the data. 

## Upgrade instructions

### Before upgrading
No actions required.

### Upgrade procedure
To upgrade the FLP Suite from v0.1.0 to v0.2.0, simply follow the [standard installation procedure](../O2_INSTALL_FLP_STANDALONE.md).

### After upgrading

#### ReadoutCard: hugepages configuration
Files `hugeapges-1GiB.conf` and `hugepages-2GiB.conf` under `/etc/flp.d/readoutcard/` will be backed up (in the same directory) and updated, with contents `4` and `128` (corresponding to allocating `4x1GB + 128*2MB` hugepages at boot). 
In case the default values are not compatible with your needs, you will have to:

1. update the default values by
   1. reverting to the backup files (`mv [backup1GB].conf hugepages-1GiB.conf; mv [backup2MB].conf hugepages-2MiB.conf`) OR 
   2. manually editing the files
2. restart the hugepages service (`systemctl restart roc_hugepages_config.service`)
3. reboot.

