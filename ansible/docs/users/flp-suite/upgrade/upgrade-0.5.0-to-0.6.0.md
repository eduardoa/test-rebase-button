# Upgrading FLP Suite from v0.5.0 to v0.6.0
This page provides instructions on how to upgrade from FLP Suite v0.5.0 to v0.6.0.    
In case you have any questions or require support, please contact us at [alice-o2-flp-support@cern.ch](mailto:alice-o2-flp-support@cern.ch) or in the [ALICE talk forum (O2/FLP category)](https://alice-talk.web.cern.ch/c/o2/flp).

## Known breaking changes
This section describes all known breaking changes that might require user intervention.

#### ReadoutCard: changes in configuration parameters

All tools now operate on a CRU _endpoint_ with a link range of 0-11.
Using configuration files or parameters with a link mask that goes beyong the 0-11 range will throw an error.

- i.e. 10241:0 -> links 0-11 (serial->10241, endpoint->0)
- i.e. 10241:1 -> links 0-11 (corresponds to 12-23 for the previous versions)

Configuration files and calls with parameters using the previous scheme should be updated accordingly.

#### Existing packages

Due to a [known issue](https://alice.its.cern.ch/jira/browse/O2-877) involving the `environment-modules` package provided by CERN CentOS 7 and the way FLP Suite RPM packages are produced, the installation tool ensures that all previous versions of the following packages are removed from the target system before deploying and configuring the new version of FLP Suite:
  - `alisw-coconut`
  - `alisw-Configuration`
  - `alisw-Control` and related
  - `alisw-Monitoring`
  - `alisw-PDA` and `alisw-ReadoutCard`
  - `alisw-QualityControl`
  - `alisw-Readout`

You are free to manually (re)install any version of any of these packages, but be aware that this might cause issues when running FLP Suite software.

## Upgrade instructions

### Before upgrading
No actions required.

### Upgrade procedure
To upgrade the FLP Suite from v0.5.0 to v0.6.0, simply follow the [standard installation procedure](../O2_INSTALL_FLP_STANDALONE.md).

### After upgrading

#### ReadoutCard: hugepages configuration
Files `hugeapges-1GiB.conf` and `hugepages-2GiB.conf` under `/etc/flp.d/readoutcard/` will be backed up (in the same directory) and updated, with contents `8` and `128` (corresponding to allocating `8*1GB + 128*2MB` hugepages at boot). 
In case the default values are not compatible with your needs, you will have to:

1. update the default values by
   1. reverting to the backup files (`mv [backup1GB].conf hugepages-1GiB.conf; mv [backup2MB].conf hugepages-2MiB.conf`) OR 
   2. manually editing the files
2. restart the hugepages service (`systemctl restart roc_hugepages_config.service`)
3. reboot.

