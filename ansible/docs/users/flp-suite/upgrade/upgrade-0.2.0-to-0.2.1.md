# Upgrading FLP Suite from v0.2.0 to v0.2.1
This page provides instructions on how to upgrade from FLP Suite v0.2.0 to v0.2.1.  
In case you have any questions or require support, please contact us at [alice-o2-flp-support@cern.ch](mailto:alice-o2-flp-support@cern.ch) or in the [ALICE talk forum (O2/FLP category)](https://alice-talk.web.cern.ch/c/o2/flp).

## Known breaking changes
Nothing to report.   

## Upgrade instructions

### Before upgrading
No actions required.

### Upgrade procedure
To upgrade the FLP Suite from v0.2.0 to v0.2.1, simply follow the [standard installation procedure](../O2_INSTALL_FLP_STANDALONE.md).

### After upgrading
No actions required.

