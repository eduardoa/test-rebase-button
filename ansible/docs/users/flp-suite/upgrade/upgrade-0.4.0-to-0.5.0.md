# Upgrading FLP Suite from v0.4.0 to v0.5.0
This page provides instructions on how to upgrade from FLP Suite v0.4.0 to v0.5.0.    
In case you have any questions or require support, please contact us at [alice-o2-flp-support@cern.ch](mailto:alice-o2-flp-support@cern.ch) or in the [ALICE talk forum (O2/FLP category)](https://alice-talk.web.cern.ch/c/o2/flp).

## Known breaking changes
This section describes all known breaking changes that might require user intervention.

  - Custom Quality Control configuration files might not work anymore due to latest changes regards Checkers. The in-built configuration files are updated by overwriting.

### Existing packages

Due to a [known issue](https://alice.its.cern.ch/jira/browse/O2-877) involving the `environment-modules` package provided by CERN CentOS 7 and the way FLP Suite RPM packages are produced, the installation tool ensures that all previous versions of the following packages are removed from the target system before deploying and configuring the new version of FLP Suite:
  - `alisw-coconut`
  - `alisw-Configuration`
  - `alisw-Control` and related
  - `alisw-Monitoring`
  - `alisw-PDA` and `alisw-ReadoutCard`
  - `alisw-QualityControl`
  - `alisw-Readout`

You are free to manually (re)install any version of any of these packages, but be aware that this might cause issues when running FLP Suite software.

## Upgrade instructions

### Before upgrading
No actions required.

### Upgrade procedure
To upgrade the FLP Suite from v0.4.0 to v0.5.0, simply follow the [standard installation procedure](../O2_INSTALL_FLP_STANDALONE.md).

### After upgrading

#### ReadoutCard: hugepages configuration
Files `hugeapges-1GiB.conf` and `hugepages-2GiB.conf` under `/etc/flp.d/readoutcard/` will be backed up (in the same directory) and updated, with contents `4` and `128` (corresponding to allocating `4x1GB + 128*2MB` hugepages at boot). 
In case the default values are not compatible with your needs, you will have to:

1. update the default values by
   1. reverting to the backup files (`mv [backup1GB].conf hugepages-1GiB.conf; mv [backup2MB].conf hugepages-2MiB.conf`) OR 
   2. manually editing the files
2. restart the hugepages service (`systemctl restart roc_hugepages_config.service`)
3. reboot.

#### Custom Quality Control configuration files
If one uses custom QC configuration files, one should update them accordingly to the latest changes regarding Checkers. Before, one would specify which Checks should be applied to the specific Monitor Objects inside QC Tasks' code. Now it is done inside configuration files, just like QC Tasks. Please add the "checks" structure to your config file, as descibred in the [documentation](https://github.com/AliceO2Group/QualityControl/blob/master/doc/ModulesDevelopment.md#check).
As the `CheckRunner`'s device name depends on names of the Checks that it runs, one has to obtain the new name by running such a workflow in standalone mode (DPL driver). Having the new name, one should update it in the workflow and tasks specifications in a ControlWorkflows repository where AliECS downloads it from.

