# List of services running in FLP Suite

This page provides a list of services deployed by the FLP Suite. 

### Data flow services
- `roc_hugepages_config`: Allocates hugepages at boot time.
- `roc_config`: Configures the CRUs at boot time. *Disabled by default.*

### Quality Control services
- `ccdb-sql`: Runs a PostgreSQL database (java process).
- `postgresql-11`: Runs a PostgreSQL database, used as a CCDB-based Quality Control repository to store histograms.
- `o2-qcg`: A web graphical user interface for O2 Quality Control.

### Control services
- `o2-cog`: A web graphical user interface which provides intuitive way of controlling the O2 data taking by communicating with the Control Agent. 
- `mesos-master`: The master component of Mesos, provides cluster resource management facilities to AliECS.
- `mesos-slave`: The slave component of Mesos, runs executors on target machines as instructed by AliECS.
- `o2-aliecs-core`: The core component of AliECS, acts as Mesos scheduler as well as entry point for all cluster control and experiment control operations.
- `zookeeper`: Service registry used by Mesos master for HA leader election.

### Slow Control services
- `o2-alf`: Runs the ALF server in the background.
- `o2-dim-dns`: Runs the DNS service for the DIM nameserver on the flp.
- `o2-dim-webdid`: Runs the DIM WebDID server.

### Infrastructure services
##### Configuration
- `consul`: Key-value store and service discovery mechanism, used as storage for application-specific configuration as well as variable store for AliECS.

##### Logging
- `o2-ilg`: A Web user interface of InfoLogger logging system. 
- `infoLoggerD`: Runs the InfoLogger daemon collecting all messages on the local node.
- `infoLoggerServer`: Runs the InfoLogger daemon collecting centrally the messages sent by remote (and local) infoLoggerD processes.
- `mariadb`: Runs a MySQL database, used by InfoLoggerServer to archive messages.


##### Monitoring
- `grafana-server`: Provides web interface with monitoring dashboards.
- `influxdb`: Time series database.
- `telegraf`: Agent with operating system performance sensors, also includes plugins to collect CRU and CCDB metrics.

##### Misc
- `nginx`: HTTP reverse proxy.
