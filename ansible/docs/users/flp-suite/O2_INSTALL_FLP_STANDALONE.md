# Single node O²/FLP software deployment

#### **IMPORTANT**: This script is now deprecated in favour of `o2-flp-setup`. Please refer to [these instructions](flp-suite-installation.md).

This guide explains how to install the FLP Suite v0.6.0 on a single node setup with a fresh CC7.7 installation.   
For multinode installations, please contact us at [alice-o2-flp-support@cern.ch](mailto:alice-o2-flp-support@cern.ch).

## Table of contents

- [Single node O²/FLP software deployment](#single-node-o%c2%b2flp-software-deployment)
  - [Table of contents](#table-of-contents)
  - [Prerequisites](#prerequisites)
  - [Installing](#installing)
  - [Upgrading](#upgrading)
  - [Quick Guide: Test the FLP deployment](#quick-guide-test-the-flp-deployment)
  - [Quick Guide: Accessing the web-based GUIs](#quick-guide-accessing-the-web-based-guis)
  - [Quick Guide: Using AliECS](#quick-guide-using-aliecs)
  - [Quick Guide: ReadoutCard configuration and Hugepages](#quick-guide-readoutcard-configuration-and-hugepages)
  - [Quick Guide: Readout configuration](#quick-guide-readout-configuration)
  - [Quick guide: Using infoLogger](#quick-guide-using-infologger)
  - [Quick guide: Using monitoring](#quick-guide-using-monitoring)
  - [Quick Guide: Using and configuring QualityControl](#quick-guide-using-and-configuring-qualitycontrol)
  - [Quick Guide: Using ALF](#quick-guide-using-alf)
  - [Troubleshooting](#troubleshooting)
    - [Running behind an HTTP proxy server](#running-behind-an-http-proxy-server)
    - [devtoolset-7 and privilege escalation prompt error](#devtoolset-7-and-privilege-escalation-prompt-error)
    - [AliECS cannot create environment (`DEADLINE_EXCEEDED`)](#aliecs-cannot-create-environment-deadline_exceeded)
    - [AliECS general diagnostics](#aliecs-general-diagnostics)

## Prerequisites

The machine executing the installation script is required to have ansible(2.8.4+) and git installed.

```
yum -y install git python-{cryptography,six,jinja2} PyYAML nmap-ncat epel-release
yum --disablerepo=* --enablerepo=epel -y install ansible
```

If the target machine is behind an **HTTP proxy**, please have a look at [these instructions](#running-behind-an-http-proxy-server).   
If you want to open InfoBrowser on a CERN Cloud target machine, make sure to use the -X option when ssh'ing.   


## Installing

A convenience script is provided to automate single node deployments.
Usage:
```
git clone --branch flp-suite-v0.6.0 https://gitlab.cern.ch/AliceO2Group/system-configuration.git flp-suite-v0.6.0
cd flp-suite-v0.6.0/ansible/utils
./o2-install-flp-standalone <your target hostname> <target username (usually root)>
```

**Important**: for the time being, the only supported user is **`root`**.
For example, to deploy on the host `my-flp-machine`, the command would be
```
./o2-install-flp-standalone my-flp-machine root
```

This will take a while, usually between 10 and 30 minutes depending on circumstances.   

**Important**: a reboot is required after the installation.

After the reboot, the installation script needs to be executed once more to produce a cfg file for readout.exe that is compatible with the CRU(s) of the system. This command is optional if a CRU is not present.
```
/o2-install-flp-standalone my-flp-machine root readout-autoconf
```

## Upgrading

* [Upgrading from v0.5.0 to v0.6.0](flp-suite/upgrade-0.5.0-to-0.6.0.md)
* [Upgrading from v0.4.0 to v0.5.0](flp-suite/upgrade-0.4.0-to-0.5.0.md)
* [Upgrading from v0.3.0 to v0.4.0](flp-suite/upgrade-0.3.0-to-0.4.0.md)
* [Upgrading from v0.2.1 to v0.3.0](flp-suite/upgrade-0.2.1-to-0.3.0.md)
* [Upgrading from v0.2.0 to v0.2.1](flp-suite/upgrade-0.2.0-to-0.2.1.md)
* [Upgrading from v0.1.0 to v0.2.0](flp-suite/upgrade-0.1.0-to-0.2.0.md)

## Quick Guide: Test the FLP deployment
This action should be done only once the installation has finished and reboot of the machine has been performed.

In order to check that components are up and running (e.g services have been started after reboot) a `test-diagnosis` mode has been added to the FLP software deployment. This will run a suite of tests against the installed components and generate a report with the results.

To run this, the command would be:
```
./o2-install-flp-standalone my-flp-machine root test-diagnosis
```

## Quick Guide: Accessing the web-based GUIs

Once the installation finishes, a list of the available web-based GUIs can be found in the target machine (e.g. `http://localhost`).
It includes links to several GUIs for Control, Logging, QC and Monitoring.

## Quick Guide: Using AliECS

The AliECS core and GUI are always running by default (systemd units `o2-aliecs-core` and `cog`).

To access the AliECS GUI, go to the index webpage (e.g. `http://localhost`).

A list of sample workflows (including FairMQ examples, as well as Readout-based `readout-1` and `readout-qc-v0.5.0`) will be listed under **Create** in the left side menu. These workflows are provided in the default configuration, and they can be started as a new environment, which then offers START/STOP/CONFIGURE/RESET buttons.

To start a new environment with the desired workflow:

1) make sure you have the lock (the **lock** icon in the top-left corner must be green, if not, click it);
2) go to **Create** (ENVIRONMENTS section), and select a workflow template (optionally also picking a configuration repository and/or branch);
3) click the **Create** button in the bottom-right corner of the page.

A page shows up with the overview of the newly started environment. You can then use the buttons at the top (section **Control**) to drive the state machine of the environment.

## Quick Guide: ReadoutCard configuration and Hugepages

The `flp-readoutcard` portion of the script will set up hugepage allocation on boot through the `roc_hugepages_config` systemd
service. By default 8 * 1GB and 128 * 2MB hugepages will be allocated.

To check the currently allocated hugepages run `hugeadm --pool-list`

To change the number of hugepages (persistent through reboot):

1) Edit `/etc/flp.d/readoutcard/hugepages-1GiB.conf` and `/etc/flp.d/readoutcard/hugepages-2MiB.conf` accordingly.
2) Restart the systemd service by running `systemctl restart roc_hugepages_config.service`.

## Quick Guide: Readout configuration

The default configuration file for the `readout.exe` binary is installed in `/home/flp/readout.cfg`.

It instantiates a HugePages memory bank, a dummy equipment generating random data, and connection to monitoring.
It also includes disabled examples for CRU equipment and file recording.

More examples can be found in the Readout installation directory (`/opt/alisw/el7/Readout/vX.Y.Z/etc/`).
For information on how to modify this configuration for your purposes, see the [Readout documentation](https://github.com/AliceO2Group/Readout/blob/master/doc/README.md#configuration) and the [Readout configuration parameters reference](https://github.com/AliceO2Group/Readout/blob/master/doc/configurationParameters.md).

## Quick guide: Using infoLogger

After the FLP standalone install, browse the log messages with:
  - Web application called InfoLogger GUI: open the index webpage (e.g. `http://localhost`)
  - Desktop application called infoBrowser (requires X11):
```
/opt/o2-InfoLogger/bin/infoBrowser &
```

When started, it connects immediately to the server collecting logs ('online' mode).
You can test the logging system by adding a message from the command line with e.g.
```
/opt/o2-InfoLogger/bin/log "This is a test message"
```
It should appear in the infoBrowser or InfoLogger GUI.
You can query archived messages:
  - InfoLogger GUI: Just click "Query" button
  - infoBrowser: by exiting the online mode clicking the green 'online' button, and then press the 'query' button.

Further information about infoLogger can be found in the [InfoLogger documentation](https://github.com/AliceO2Group/InfoLogger/blob/master/doc/README.md) and in the [InfoLogger GUI README](https://github.com/AliceO2Group/WebUi/tree/dev/InfoLogger).

## Quick guide: Using monitoring

Monitoring module collects, stores and visualises metrics:
 - Readout: readout rate and size since SOR
 - CRU: temperature, dropped packets, links status
 - CCDB: object size and count
 - system performance: CPU, memory, I/O.

Enter the index webpage (e.g. `http://localhost`) and open a dashboard by clicking on a link under Monitoring section.

In addition, Monitoring module extends the AliECS GUI, providing Readout values (bytes read since startup, current readout rate) and a plot (readout rate) embedded into the environment details page.

## Quick Guide: Using and configuring QualityControl

We use AliECS for this exercise. To start the workflow `readout->QC` do:

0. Open the index webpage (e.g. `http://localhost`) and click on the AliECS GUI link.
1. Make sure you have the lock (the **lock** icon in the top-left corner must be green, if not, click it);
2. Go to **Create** in the ENVIRONMENTS section;
3. Select `readout-qc-v0.5.0` from the list and click on **Create** (optionally also picking a configuration repository and/or branch).
4. In the **Active** environments page, that shows up with the overview of the newly started environment, use the buttons at the top to start the workflow.

To see the objects published by the QC do:
1. Open the index webpage (e.g. `http://localhost`) and click on the Quality Control GUI link.
2. Click on the "Online" button at the top;
3. Expand the tree and select the object you want to display.

The default configuration files for the QC are installed in `/etc/flp.d/qc`. We ship two configuration files:
1. basic.json - used by the workflow o2-qc-run-basic
2. readout.json - used by the workflow o2-qc-run-readout (the workflow launched above). One will typically want to modify the `className` and `moduleName` to use their detector's plugin.

After modifying a config file, do
1. In the Control GUI, "shutdown" the environment.
2. Recreate the environment as specified above.
The task name must not be changed. This is a limitation that will be lifted soon. The module, class or cycle duration
can be modified though.

Please do note that only modules who have been released along with a the QC Framework are available out of the box.
Instructions about developing a QC module on a machine with FLP suite installed can be found [here](https://github.com/AliceO2Group/QualityControl/blob/master/doc/Advanced.md#developing-qc-modules-on-a-machine-with-flp-suite).

More information about QC and its configuration can be found in the documentation hosted in the [QualityControl repo](https://github.com/AliceO2Group/QualityControl) on GitHub.


## Quick Guide: Using ALF

ALF runs as a background systemd unit (`o2-alf`), along with its supporting DIM systemd units (`o2-dim-dns` and `o2-dim-webdid`).

To check the status of the ALF services or send and receive DIM strings, access the DIM WebDID interface from the link in FLP's homepage, or directly at e.g. localhost:2500.

More information about ALF and its services can be found in the [ALF github page](https://github.com/AliceO2Group/ALF).

## Troubleshooting

### Running behind an HTTP proxy server

When deploying in the O² FLP lab in CERN building 4, the previous command must be modified to also pass the proxy settings:
```
ANSIBLE_HTTP_PROXY="http://10.163.32.14:8080" ANSIBLE_HTTPS_PROXY="http://10.163.32.14:8080" ./o2-install-flp-standalone <your target hostname> <target username (usually root)>
```

Proxy settings should be adapted based on the previous example if your setup requires a proxy server.

### devtoolset-7 and privilege escalation prompt error

When running the script for a user, for which `devtoolset-7` is sourced, [this](https://github.com/ansible/ansible/issues/14426) ansible error occurs. To bypass this make sure to
remove the relevant source command, usually in `.bashrc` or `.bash_profile`, and reload the shell before executing the script.

Examples of commands that may trigger this:
```
./o2-install-flp-standalone flp-host
./o2-install-flp-standalone flp-host someuser
```

### AliECS cannot create environment (`DEADLINE_EXCEEDED`)

This is usually caused by a network configuration change which wasn't automatically propagated to the Mesos master ([OCONF-214](https://alice.its.cern.ch/jira/browse/OCONF-214)).

A restart of the Mesos master should fix this:
```
systemctl restart mesos-master
```

If the issue persists, reboot the machine, or perform a more thorough cleanup:
```
pkill -9 -f o2control-executor
systemctl restart zookeeper mesos-{master,slave} o2-aliecs-core cog
```

### AliECS general diagnostics

To check the status of the core and GUI, run the following commands:
```
systemctl status o2-aliecs-core
systemctl status cog
```

If one or both are not running for some reason, they can be started manually as follows:
```
systemctl start o2-aliecs-core
systemctl start cog
```

The GUI should connect to the core within a few seconds. If needed, the GUI can be restarted with `systemctl restart cog`.

All systemctl commands for `o2-aliecs-core` and `cog` (`stop`, `restart`, `status`) work as expected, and the logs are accessible through `journalctl` (e.g. `journalctl -u o2-aliecs-core`).

If you encounter issues, please make sure the following systemd units are running:
* `zookeeper`
* `mesos-master`
* `mesos-slave`
* `o2-aliecs-core`
* `cog`

In case of problems, they can be restarted, in the order in which they are listed above.

It is also possible to directly start the AliECS core without systemd. Like so:
```
source /etc/profile.d/modules.sh && MODULEPATH=/opt/alisw/el7/modulefiles module load Control-Core/v0.11.0-1 && /opt/alisw/el7/Control-Core/v0.11.0-1/bin/o2control-core --coreConfigurationUri file:///etc/aliecs.d/settings.yaml
```

**Important**: Due to a known issue, if the workflow goes into `ERROR`, there might be stale instances of Readout left running. To clean up:
```
pkill -9 -f o2control-executor
```

`coconut`, the O²/FLP **co**ntrol and **con**figuration **ut**ility is included in the deployment. Usage:
```
module load coconut
coconut info
```

For more information on `coconut`, see [the documentation](https://github.com/AliceO2Group/Control/blob/master/coconut/README.md).
