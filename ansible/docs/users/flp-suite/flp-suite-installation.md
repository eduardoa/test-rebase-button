# O²/FLP Suite software installation

This guide explains how to install the FLP Suite v0.7.0 on both single and multi node setups with fresh CC7.7 installations.   

## Table of contents

- [Installing](#installing)
- [Upgrading](#upgrading)
- [Quick Guide: Test the FLP deployment](#quick-guide-test-the-flp-deployment)
- [Quick Guide: Accessing the web-based GUIs](#quick-guide-accessing-the-web-based-guis)
- [Quick Guide: Using AliECS](#quick-guide-using-aliecs)
- [Quick Guide: ReadoutCard configuration and Hugepages](#quick-guide-readoutcard-configuration-and-hugepages)
- [Quick Guide: Readout configuration](#quick-guide-readout-configuration)
- [Quick guide: Using infoLogger](#quick-guide-using-infologger)
- [Quick guide: Using monitoring](#quick-guide-using-monitoring)
- [Quick Guide: Using and configuring QualityControl](#quick-guide-using-and-configuring-qualitycontrol)
- [Quick Guide: Using ALF](#quick-guide-using-alf)
- [Troubleshooting](#troubleshooting)
  - [Running behind an HTTP proxy server](#running-behind-an-http-proxy-server)
  - [devtoolset-7 and privilege escalation prompt error](#devtoolset-7-and-privilege-escalation-prompt-error)
  - [AliECS cannot create environment (`DEADLINE_EXCEEDED`)](#aliecs-cannot-create-environment-deadlineexceeded)
  - [AliECS general diagnostics](#aliecs-general-diagnostics)
  - [Ansible failed to Deploy AliECS configuration](#ansible-failed-to-deploy-aliecs-configuration)

## Installing

The utility `o2-flp-setup` is provided to automate deployments.   
Detailed instructions are available [here](../../../../utils/o2-flp-setup/README.md).   
This will take a while, usually between 10 and 30 minutes depending on circumstances.      
 
**Important**: a reboot is required after the installation. You can use the `reboot` command of `o2-flp-setup`.

## Upgrading

* [Upgrading from v0.6.0 to v0.7.0](upgrade/upgrade-0.6.0-to-0.7.0.md)
* [Upgrading from v0.5.0 to v0.6.0](upgrade/upgrade-0.5.0-to-0.6.0.md)
* [Upgrading from v0.4.0 to v0.5.0](upgrade/upgrade-0.4.0-to-0.5.0.md)
* [Upgrading from v0.3.0 to v0.4.0](upgrade/upgrade-0.3.0-to-0.4.0.md)
* [Upgrading from v0.2.1 to v0.3.0](upgrade/upgrade-0.2.1-to-0.3.0.md)
* [Upgrading from v0.2.0 to v0.2.1](upgrade/upgrade-0.2.0-to-0.2.1.md)
* [Upgrading from v0.1.0 to v0.2.0](upgrade/upgrade-0.1.0-to-0.2.0.md)

## Quick Guide: Test the FLP deployment

**Important**: currently, this only works for single node setups. For multi node setups, the test will report errors because not all services run on the same machine.

This action should be done only once the installation has finished and reboot of the machine(s) has been performed.   
In order to check that components are up and running (e.g services have been started after reboot) a `diagnose` mode has been added to the FLP software deployment. This will run a suite of tests against the installed components and generate a report with the results.
Instructions on how to run this test are available [here](../../../../utils/o2-flp-setup/README.md#diagnosing-a-deployment). 

## Quick Guide: Accessing the web-based GUIs

Once the installation finishes, a list of the available web-based GUIs can be found in the `head` node (e.g. `http://localhost`).
It includes links to several GUIs for Control, Logging, QC and Monitoring.

## Quick Guide: Using AliECS

The AliECS core and GUI are always running by default (systemd units `o2-aliecs-core` and `o2-cog`).

To access the AliECS GUI, go to the index webpage (e.g. `http://localhost`).

A list of sample workflows (`readout` and `readout-stfb`) will be listed under **Create** in the left side menu. These workflows are provided in the default configuration, and they can be started as a new environment, which then offers START/STOP/CONFIGURE/RESET buttons.

To start a new environment with the desired workflow:

1) make sure you have the lock (the **lock** icon in the top-left corner must be green, if not, click it);
2) go to **Create** (ENVIRONMENTS section), and select a workflow template (optionally also picking a configuration repository and/or branch);
3) [Optional] From the "FLP Selection Panel", select your desired FLP machines on which the environment should be created.
   
    If the panel displays a message that consul is not available, use the "Environment Variables" panel to add the FLPs as follows:
    * key: `hosts`
    * value: `[<flp_machine_1>, <flp_machine_2>, ... , <flp_machine_n> ]`
4) click the **Create** button in the bottom-right corner of the page.

A page shows up with the overview of the newly started environment. You can then use the buttons at the top (section **Control**) to drive the state machine of the environment.

## Quick Guide: ReadoutCard configuration and Hugepages

The `flp-readoutcard` portion of the script will set up hugepage allocation on boot through the `roc_hugepages_config` systemd
service. By default 8 * 1GB and 128 * 2MB hugepages will be allocated.

To check the currently allocated hugepages run `hugeadm --pool-list`

To change the number of hugepages (persistent through reboot):

1) Edit `/etc/flp.d/readoutcard/hugepages-1GiB.conf` and `/etc/flp.d/readoutcard/hugepages-2MiB.conf` accordingly.
2) Restart the systemd service by running `systemctl restart roc_hugepages_config.service`.

## Quick Guide: Readout configuration

The default configuration file for the `readout.exe` binary is installed in `/home/flp/readout.cfg`.

It instantiates a HugePages memory bank, a dummy equipment generating random data, and connection to monitoring.
It also includes disabled examples for CRU equipment and file recording.

More examples can be found in the Readout installation directory (`/opt/alisw/el7/Readout/vX.Y.Z/etc/`).
For information on how to modify this configuration for your purposes, see the [Readout documentation](https://github.com/AliceO2Group/Readout/blob/master/doc/README.md#configuration) and the [Readout configuration parameters reference](https://github.com/AliceO2Group/Readout/blob/master/doc/configurationParameters.md).

## Quick guide: Using infoLogger

After the FLP standalone install, browse the log messages with:
  - Web application called InfoLogger GUI: open the index webpage (e.g. `http://localhost`)
  - Desktop application called infoBrowser (requires X11):
```
/opt/o2-InfoLogger/bin/infoBrowser &
```

When started, it connects immediately to the server collecting logs ('online' mode).
You can test the logging system by adding a message from the command line with e.g.
```
/opt/o2-InfoLogger/bin/log "This is a test message"
```
It should appear in the infoBrowser or InfoLogger GUI.
You can query archived messages:
  - InfoLogger GUI: Just click "Query" button
  - infoBrowser: by exiting the online mode clicking the green 'online' button, and then press the 'query' button.

Further information about infoLogger can be found in the [InfoLogger documentation](https://github.com/AliceO2Group/InfoLogger/blob/master/doc/README.md) and in the [InfoLogger GUI README](https://github.com/AliceO2Group/WebUi/tree/dev/InfoLogger).

## Quick guide: Using monitoring

Monitoring module collects, stores and visualises metrics:
 - Readout: readout rate and size since SOR
 - CRU: temperature, dropped packets, links status
 - CCDB: object size and count
 - system performance: CPU, memory, I/O.

Enter the index webpage (e.g. `http://localhost`) and open a dashboard by clicking on a link under Monitoring section.

In addition, Monitoring module extends the AliECS GUI, providing Readout values (bytes read since startup, current readout rate) and a plot (readout rate) embedded into the environment details page.

## Quick Guide: Using and configuring QualityControl

**There is currently a Protobuf dynamic linking issue, brought in by a QC dependency which prevents QC FairMQ devices from loading the OCC plugin. For this reason, tests with AliECS and QC are suspended with FLP Suite v0.7.0.**

We use AliECS for this exercise. To start the workflow `readout->QC` do:

0. Open the index webpage (e.g. `http://localhost`) and click on the AliECS GUI link.
1. Make sure you have the lock (the **lock** icon in the top-left corner must be green, if not, click it);
2. Go to **Create** in the ENVIRONMENTS section;
3. Select `readout-qc` from the list and click on **Create** (optionally also picking a configuration repository and/or branch).
4. In the **Active** environments page, that shows up with the overview of the newly started environment, use the buttons at the top to start the workflow.

To see the objects published by the QC do:
1. Open the index webpage (e.g. `http://localhost`) and click on the Quality Control GUI link.
2. Click on the "Online" button at the top;
3. Expand the tree and select the object you want to display.

The default configuration files for the QC are installed in `/etc/flp.d/qc`. We ship two configuration files:
1. basic.json - used by the workflow o2-qc-run-basic
2. readout.json - used by the workflow o2-qc-run-readout (the workflow launched above). One will typically want to modify the `className` and `moduleName` to use their detector's plugin.

After modifying a config file, do
1. In the Control GUI, "shutdown" the environment.
2. Recreate the environment as specified above.
The task name must not be changed. This is a limitation that will be lifted soon. The module, class or cycle duration
can be modified though.

Please do note that only modules who have been released along with a the QC Framework are available out of the box.
Instructions about developing a QC module on a machine with FLP suite installed can be found [here](https://github.com/AliceO2Group/QualityControl/blob/master/doc/Advanced.md#developing-qc-modules-on-a-machine-with-flp-suite).

More information about QC and its configuration can be found in the documentation hosted in the [QualityControl repo](https://github.com/AliceO2Group/QualityControl) on GitHub.


## Quick Guide: Using ALF

ALF runs as a background systemd unit (`o2-alf`), along with its supporting DIM systemd units (`o2-dim-dns` and `o2-dim-webdid`).

To check the status of the ALF services or send and receive DIM strings, access the DIM WebDID interface from the link in FLP's homepage, or directly at e.g. localhost:2500.

More information about ALF and its services can be found in the [ALF github page](https://github.com/AliceO2Group/ALF).

## Troubleshooting

### Running behind an HTTP proxy server

When running behind an HTTP proxy server (like in CR1 or in the O² FLP lab in CERN building 4), this information must be passed on to `o2-flp-setup`.   
Instructions on how to run this test are available [here](../../../../utils/o2-flp-setup/README.md#performing-a-deployment).   
Proxy settings should be adapted based on the previous example if your setup requires a proxy server.

### devtoolset-7 and privilege escalation prompt error

When running the script for a user, for which `devtoolset-7` is sourced, [this](https://github.com/ansible/ansible/issues/14426) ansible error occurs. To bypass this make sure to
remove the relevant source command, usually in `.bashrc` or `.bash_profile`, and reload the shell before executing the script.

Examples of commands that may trigger this:
```
./o2-install-flp-standalone flp-host
./o2-install-flp-standalone flp-host someuser
```

### AliECS cannot create environment (`DEADLINE_EXCEEDED`)

This is usually caused by a network configuration change which wasn't automatically propagated to the Mesos master ([OCONF-214](https://alice.its.cern.ch/jira/browse/OCONF-214)).

A restart of the Mesos master should fix this:
```
systemctl restart mesos-master
```

If the issue persists, reboot the machine, or perform a more thorough cleanup:
```
pkill -9 -f o2control-executor
systemctl restart zookeeper mesos-{master,slave} o2-aliecs-core o2-cog
```

### AliECS general diagnostics

To check the status of the core and GUI, run the following commands:
```
systemctl status o2-aliecs-core
systemctl status o2-cog
```

If one or both are not running for some reason, they can be started manually as follows:
```
systemctl start o2-aliecs-core
systemctl start o2-cog
```

The GUI should connect to the core within a few seconds. If needed, the GUI can be restarted with `systemctl restart o2-cog`.

All systemctl commands for `o2-aliecs-core` and `o2-cog` (`stop`, `restart`, `status`) work as expected, and the logs are accessible through `journalctl` (e.g. `journalctl -u o2-aliecs-core`).

If you encounter issues, please make sure the following systemd units are running:
* `zookeeper`
* `mesos-master`
* `mesos-slave`
* `o2-aliecs-core`
* `o2-cog`

In case of problems, they can be restarted, in the order in which they are listed above.

It is also possible to directly start the AliECS core without systemd. Like so:
```
source /etc/profile.d/modules.sh && MODULEPATH=/opt/alisw/el7/modulefiles module load Control-Core/v0.13.3-1 && /opt/alisw/el7/Control-Core/v0.13.3-1/bin/o2control-core --coreConfigurationUri file:///etc/aliecs.d/settings.yaml
```

**Important**: Due to a known issue, if the workflow goes into `ERROR`, there might be stale instances of Readout left running. To clean up:
```
pkill -9 -f o2control-executor
```

`coconut`, the O²/FLP **co**ntrol and **con**figuration **ut**ility is included in the deployment. Usage:
```
module load coconut
coconut info
```

For more information on `coconut`, see [the documentation](https://github.com/AliceO2Group/Control/blob/master/coconut/README.md).

### Ansible failed to Deploy AliECS configuration

The following errors are caused by a known template issue on Ansible `2.9.0` that was fixed in later versions.
```
msg: An unhandled exception occurred while running the lookup plugin 'template'. Error was a <class 'ansible.errors.AnsibleError'>, original message: An unhandled exception occurred while templating '{{ hostvars[groups['head'][0]]['ansible_hostname'] }}'. Error was a <type 'exceptions.AttributeError'>, original message: 'VariableManager' object has no attribute '_loader'

or 

The task includes an option with an undefined variable. The error was: 'ansible.vars.hostvars.HostVars object' has no attribute '<head-host-machine>'

The error appears to be in '~/.local/share/o2-flp-setup/system-configuration/ansible/roles/control-core/tasks/main.yml': line 101, column 3, but may
be elsewhere in the file depending on the exact syntax problem.

The offending line appears to be:


- name: Deploy AliECS configuration
  ^ here
```

The best way to overcome this issue is to update to a later version of Ansible.

```
# CentOS
sudo yum remove ansible
sudo yum --disablerepo=* --enablerepo=epel -y install ansible

# macOS
brew upgrade ansible
``` 