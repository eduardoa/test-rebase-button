Testing/Diagnosis deployed role
===

The purpose of this role is to run checks that all roles were installed, configured and launched correctly. This can be used in two scenarios:
* Within our CI pipeline to check that new merge requests will not brake existing/new functionality.
* As a `diagnosis role` in which the users could quickly run a command and receive feedback on which components were not installed/configured/launched correctly. 
  
Preparation
---

1. Create a `[purpose]-test.yml` file in the `test` role, `tasks` directory. (e.g For GUIs we have `gui-test.yml`)
2. Update the `main.yml` file from `test/tasks` directory to include your tests as well

```yaml
---
- name: Is run-test mode enabled {{ test | default(false) }}
  include_tasks: "{{ item }}"
  with_items:
    - gui-test.yml
    - [purpose]-test.yml # Add your new test file as a new item to this list 
  when: test | default(false) | bool
  tags: test


```
3. Define checks in the your newly created `tasks/[purpose]-test.yml` file
4. Execute tests by running:
   * `ansible-playbook` with the `test-diagnosis` tag set `--tags test-diagnosis` and the `test` variable set to `true`
      ```
      ansible-playbook --tags test-diagnosis -e test=true ...
      ```
   * or by using `--with-tests` as an argument to our `o2-install-flp-standalone` script;
      ```
      /ansible/utils/o2-install-flp-standalone [host-machine] root test-diagnosis
      ```



Testing strategy
---
Tests will have to be written in blocks which will have `ignore_errors: true` so that a failing check would not stop the entire test role execution. Because of this a global check is added at the end of the test role. This will check that all tests passed successfully. If that is not the case, an array containing all problematic components will be shown to the user with instructions on what to do further. 
```
fatal: 1/1 [demo-flp]: FAILED! => {"changed": false, "msg": "Errors were encountered while running the final checks. The following components were not installed correctly: ['ControlGUI']. Please copy the logs above and ask for support from the FLP Team"}
```
This final check makes use of a global array fact `components_with_error` which is being updated every time there is a failing test. 

Defining tests
---
The `[purpose]-test.yml` file is ordinary Ansible task file. 

Tests should be written in a manner that sufficient feedback would be provided in `diagnosis role` so that we would understand were the problem is without having to re-run the installation roles. 

As an example, the following code verifies that a HTTP request sent to an application port returns code 200 and the returned body contains "Control - Alice O2 UI" phrase and stores the response in a `result` variable. 

If the registered result is not passing the criteria then two tasks are executed:
* One that adds the component name to `components_with_errors` so that it will be displayed at the end of the test role;
* One that will fail the block with feedback on why this might have happened. Remember to use: `ignore_errors: true` so that the block will not fail the whole test role; 

```yaml
---
- name: Test 'Control GUI' component
  block:
    - name: Ping 'Control GUI' and register the 'result'
      uri:
        url: "http://{{ ansible_fqdn }}:{{ cog_http_port }}"
        method: GET
        return_content: yes
      register: result
    - name: Check that the 'Control GUI result' is passing all checks
      block:
        - name: Add 'ControlGUI' to 'components_with_errors' fact
          set_fact:
              components_with_error: "{{ components_with_error + [ 'ControlGUI' ] }} " # Adding problematic component to the global fact `components_with_error`
        - name: Check whether the response from 'Control GUI' is correct
          fail:
            msg: 'The response from Control GUI HTTP server was incorrect: `{{result.status}}` instead of `200`'
      when: "'Control - Alice O2 UI' not in result.content or result.status != 200" # Checks for test not to be considered successful 
  ignore_errors: true 
```

Printing Warnings
---

On failure of some checks, it is not necessary that an error has occured. For such cases, it is useful to be able to print a warning for the test role.

To print a warning message the following procedure should be followed.
1. Use the 'set_fact' module to pipe the warning message to the `warn()` filter.
2. Add the component to the `components_with_warning` array, in a similar fashion to the errors.

```yaml
---
- name: "Check that should only print a warning"
  set_fact:
    check_warning_msg: "{{ ('42 is bigger than 2') | warn() }}"
  when: "(42 | int) > (2 | int)"

- name: "Add 'NumberCheck' to 'components_with_warnings' fact"
  set_fact:
    components_with_warning: "{{ components_with_warning + [ 'NumberCheck' ] }}"
  when: "(42 | int) > (2 | int)"
```

Running tests in `CI`
---
Continuous integration always sets `test=true` therefore it always runs the tests after the deployment has been done in a separate GitLab CI task. 

Running tests `locally for developing purposes`
---
If only the tests need to be ran locally for developing purposes the following command can be used.
 ```
      /ansible/utils/o2-install-flp-standalone [host-machine] root test-diagnosis
  ```
