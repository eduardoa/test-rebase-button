# Release process

__This is partially outdated__

The release process is made of several steps, each delivering the software to a different, usually
broader, group of people. One can stop at, and potentially resume from, any step.

Steps 1-4 concern a specific package. Step 5 is needed to release the FLP Suite.

## Step 1 - GitHub
Target : Mostly yourself.

1. Go to your project
2. Click "Releases"
2. Optional : Build the release notes
   1. Click on the number of commits of the last version
   1. Go through the changes and select / summarize the ones important for the users
3. Click "Draft a new release"
4. On the Releases page, click "Draft a new release"
5. Give it a semantic version number
6. Write the release notes here (or copy the ones you prepared earlier)
7. Publish the release
8. Advertise to the users according to the needs of the package

## Step 2 - alidist recipe
Target : Mostly developers

1. Modify the corresponding recipe in the alidist repo (https://github.com/alisw/alidist)
2. Make a PR
3. Wait for tests passing
4. Wait for approval (it should come from FLP people) and merging

## Step 3 - RPMs
Target : End users

Trigger a new build in alijenkins :

1. Go to [https://alijenkins.cern.ch/job/build-any-ib/]. People in egroup alice-o2-flp-prototype should have access to it.
1. Click on "Build with Parameters" on the left menu.
1. Fill in the form:
   1. Architecture : slc7
   1. package_name : O2Suite
   1. override_tags : leave it blank (or use the tag if the alidist recipe is not yet approved)
   1. defaults : o2-dataflow
1. Click "Build". It can take quite some time if dependencies have changed.
1. When the build is successful RPMs will be built shortly after (within minutes).
8. Advertise to the users according to the needs of the package

## Step 4 - Ansible
Target : Detectors with an FLP setup

1. Update the version number in basevars/vars/main.yml in the [repo](https://gitlab.cern.ch/AliceO2Group/system-configuration/tree/master/ansible)
2. Make a PR
3. Wait for approval and merging

## Step 5 - GitLab - FLP Suite version
Target : Detectors with an FLP setup (FLP Suite version)

1. Write Release Notes and add it to ReleaseNotes.md
1. Tag the GitLab [repo](https://gitlab.cern.ch/AliceO2Group/system-configuration)
2. Advertise it on the mailing list alice-o2-flp-info

# GitHub Actions

## Bump version in `alidist` on published release in your repo

This action creates a Pull Request in the `alidist` repo when a new release of a given package is published. The Pull Request modifies an `alidist` recipe in order set a tag field with just released package version.


1. Generate a `repo` scoped GitHub token: https://github.com/settings/tokens

2. Set the following secrets in the settings of your repo (Settings > Secrets)

| Name          |      Value      |
|---------------|-----------------|
| GH_EMAIL      | Your e-mail address (required to associate commit with user) |
| GH_USERNAME   | GitHub username (required to associate commit with user) |
| GH_TOKEN      | Previously created GitHub token  |
| ORG           | Organisation name of `alidist` repo: `alisw` (or your fork) |
| MODULE        | Name of your module, lowercase (as `alidist` recipe filename but without `.sh` extension) |

3. Create a new file in the repo: `.github/workflows/release.yml`

```yaml
name: Release

on:
  release:
    types: [published]

jobs:
  bump_alidist:
    runs-on: ubuntu-18.04
    steps:
      - run: curl -L https://github.com/github/hub/releases/download/v2.12.7/hub-linux-amd64-2.12.7.tgz | tar xz
      - run: |
          git config --global user.email ${{ secrets.GH_EMAIL }}
          git config --global user.name ${{ secrets.GH_USERNAME }}
      - run: git clone git://github.com/${{ secrets.ORG }}/alidist
      - run: |
          cd alidist
          CURRENT_VERSION=`cat ${{ secrets.MODULE }}.sh | grep "tag:" | awk '{print $2}'`
          sed -i "s/${CURRENT_VERSION}/${GITHUB_REF##*/}/g" ${{ secrets.MODULE }}.sh
      - run: |
          cd alidist
          git add .
          git commit -m "Bump ${{ secrets.MODULE }} to ${GITHUB_REF##*/}"
          git push "https://${{ secrets.GH_TOKEN }}@github.com/${{ secrets.ORG }}/alidist" HEAD:refs/heads/${{ secrets.MODULE }}-${GITHUB_REF##*/} -f > /dev/null 2>&1
      - run: |
          cd alidist
          GITHUB_TOKEN=${{ secrets.GH_TOKEN }} ../hub-linux-amd64-2.12.7/bin/hub pull-request -h ${{ secrets.MODULE }}-${GITHUB_REF##*/} -b master -m "Bump ${{ secrets.MODULE }} to ${GITHUB_REF##*/}"
```
4. Push the file using SSH remote

## Trigger `O2Suite` RPM build if your `alidist` recipe is modified

This action triggers `O2Suite` RPM regeneration when a recipe listed in `paths` section is modified.

In order to add your recipe to the `paths` section modify this file: https://github.com/alisw/alidist/blob/master/.github/workflows/jenkins.yml#L7
