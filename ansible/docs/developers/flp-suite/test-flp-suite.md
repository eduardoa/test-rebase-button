# Testing FLP Suite releases 

## Table of contents

  - [Test machine](#test-machine)
  - [How to reinstall the test machine via Foreman](#how-to-reinstall-the-test-machine-via-foreman)
  - [How to keep logs](#how-to-keep-logs)
  - [Test procedure](#test-procedure)
    - [Time estimations](#time-estimations)
    - [Testing scenarios](#testing-strategies)
    - [Testing task description](#testing-task-descriptions)

## Test machines

The servers allocated for testing FLP Suite releases are:

- `flptest-asus.cern.ch`: (Asus ESC4000G4, dual-CPU Intel Xeon Skylake Silver 4112 4 cores, 96 GB RAM)
- `flptest2.cern.ch`: (Dell Poweredge R740, dual-CPU Intel Xeon Cascade Lake 4210 10 cores, 96 GB RAM)

Both are equipped with 1 CRU.   
Tests that don't require a CRU can also be performed on Openstack nodes.

Both machines are connected to GPN.

## How to reinstall the test machines via Foreman

1. Go to [https://aidrefsrv20.cern.ch](https://aidrefsrv20.cern.ch) for the O² lab instance of Foreman. This instance is used for all deployments throughout CERN, except for Point 2 which has a dedicated Foreman server.
2. Login (user: `admin`, password: usual root password)
3. On the top dropdown to the right of "ALICE", select the appropriate location: `Building 4 GPN`
4. On the left sidebar hover item `Hosts` and then click `All Hosts`
5. Tick the checkbox for the server(s) you want to reinstall in the Hosts table
6. In the `Select Action` dropdown (top right), select `Build Hosts`; the chosen hosts are now in "build mode" and by clicking on a host and viewing its `Properties`, the second row reports `Build: Pending installation`
7. Tick the `Reboot now` select box and click on `Submit` **important:** if the **⏻** icon in front of a host is grey and not green, Foreman cannot talk to the machine's IPMI, so the `Reboot now` action won't work; in this case the machine must be rebooted manually **now** (either physically or via the IPMI web interface - iDRAC/ASMB9-iKVM, accessible as `flptest-asus-ipmi` and `flptest2-ipmi`)
8. Once Kickstart finishes on the target machine, the machine reboots and its status in Foreman becomes `Build: Installed`
9. You can wait until SSH port is available again in order to check that the machine has rebooted: `until nc -w 1 -z "<host>" 22; do sleep 1; done`
10. On the left sidebar hover item `Monitor` and then click `Jobs`; a "Run ansible roles" job should be queued right at the top, due to start in a few minutes
11. Once the job has completed, the machine is ready for use; to verify that machine has been reinstalled, check whether date matches: `rpm -qi basesystem | grep Date:`

For more information and screenshots, see
[these detailed Foreman build instructions](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/dev/foreman/docs/ITS_SETUP.md).
The ITS instance is used as example but the instructions are identical for the O² lab.

## Test procedure
Testing before release will be done by team members following below strategies and scenarios

FLP test machines spec:
* OS: Centos 7
* Browser: Firefox >60.0

### Time estimations
* Deployment of FLP suite `~55mins`
  * Rebuild machine through Foreman & PXE boot `~30mins`
  * Deploying FLP suite via our script `~20mins`
  * Rebooting machine and running test diagnosis `~5mins`


### Main Testing scenarios

These test scenarios should be used for the full FLP Suite validation.

##### Full fresh installation test

| Index | Only on | Tasks                                                                                                                       | 
|-------|---------|-----------------------------------------------------------------------------------------------------------------------------|
| 1     |         | [Deployment of FLP suite (NEW VERSION)](flp-test-scenarios.md#1-deployment-of-flp-suite)                                    |
|       | `CRU`   | [Make the transition from dummy generator to CRU](flp-test-scenarios.md#2-make-the-transition-from-dummy-generator-to-cru)  |
|       | `CRU`   | [Test AliECS Environment Creation/Start/Stop GUI](flp-test-scenarios.md#3-test-aliecs-core-using-web-gui)                   |
|       | `CRU`   | [Test AliECS Start/Stop/Start by using COCONUT](flp-test-scenarios.md#4-test-aliecs-core-using-coconut)                     |
|       | `CRU`   | [Test AliECS together with QC GUI](flp-test-scenarios.md#5-testing-aliecs-together-with-qc-gui)                            |
|       |         | [Testing WebDid / ALF Component](flp-test-scenarios.md#6-testing-webdid-alf-component)                                      |

##### Full upgrade test

| Index | Only on | Tasks                                                                                                                       | 
|-------|---------|-----------------------------------------------------------------------------------------------------------------------------|
| 2     |         | [Deployment of FLP suite (PREVIOUS VERSION)](flp-test-scenarios.md#1-deployment-of-flp-suite)                               |
|       |         | [Deployment of FLP suite (NEW VERSION)](flp-test-scenarios.md#1-deployment-of-flp-suite)                                    |
|       | `CRU`   | [Make the transition from dummy generator to CRU](flp-test-scenarios.md#2-make-the-transition-from-dummy-generator-to-cru)  |
|       | `CRU`   | [Test AliECS Environment Creation/Start/Stop GUI](flp-test-scenarios.md#3-test-aliecs-core-using-web-gui)                   |
|       | `CRU`   | [Test AliECS Start/Stop/Start by using COCONUT](flp-test-scenarios.md#4-test-aliecs-core-using-coconut)                     |
|       | `CRU`   | [Test AliECS together with QC GUI](flp-test-scenarios.md#5-testing-aliecs-together-with-qc-gui)                             |
|       |         | [Testing WebDid / ALF Component](flp-test-scenarios.md#6-testing-webdid-alf-component)                                      |


### Quick Testing scenarios

These test scenarios should be used for quick tests, not for a full FLP Suite validation.

##### Quick fresh installation test

| Index | Only on | Tasks                                                                                                              | 
|-------|---------|--------------------------------------------------------------------------------------------------------------------|
| 1     |         | [Deployment of FLP suite (NEW VERSION)](flp-test-scenarios.md#1-deployment-of-flp-suite)                           |
|       |         | [Test AliECS together with QC GUI (dummy generator)](flp-test-scenarios.md#5-testing-aliecs-together-with-qc-gui) |

##### Quick upgrade test

| Index | Only on | Tasks                                                                                                              | 
|-------|---------|--------------------------------------------------------------------------------------------------------------------|
| 2     |         | [Deployment of FLP suite (PREVIOUS VERSION)](flp-test-scenarios.md#1-deployment-of-flp-suite)                      |
|       |         | [Deployment of FLP suite (NEW VERSION)](flp-test-scenarios.md#1-deployment-of-flp-suite)                           |
|       |         | [Test AliECS together with QC GUI (dummy generator)](flp-test-scenarios.md#5-testing-aliecs-together-with-qc-gui) |


### Testing task descriptions
Tasks which are required by each of the testing scenarios from above can be found in a separate [document](flp-test-scenarios.md ).
