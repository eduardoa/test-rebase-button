# FLP test scenarios

## 1. Deployment of FLP suite

| Only | Scenario Steps                                           | Description / command     |
|------|----------------------------------------------------------|---------------------------|
| CRU  | Use Foreman to setup CRU test machine                    | Use Foreman to rebuild the machine from scratch for a clean installation of FLP Suite (to be executed only if we are not testing an upgrade scenario e.g. from 0.1.0 to 0.2.0) |
|      | Use OpenStack to setup CC7 machine                       | Make use of [OpenStack](https://clouddocs.web.cern.ch/tutorial_using_a_browser/README.html) to rebuild/restart/create a new CC7 machine (ignore if using a test machine with a CRU) |
|      | Login to machine as root                                 | `ssh root@<test-machine>` |
|      | Download installer                                       | `bash <(curl -s https://cernbox.cern.ch/index.php/s/kNMAmYaN9l6RaKD/download)` |
|      | Install installer                                        | `o2-flp-setup install`. This will download the Ansible roles to be deployed and requires CERN gitlab access (use your CERN credentials) |
|      | Checkout FLP Suite version to deploy                     | `o2-flp-setup checkout flp-suite-vX.Y.Z`|
|      | Configure Ansible to preserve logs in fil                | `export ANSIBLE_LOG_PATH=/root/flp-suite-deployment.log`|
|      | Deploy FLP Suite                                         | `o2-flp-setup deploy --head <test-machine> --flps <test-machine>`|
|      | Reboot machine                                           | `reboot` |
|      | Login to machine as root                                 | `ssh root@<test-machine>` |
| CRU  | Produce readout.exe cfg file for a CRU                   | `o2-flp-setup deploy --head <test-machine> --flps <test-machine> --modules readout-autoconf`. |
|      | Run test-diagnosis mode                                  | `o2-flp-setup diagnose --head <test-machine> --flps <test-machine>` |
|      | Open base web page of FLP Suite                          | Every FLP Suite installation will contain a web base page containing links to all of the components at `http://<test-machine>` |


## 2. Make the transition from dummy generator to CRU

| Only | Scenario Steps                                           | Description / command         |
|------|----------------------------------------------------------|-------------------------------|
| CRU  | Login to machine as root                                 | `ssh root@<test-machine>`     |
| CRU  | Enter ReadoutCard mode                                   | `aliswmod enter ReadoutCard`  |
| CRU  | List cards                                               | `roc-list-cards`              |
| CRU  | Check listed cards                                       | From the listed cards, check that at least one card was printed; the card with index `0` will be used for the tests. |
| CRU  | Open `readout.cfg` file                                  | `vi /home/flp/readout.cfg`    |
| CRU  | Disable dummy generator from config file                 | Under `equipment-dummy-1`, change the "enabled" field from 1 to 0  |
| CRU  | Enable CRU readoutcard from config file                  | Under `equipment-rorc-1`, change the "enabled" field from 0 to 1  |
| CRU  | Change "cardId"                                          | Under `equipment-rorc-1` check that the field "cardID" is set to sequence ID `#0`, if not set it (`cardID=#0`) |
| CRU  | Save changes and close editor                            |   |
| CRU  | Configure the CRU                                        | Run `roc-config` command to configure the readout card `#0`, the card enabled in `readout.cfg`: `roc-config --id=#0 --gbtmux=ddg --links=0 --clock=local --loopback --datapathmode=continuous --force` |
| CRU  | Print CRU configuration                                  | Run `roc-status` command for readout card `#0`, the card enabled in `readout.cfg`: `roc-status --id=#0`  |
| CRU  | Check CRU configuration                                  | In the output of `roc-status` command, confirm that "link" "0" is "enabled" and has "GBT MUX" set as "DDG" |
| CRU  | Exit ReadoutCard mode                                    | `exit`  |


## 3. Test AliECS Environment Creation/Start/Stop GUI
**For all actions on an environment, check that the top left "lock" button is pressed on the AliECS GUI !**

| Only | Scenario Steps                                                        | Description / command    |
|------|-----------------------------------------------------------------------|--------------------------|
|      | **Creating an Environment through AliECS GUI**                        |                          |
|      | Open base web page of FLP Suite                                       | URL: `http://<test-machine>` |
|      | Open tab for AliECS GUI                                               | From category "Control" select "AliECS GUI" in a new tab |
|      | Open tab for InfoLogger GUI                                           | From category "Logging" select "InfoLogger GUI" in a new tab |
|      | Set filters in InfoLogger GUI                                         | Set the following filters on the filter boxes at the top: Facility = `readout`   |
|      | Enter "Live" mode in InfoLogger GUI                                   | From the top left side of InfoLogger GUI, click "Live" mode\. This will allow you to watch all the logs while performing actions on other FLP Components |
|      | On AliECS GUI, take the top left "lock"                               | On the AliECS GUI, check that the top left "lock" button is pressed. If not, click on it. |
|      | Navigate to "\+ Create"                                               | Click on the sidebar item "\+ Create" |
|      | Fill in "Select Template" for a new environment                       | Use the already selected values for "Repository" and "Revision" and select the template `readout`.|
|      | Select the FLP Machines                                               | Select the machines on which the environment should be deployed by clicking on the name of the FLP. (If "Select Template" section displays an error message, follow the instructions to add the hosts by using the environment variables such as: `key`: `hosts` and `value`: `[ <test-machine> ]`) |
|      | Create the new environment                                            | Click "Create" and you should be redirected to a new page which contains your newly created environment. |
|      | Check InfoLogger GUI Logs                                             | After creating the environment, go to InfoLogger GUI which is running in Live mode\. Check that logs regarding creating a new environment appeared in the table, in particular a message like `Readout executing CONFIGURE` |
|      | **Starting an Environment from AliECS GUI**                           | |
|      | Start Environment after Creation                                      | After the environment was successfully created, you should have been automatically redirected to the environment page\. If not, click on "Active" item from the left sidebar in AliECS GUI, select your environment and then click the green "START" button" |
|      | Check AliECS Run Number                                               | On the AliECS GUI, same environment page, check that the run number component is one \(1\) or \(2\) in second interation |
|      | Check InfoLogger GUI Logs                                             | After starting the environment, go to InfoLogger GUI which is running in Live mode\. Check that logs regarding starting the environment appeared in the table, in particular a message like `Readout executing START` |
|      | **Send trigger to CRU**                                               | |
| CRU  | Login to machine as root                                              | `ssh root@<test-machine>`     |
| CRU  | Enter ReadoutCard mode by running:                                    | `aliswmod enter ReadoutCard` |
| CRU  | Send trigger to CRU by running the following for readout card `#0`, the card enabled in readout\.cfg | `roc-ctp-emulator --id=#0 --trigger-mode continuous` |
|      | **Check AliECS panels are updating**                                  | On the AliECS GUI, same environment page, check that the 3 status panels are updating with data every 5s |
|      | **Check Grafana is adding data in the plots**                         | |
|      | Check System Monitoring Dashboard                                     | Open web base page and from category "Monitoring", click System Monitoring Dashboard and check data is flowing through the dashboard's plots |
|      | Check Readout Dashboard                                               | Open web base page and from category "Monitoring", click Readout Dashboard and check data is flowing through the dashboard's plots |
| CRU  | Check CRU Dashboard                                                   | Open web base page and from category "Monitoring", click CRU Dashboard and check data is flowing through the dashboard's plots |
|      | **Stoping an Environment from GUI**                                   | |
|      | Stop environment from AliECS GUI                                      | From the environment which has been created page, click the red "STOP" button to stop the environment |
|      | Confirm state of environment: CONFIGURED                              | On the AliECS GUI, check that the state of the environment is now "CONFIGURED" |
|      | Check AliECS GUI                                                      | Confirm "Run Number" and "Panels" disappeared from the AliECS GUI environment page |
|      | Check InfoLogger GUI Logs                                             | After stoping the environment, go to InfoLogger GUI which is running in Live mode\. Check that logs regarding stoping the environment appeared in the table, in particular a message like `Readout executing STOP` |
|      | **Start Environment again from AliECS GUI and repeat all the steps**  | Repeat steps from "Starting an Environment from AliECS GUI" and make sure that run number increased in both AliECS and InfoLogger GUIs (**Hint**: in the InfoLogger GUI, click on the box `Run` at the top to toggle this field's visibility) |
|      | **After 2 runs: Shutdown environment from GUI**                       | |
|      | Go to environments page (Active)                                      | Click on the "Active" sidebar item from AliECS GUI you will be able to access all environments page. |
|      | Shutdown environment from GUI	                                       | Click the "ShutDown" red button from the table inline with the environment. |

## 4. Test AliECS Start/Stop/Start by using COCONUT
**If you wish to check the status of AliECS in the GUI after performing a `coconut` operation, be sure to refresh and reopen the details page of the relevant environment.**

| Only | Scenario Steps                                                                                            | Description / command    |
|------|-----------------------------------------------------------------------------------------------------------|--------------------------|
|      | **Creating a new Environment**                                                                            |                          |
|      | Login to machine as root                                                                                  | `ssh root@<test-machine>`|
|      | Load coconut go module                                                                                    | `module load coconut`    |
|      | Check there are no environments created                                                                   | `coconut env list`       |
|      | Create a new `readout` environment by running                                                             | `coconut env create -w readout -e '{"hosts":["target-hostname"]}'` |
|      | Confirm creation of new environment was successful                                                        | Check that the output of the create command confirms that a new environment was created|
|      | Confirm environment state: CONFIGURED                                                                     | Check that the output of the create command confirms that the new environment is in state: CONFIGURED|
|      | Copy environment ID                                                                                       | From the output of the create command copy and save the environment id |
|      | Open base page and access AliECS GUI                                                                      | Open web base page and from category "Control" select "AliECS GUI" in a new tab\. Click on the left sidebar item "Active" and confirm your environment is displayed in the table with the same id |
|      | **Starting an Environment from Coconut**                                                                  |                          |
|      | Start the created environment by replacing `<saved_env_id>` with the id saved earlier                     | `coconut env control <saved_env_id> -e START_ACTIVITY` |
|      | Confirm action was successful                                                                             | Check that the output of the start command confirms that the transition was completed |
|      | Confirm environment state: RUNNING                                                                        | Check that the output of the start command confirms that the new environment is in state: RUNNING |
|      | Confirm environment run number is `3` in first interation or `4` in second interation                     | Check that the output of the start command confirms that the run number is three \(3\) |
|      | Confirm AliECS Run Number in GUI                                                                          | On the AliECS GUI, check that the run number component is three \(3\) |
| CRU  | **Send trigger to CRU**                                                                                   |                          |
| CRU  | Enter ReadoutCard mode by running:                                                                        | `aliswmod enter ReadoutCard` |
| CRU  | Send trigger to CRU by running the following for readout card `#0`, the card enabled in `readout.cfg` | `roc-ctp-emulator --id=#0 --trigger-mode continuous`. |
|      | **Check AliECS Graphs are updating**                                                                      | On the AliECS GUI, same environment page, check that the 3 status panels are updating with data every 5s |
|      | **Check Grafana is updating data**                                                                        |                          |
|      | Check System Monitoring Dashboard                                                    | Open web base page and from category "Monitoring", click System Monitoring Dashboard and check data is flowing through the dashboard's plots |
|      | Check Readout Dashboard                                                              | Open web base page and from category "Monitoring", click Readout Dashboard and check data is flowing through the dashboard's plots           |
|      | Look for a drop in Readout Dashboard data plots                                      | Due to the fact that environment has been stopped and started again, readout dashboard should have a drop in data                            |
| CRU  | Check CRU Dashboard                                                                  | Open web base page and from category "Monitoring", click CRU Dashboard and check data is flowing through the dashboard's plots               |
|      | **Stop Environment from Coconut**                                                    | |
|      | Stop the running environment by replacing `<saved_env_id>` with the id saved earlier | `coconut env control <saved_env_id> -e STOP_ACTIVITY`                                                                                        |
|      | Confirm action was successful                                                        | Check that the output of the stop command confirms that the transition was completed                                                         |
|      | Confirm environment state: CONFIGURED                                                | Check that the output of the stop command confirms that the new environment is in state: CONFIGURED                                          |
|      |  Confirm environment run number is "none"                                            | Check that the output of the stop command confirms that the run number is none                                                               |
|      | Check AliECS GUI                                                                     | Refresh AliECS GUI and confirm "Run Number" and "Panels" disappeared from the AliECS GUI                                                      |
|      | **Repeat all above steps from: "Starting an Environment" onward**                    | Make sure that at the end of the procedure the run number changes to four (4)                                                                |
|      | **Shutdown ENV from Coconut**                                                        | |
|      | Shutdown the configured environment by replacing `<saved_env_id>` with the id saved earlier | `coconut env destroy <saved_env_id>`                                                                                             |
|      | Confirm action was successful                                                               | Check that the output of the destroy command confirms that the teardown was complete                                             |
|      | Check AliECS GUI                                                                            | On the AliECS GUI, click on the left sidebar item "Active" and confirm environment is not part of the table anymore              |
|      | **Check InfoLogger GUI Logs**                                                               | |
|      | Open base page and access InfoLogger GUI                                                    | Open web base page and from category "Logging" select "InfoLogger GUI" in a new tab                                              |
|      | Enable debug messages                                                                       | Click on the "Debug" button at the top |
|      | Set filters in InfoLogger GUI                                                               | Set the following filters on the filter boxes at the top: System = `ECS`, Message = `%updating state%` |
|      | Use InfoLogger in Query mode                                                                | By using the filters from InfoLogger GUI, query the logs for all the action performed from Coconut \(create/start/stop/destroy\). You should see messages like `... updating state = CONFIGURED \| RUNNING \| STANDBY \| DONE` |
|      | Login to machine as root with `-X` flag                                                     | `ssh root@<test-machine> -X`     |
|      | Open InfoBrowser from the terminal by running:                                              | `/opt/o2-InfoLogger/bin/infoBrowser &` |
|      | Use InfoBrowser in Query mode                                                               | By using the filters from InfoBrowser (see instructions for InfoLogger GUI), query the logs for all the action performed from Coconut \(create/start/stop/destroy\) (again, see instructions for InfoLogger GUI)   |


## 5. Testing AliECS together with QC GUI
**For all actions on an environment, check that the top left "lock" button is pressed on the AliECS GUI !**

| Only | Scenario Steps                                                   | Description / command    |
|------|------------------------------------------------------------------|--------------------------|
|      | **Check Consul instance**                                        |                          |
|      | Open base web page of FLP Suite                                  | URL: `http://<test-machine>` |
|      | Open tab for Consul GUI                                          | From category "Quality Control" open Consul" in a new tab |
|      | Confirm there are no services                                    | Check that Consul is accessible and contains no services excepts itself \(consul\) |
|      | **Creating an Environment through AliECS GUI**                   | |
|      | Open tab for AliECS GUI                                          | Open web base page and from category "Control" select "AliECS GUI" in a new tab |
|      | Open tab for InfoLogger GUI                                      | Open web base page and from category "Logging" select "InfoLogger GUI" in a new tab |
|      | Enable debug messages                                            | Click on the "Debug" button at the top |
|      | Set filters in InfoLogger GUI                                    | Set the following filters on the filter boxes at the top: System = `ECS`, Message = `%updating state%` |
|      | Enter "Live mode" in InfoLogger GUI                              | From the top left side of InfoLogger GUI, click "Live mode". This will allow you to watch all the logs while performing actions on other FLP Components |
|      | On AliECS GUI, take the top left "lock"                          | On the AliECS GUI, check that the top left "lock" button is pressed. If not, click on it. 
|      | Navigate to "+ Create"                                           | Click on the sidebar item "\+ Create" |
|      | Fill in "Select Template" for a new environment                  | Use the already selected values for "Repository" and "Revision" and select the template `readout-stfb`.|
|      | Select the FLP Machines                                          | Select the machines on which the environment should be deployed by clicking on the name of the FLP. (If "Select Template" section displays an error message, follow the instructions to add the hosts by using the environment variables such as: `key`: `hosts` and `value`: `[ <test-machine> ]`) |
|      | Create the new environment                                       | Click "Create" and you should be redirected to a new page which contains your newly created environment. |
|      | **Check CCDB instance**                                          | |
|      | Open base page and access CCDB GUI                               | Open web base page and from category "Quality Control" open "CCDB browser" in a new tab |
|      | Confirm there are no objects stored as of now in CCDB            | |
|      | **Check Consul instance**                                        | |
|      | Open base page and access Consul GUI                             | Open web base page and from category "Quality Control" open Consul" in a new tab |
|      | Confirm that "Tags" column is empty for each service             | Check that Consul is accessible and contains 2 services \(consul and another one\) |
|      | **Test QC GUI**                                                  | |
|      | Open base page and navigate to QC GUI                            | Open web base page and from category "Quality Control" select "QC GUI" in a new tab |
|      | Create a new layout                                              | From the left sidebar of QCGUI, click "\+ New Layout" to create a new layout |
|      | Save new layout                                                  | After creation, from top right click the blue check button to save the layout |
|      | **Starting an Environment from AliECS GUI**                      | |
|      | Start Environment after Creation                                 | After the environment was successfully created, you should have been automatically redirected to the environment page\. If not, click on "Active" item from the left sidebar in AliECS GUI, select your environment and then click the green "START" button |
|      | Check AliECS Run Number                                          | On the AliECS GUI, same environment page, check that the run number component is five \(5\) |
|      | Check InfoLogger GUI Logs                                        | After starting the environment, go to InfoLogger GUI which is running in Live mode\. Check that logs regarding starting the environment appeared in the table. You should see messages like `... updating state = RUNNING` |
| CRU  | **Send trigger to CRU**                                          | |
|      | Login to machine as root                                         | `ssh root@<test-machine>`|
| CRU  | Enter ReadoutCard mode by running:                               | `aliswmod enter ReadoutCard` |
| CRU  | Send trigger to CRU by running the following for readout card `#0`, the card enabled in `readout.cfg` | `roc-ctp-emulator --id=#0 --trigger-mode continuous`. |
|      | **Check AliECS Graphs are updating**                             | On the AliECS GUI, same environment page, check that the 3 status panels are updating with data every 5s |
|      | **Check Grafana is adding data in the plots**                    | |
|      | Check System Monitoring Dashboard                                | Open web base page and from category "Monitoring", click System Monitoring Dashboard and check data is flowing through the dashboard's plots |
|      | Check Readout Dashboard                                          | Open web base page and from category "Monitoring", click Readout Dashboard and check data is flowing through the dashboard's plots |
| CRU  | Check CRU Dashboard                                              | Open web base page and from category "Monitoring", click CRU Dashboard and check data is flowing through the dashboard's plots |
|      | Check CCDB Dashboard                                             | Open web base page and from category "Monitoring", click CCDB Dashboard and check data is flowing through the dashboard's plots |
|      | **Test QC GUI**                                                  | |
|      | Open base page and navigate to QC GUI                            | Open web base page and from category "Quality Control" select "QC GUI" in a new tab\. |
|      | Enter "ONLINE" Mode                                              | Click "ONLINE" button from top left window to enter Online mode |
|      | Navigate to Objects sidebar item                                 | Click on the left sidebar item and check that new objects appear every x seconds \(interval can be set from the left sidebar\) |
|      | Access the layout created                                        | From the sidebar of QC GUI click on the layout that it has just been created  |
|      | Adde objects to the new layout                                   | Add new objects to the layout and confirm they are being refreshed every x seconds |
|      | **Check CCDB instance**                                          | |
|      | Open base page and access CCDB GUI                               | Open web base page and from category "Quality Control" select "CCDB browser" in a new tab\. |
|      | Confirm there are rows in the tables                             | Confirm there are new entries in the table |
|      | **Check Consul instance**                                        | |
|      | Open base page and access Consul GUI                             | Open web base page and from category "Quality Control" select Consul" in a new tab\. |
|      | Confirm there are services                                       | Check that Consul is accessible and contains services and tags in the services \(except the consul service\)  |
|      | **Stop Environment from Coconut**                                | |
|      | Login to machine as root                                         | `ssh root@<test-machine>`|
|      | Load coconut go module by running:                               | `module load coconut` |
|      | List environments and save the id of the environment you created | `coconut env list` |
|      | Stop the running environment by replacing `<saved_env_id>` with the id saved earlier | `coconut env control <saved_env_id> -e STOP_ACTIVITY` |
|      | Confirm action was successful                                    | Check that the output of the stop command confirms that the transition was completed |
|      | Confirm environment state: CONFIGURED                            | Check that the output of the stop command confirms that the new environment is in state: CONFIGURED |
|      | Confirm environment run number is "none"                         | Check that the output of the stop command confirms that the run number is none |
|      | Check AliECS GUI                                                 | Refresh AliECS GUI and confirm "Run Number" and "Panels" disappeared from the AliECS GUI |
|      | **Repeat all above steps from: "Starting an Environment" onward**| Make sure run number changes to \(6\)          |
|      | **Shutdown environment from GUI**                                | |
|      | Go to environments page \(Active\)                               | Click on the "Active" sidebar item from AliECS GUI you will be able to access all environments page\. |
|      | Shutdown environment from GUI                                    | Click the "ShutDown" red button from the table inline with the environment\.                          |

## 6. Testing WebDid / ALF Component

| Only | Scenario Steps                        | Description / command    |
|------|---------------------------------------|--------------------------|
|      | **Check WebDid displays services**    |                                                                                                |
|      | Open base page of the FLP Suite       | Click on "DID\-DIM Information Display" under category DIM                                       |
|      | Select your  server                   | Once the page loaded, from the left panel select your machine\-name folder, expand it and select the "DIS_DNS" entry           |
|      | Check for services                    | A list of services will appear in the central panel "Services" |
|      | Confirm `SWT_SEQUENCE`                | Confirm among the services there is at least one called "SWT\_SEQUENCE"  (you use browser search: CTRL+F) |
|      | Confirm `REGISTER_READ`               | Confirm among the services there is at least one called "REGISTER\_READ" (you use browser search: CTRL+F) |

