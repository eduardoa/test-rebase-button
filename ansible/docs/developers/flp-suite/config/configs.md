# Configurations inventory

Configuration information can be found in the consul kv store under `/o2/configs`.

## FLPs

Configurations that are specific to the FLPs can be found within `/o2/configs/flps`, under the `short-hostname` of the FLP in
question.

### Card Configuration

For each FLP the parameters to configure a CRU can be found under `/o2/configs/flps/[flp-short-hostname]/cards/[card-id]`,
e.g. `/o2/configs/flps/alio2-cr1-flp139/cards/2`. Directly usable by `roc-config`.

The KV store is populated by ansible during deployment, with default values for each CRU, as part of the `flp-readoutcard` role. The relevant
block can be found [here](https://gitlab.cern.ch/AliceO2Group/system-configuration/-/blob/dev/ansible/roles/flp-readoutcard/tasks/main.yml#L254)

| Key | Description | Example |
|-----|-------------|---------|
| **/o2/configs/flps/[hostname]/cards/[id]** | Top level key in consul. Card ID is the sequence id, as reported by `roc-list-cards` | `"/o2/configs/flps/alio2-cr1-flp139/cards/2"` |
| **cru** | Global parameters to configure the card | _is a key; no value_ |
| cru/**allowRejection** | Allow packet rejection; true or false | `"false"` |
| cru/**clock** | Clock to use; local or CTP | `"local"` |
| cru/**cruId** | The CRU ID; a hex | "0x0" |
| cru/**datapathMode** | Datapath mode; packet or continuous | `"continuous"` |
| cru/**downstreamData** | Downstream data type; CTP, Pattern, or MIDTRG | `"CTP"` |
| cru/**dynamicOffset** | Dynamic offset between DMA pages; true or false | `"true"` |
| cru/**gbtEnabled** | Enable GBT links; true or false | `"true"` |
| cru/**gbtMode** | The GBT mode; GBT or WB | `"GBT"` |
| cru/**loopback** | Enable link loopback; true or false | `"true"` |
| cru/**onuAddress** | The ONU address; an int | `"0"` |
| cru/**ponUpstream** | Enable PON upstream; true or false | `"false"` |
| cru/**triggerWindowSize** | The trigger window size; an int | `"100"` |
| cru/**userLogicEnabled** | Enable the User Logic link; true or false | `"false"` |
| **links** | Parameters applicable to all links | _is a key; no value_ |
| links/**gbtMux** | GBT mux; TTC, DDG, or SWT | `"TTC"` |
| links/**enabled** | Enable the link; true or false | `"true"` |
| **link[id]** | Parameters applicable to link #[id]. Optional group, overwrites `links` above  | _ids as reported by `roc-status`_ |
| link[id]/**gbtMux** | GBT mux; TTC, DDG, or SWT | `"SWT"` |
| link[id]/**enabled** | Enable the link; true or false | `"false"` |

The json [template](https://gitlab.cern.ch/AliceO2Group/system-configuration/-/blob/dev/ansible/roles/flp-readoutcard/templates/cru_config.json.j2) that is pushed to the KV store.
