Create your own inventory for tests
===
An Ansible inventory is basically a list of hostnames grouped into logical sets: 
```
# example from official documentation
[webservers]
foo.example.com
bar.example.com

[dbservers]
one.example.com
two.example.com
three.example.com
```
More information can be found [here](http://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html).

In order to test the roles you develop, it is useful to have a private inventory on which you can test without risking rolling out changes to official setups. An easy way to do this is to create a few [CERN Openstack](http://openstack.cern.ch) instances and use them to test your Ansible work. 

Create CERN Openstack instances
---
To create CERN Openstack instances, please follow the instructions available [here](https://clouddocs.web.cern.ch/clouddocs/). 
For the purpose of this toturial, create 2 nodes (use your own hostname if these ones already exist): 

- `my-head-server.cern.ch`: used to install the central services like AliECS, monitoring and infologger.
- `my-flp.cern.ch`: target FLP.


Create an Ansible inventory
---
In its simplest form, an Ansible inventory is just an INI file with groups of hosts. Please put the content below in a file called `inventory-test`, replacing the hostnames with whatever name you used to create your CERN Openstack instances.  Put it somewhere outside the repo because this is not something you want to push upstream. Here we assume it's in `/tmp`.

```
[head]
my-head-server.cern.ch   # replace this with your hostname

[flps]
my-flp.cern.ch           # replace this with your hostname
```

We can now run the ping *ad-hoc* command from the previous section against this new inventory:  

```
ansible all -i /tmp/inventory-test -m ping
...
my-head-server.cern.ch | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    }, 
    "changed": false, 
    "ping": "pong"
}
my-flp.cern.ch | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    }, 
    "changed": false, 
    "ping": "pong"
}
```

If we now run `ansible-playbook` to execute the `flp-multinode.yml` playbook, it should do a bunch of tasks like setting up YUM repos and deploying packages like AliECS and Readout (this will take ~ 20 min): 

```
ansible-playbook -u root -i /tmp/inventory-test ansible/flp-multinode.yml
...
PLAY [all] ******************************************************************************************

TASK [Gathering Facts] ******************************************************************************
ok: [head-server.cern.ch]
ok: [my-flp.cern.ch]

TASK [basevars : Gather CRU PCI addresses] **********************************************************
ok: [head-server.cern.ch]
ok: [my-flp.cern.ch]

TASK [basevars : Gather CRORC PCI addresses] ********************************************************
ok: [head-server.cern.ch]
ok: [my-flp.cern.ch]

TASK [o2-base : Ensure EPEL repository present] *****************************************************
changed: [head-server.cern.ch]
changed: [my-flp.cern.ch]

TASK [o2-base : Ensure CERN repository exists] ******************************************************
changed: [head-server.cern.ch]
changed: [my-flp.cern.ch]

TASK [o2-base : Check if ali02-c7-base.repo is present] *********************************************
changed: [head-server.cern.ch]
changed: [my-flp.cern.ch]
...
```

Next steps
---
Notice the `-u root` option used. Ideally you should be able to SSH as a normal user and then Ansible would use existing privilege escalation systems to execute the tasks that need it. Let's see how to [setup proper authentication](authentication.md).
