Linting roles and playbooks
===

An Ansible file can be checked for the best practices and behaviour that could potentially be improved.   
This is done using `ansible-lint`. The list of default rules can be found [here](https://docs.ansible.com/ansible-lint/rules/default_rules.html).

Installation
---

```
pip install ansible-lint
```

In case you don't have `pip` installed run `yum install python-pip` on CentOS 7 or `brew install python` on MacOS.


Usage
---

Linting a file or a role is as simple as:
```
ansible-lint <file>/<role_dir>
```


Recommended options:

| Option   |      Description      |  Recommended/CI value |
|----------|-----------------------|-----------------------|
| `-x` |  disables certain rule(s), the full list of rules is available in [here](https://docs.ansible.com/ansible-lint/rules/default_rules.html) | `403,701` |
| `--exclude` |    excludes files/directories, eg. imported roles  |   `ansible/roles/ansible-mesos,ansible/roles/ansible-zookeeper`  |



Sample commands
---

Linting `flp-standalone` playbook:
```
ansible-lint ansible/flp-standalone.yml -x 403,701 --exclude ansible/roles/ansible-mesos,ansible/roles/ansible-zookeeper
```


Linting `flp-standalone` role:
```
ansible-lint ansible/roles/flp-standalone -x 403,701 --exclude ansible/roles/ansible-mesos,ansible/roles/ansible-zookeeper
```

Ignoring a rule
---

To ignore a specific rule for a specific task, the culprit line, as reported by the linter, should end with `# noqa [rule_id]`. For example ignoring rule 503 for the task "A random task" would go like this;
```
-name: "A random task" # noqa 503
...
```

More advanced scenarios can be found [here](https://github.com/ansible/ansible-lint#false-positives-skipping-rules).