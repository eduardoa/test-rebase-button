Examples of commands
===
A few examples: 

Run the main FLP playbook on all hosts: 
```
ansible-playbook -i /tmp/inventory-test ansible/flp-multinode.yml
```

Deploy dataflow components on all nodes: 
```
ansible-playbook -i /tmp/inventory-test -t dataflow ansible/flp-multinode.yml
``` 

Same as previous, but on a single host: 
```
ansible-playbook -i /tmp/inventory-test -l my-special-node.cern.ch -t dataflow ansible/flp-multinode.yml
```

Different partial deployments of different modules
```
ansible-playbook -i /tmp/inventory-test ansible/flp-multinode.yml -t dataflow
ansible-playbook -i /tmp/inventory-test ansible/flp-multinode.yml -t monitoring
ansible-playbook -i /tmp/inventory-test ansible/flp-multinode.yml -t infologger
ansible-playbook -i /tmp/inventory-test ansible/flp-multinode.yml -t consul
ansible-playbook -i /tmp/inventory-test ansible/flp-multinode.yml -t aliecs
ansible-playbook -i /tmp/inventory-test ansible/flp-multinode.yml -t qc
ansible-playbook -i /tmp/inventory-test ansible/flp-multinode.yml -t extra
```

Next steps
---
Let's see how to ensure proper Ansible [linting](lint.md).
