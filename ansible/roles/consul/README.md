# Consul

An Ansible role that installs and configures [Consul](https://www.consul.io/).
By default:
 - Installs Consul
 - Configures Consul GUI to run over HTTP on port `8500`
 - Runs under systemd

## Host Variables (optional)

| Variable               | Default value      | Notes                                                         |
|------------------------|--------------------|---------------------------------------------------------------|
| consul_bin_dir         | `/usr/local/bin`   | -                                                             |
| consul_config_dir      | `/etc/consul`      | -                                                             |
| consul_data_dir        | `/var/lib/consul`  | -                                                             |
| consul_datacenter      | `alice-o2-cluster` | -                                                             |
| consul_ui              | `true`             | -                                                             |
| consul_port_ui         | `8500`             | Consul port for UI and HTTP Api                               |
| consul_role            | -                  | Boolean if consul should install in `server` or `client` mode |
| consul_flps_key_prefix | `o2/hardware/flps` | Key prefix by which flps hosts can be found in consul         |
| consul_configs_key_prefix | `o2/configs/flps` | Key prefix by which configurations can be found in consul  |
