# O<sup>2</sup> Monitoring performance tests
In order to verify the performance of the Monitoring tools a test role has been created.
It runs the Monitoring library benchmark: https://github.com/AliceO2Group/Monitoring/blob/dev/examples/5-Benchmark.cxx

### Starting tests
Benchmark can be lauched at all 40 client machines at once with following commands:
* Starting single instance of the benchmark  per machine
```
ansible -b -i <inventory> <hosts> -u root -a "systemctl start o2-monitoring"
```
* Starting multiple instances per machine
```
ansible -b -i <inventory> <hosts> -u root -a "systemctl start o2-monitoring@id"
```
where `id` is a instance ID - integer number.
