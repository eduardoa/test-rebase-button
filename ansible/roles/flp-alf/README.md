# flp-alf

An Ansible role that installs and deploys the ALF package.

- The role installs the ALF RPM.
- It deploys the systemd unit to run ALF.

## Role Variables

Available variables are listed below, along with default values:

| Variable                                | Default Value            | Notes                                                                            |
| ----------------------------------------|--------------------------|----------------------------------------------------------------------------------|
| `flp_alf_packagename`                   | ALF                      | ALF package name to install                                                      |
| `flp_alf_module`                        | ALF/vX.Y.Z-1             | ALF module identifier                                                            |
| `flp_alf_binary`                        | o2-alf                   | ALF binary for the systemd unit                                                  |

See [alf/vars](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/master/ansible/roles/alf/vars/main.yml)

## Dependencies

`o2-base`

`basevars`

See [alf/meta](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/master/ansible/roles/alf/meta/main.yml)
