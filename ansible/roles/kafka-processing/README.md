# kafka-processing

An Ansible role that configures [MonitoringCustomComponents](https://github.com/AliceO2Group/MonitoringCustomComponents/tree/master/kafka-components) in order to provide Kafka Stream processing:
- Router
- OnOff
- Aggregation: average, min, max, sum

## Dependencies
 - kafka-base

 ## Defaults

| Variable                          | Default value             | Notes                   |
| --------------------------------- | ------------------------- | ----------------------- |
| kafka_processing_router_topic     | `router`                  | - |
| kafka_processing_onoff_topic      | `onoff`                   | - |
| kafka_processing_aggregator_topic | `aggregator`              | - |
| kafka_processing_state_dir        | `/local/data/kafka_state` | - |
