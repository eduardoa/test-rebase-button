# FLP software development

An Ansible role that installs FLP development dependencies and configures aliBuild according to instructions: https://alice-doc.github.io/alice-analysis-tutorial/building/prereq-centos7.html

Available variables are listed below, along with default values:

| Variable                                | Default Value            | Notes                                            |
| ----------------------------------------|--------------------------|--------------------------------------------------|
| `flp_dev_user`                          | `alice`                  | User account for aliBuild configuration          |


## Installation

1. Install `o2-flp-setup` as described [here](../../../utils/o2-flp-setup/README.md)
2. Execute `o2-flp-setup --head <target_hostname> --flps <target_hostname> deploy --modules dev`

If you have an existing Ansible inventory and want to install on all machines in it, you can do the following: 

2. Execute `o2-flp-setup -i /path/to/inventory deploy --modules dev`

If the target host is behind an HTTP proxy, you will need to specify it either in the inventory or using the `--proxy` option of `o2-flp-setup`.

## Usage

After installation, go to `~/alice` and use `aliBuild` to build a package.
