# test

A role containing tests which are being ran at the end of the `flp-standalone` role during the GitLab - CI/CD build process.

## Requirements
It makes use of `basevars` role to access variables

For guidance on how to write a test for ansible please see: [Testing deployed role](../../docs/test-role.md)