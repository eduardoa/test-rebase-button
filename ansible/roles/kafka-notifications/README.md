# kafka-notifications

An Ansible role that configures [MonitoringCustomComponents](https://github.com/AliceO2Group/MonitoringCustomComponents/tree/master/kafka-components) in order to distribute notifications to the following endpoints:
- Mattermost
- Email
- WebUI

## Dependencies
 - kafka-base

## Mattermost consumer specific variables

| Variable                   | Default value         | Notes                   |
| -------------------------- | --------------------- | ----------------------- |
| kafka_mattermost_hook      | -                     | Enables deployment of Mattermost consumer |
| kafka_mattermost_topic     | `notifications`       | - |

## Email consumer specific variables

| Variable                   | Default value         | Notes                   |
| -------------------------- | --------------------- | ----------------------- |
| kafka_email_topic          | `email`               | -                       |
| kafka_email_smtp_hostname  | `smtp.cern.ch`        | -                       |
| kafka_email_smtp_port      | `587`                 | -                       |
| kafka_email_address        | `alice-o2.notifications@cern.ch` | Enables deployment of Email consumer |
| kafka_email_username       | `alinotif`            |                         |
| kafka_email_password       | `***`                 |   -                     |
