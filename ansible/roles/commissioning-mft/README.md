# MFT commissioning

An Ansible role that installs extra software required by the MFT team to perform the commissioning of the detector in building 581.


## Prerequisites (as root)
```
yum -y install git python-{cryptography,six,jinja2} PyYAML nmap-ncat
yum --disablerepo=* --enablerepo=epel -y install ansible
git clone --branch dev https://gitlab.cern.ch/AliceO2Group/system-configuration.git
```

## Local installation on single node 

```
cd system-configuration/ansible
ansible-playbook --connection=local --inventory 127.0.0.1, commissioning-mft.yml
```

## Remote installation on all nodes

```
cd system-configuration/ansible
ansible-playbook -u root -k --inventory alimftcom1,alimftcom2,alimftcom3, commissioning-mft.yml
```
