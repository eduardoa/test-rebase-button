flp-readout.exe-config
=========

This roles creates a basic configuration for readout.
The file is created in the home directory of user configured to run the readoutcard (typically: /home/flp/readout.cfg).
It instanciates a HugePage memory bank, a dummy equipment generating random data, and connection to monitoring.
It includes disabled examples for CRU equipment and file recording.
If one or more CRUs are present in the system, the CRU equipment will be assigned a valid PCI address (the first CRU returned by
lspci -n). Otherwise the invalid PCI address xx:yy.z will be set, which will need to be changed manually in the final .cfg file.

Also, it creates a cru-optimized configuration file (typically: /home/flp/readout-cru.cfg) based on system settings discovery
at the time this recipe is called (available HugePage size, number and NUMA settings of CRU boards detected).

Requirements
------------

None.

Role Variables
--------------

| Variable                  | Default value | Notes                         |
|---------------------------|---------------|-------------------------------|
| monitoring_readout_url    |               | See role basevars.            |
| flp_username              |               | See role basevars.            |


Dependencies
------------

This role depends on: basevars.

