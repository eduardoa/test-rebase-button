# influxdbv2

An Ansible role that installs and configures [InfluxDB 2.0](https://v2.docs.influxdata.com/v2.0/).
