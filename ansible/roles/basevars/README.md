# basevars

A role containing variables that are common across roles.

## Requirements

### InfoLogger

A non-empty `mariadb` group in the Ansible inventory (override through variables below).

## Role Variables

Available variables are listed below, along with default values:

| Variable                   | Default Value                                                         | Notes                                                                        |
|----------------------------|-----------------------------------------------------------------------|------------------------------------------------------------------------------|
| `infologger_mysql_dbname`  | 'INFOLOGGER'                                                          | MariDB database name                                                         |
| `infologger_mysql_dbhost`  | -                                                                     | Defaults to `ansible_hostname` (single node)` or `head[0]` (multi node)      |
| `infologger_server_host`   | -                                                                     | Defaults to `ansible_hostname` (single node)` or `head[0]` (multi node)      |
| `influxdb_http_port`       | 8086                                                                  |                                                                              |
| `influxdb_user`            | "grafana"                                                             |                                                                              |
| `influxdb_pass`            | "b40NQyen2PjwSpW"                                                     |                                                                              |
| `influxdb_host`            | -                                                                     | Defaults to `ansible_hostname` (single node)` or `head[0]` (multi node)      |
| `influxdb_https_enabled`   | "false"                                                               |                                                                              |
| `influxdb_readout_port`    | 8088                                                                  |                                                                              |
| `monitoring_readout_url`   | "influxdb-udp://localhost:{{ influxdb_readout_port }}"                |                                                                              |
| `dim_dns_node`             | -                                                                     | Defaults to `ansible_hostname` (single node)` or `head[0]` (multi node)      |
| `flp_alf_instances`        | 1                                                                     | Number of ALF instances to be spawned                                        |
| `flp_username`             | flp                                                                   | Name of user to perform readout                                              |
| `flp_user_password`        | -                                                                     | Password for flp user (Contact the FLP team for the password)                | 
| `consul_port_ui`           | 8500                                                                  | Port of the Consul UI and HTTP API                                           |
| `consul_bind_address`      | -                                                                     | Defaults to `ansible_hostname` (single node)` or `head[0]` (multi node)      |
| `ccdb_port`                | 8083                                                                  | Port at which the CCDB is available                                          |
| `ccdb_host`                | -                                                                     | Defaults to `ansible_hostname` (single node)` or `head[0]` (multi node)      |
| `qc_repo_implementation`   | CCDB                                                                  | Implementation or backend of the QC repository (CCDB or MySQL)               |
| `cog_http_port`            | 8080                                                                  | HTTP port for Control GUI                                                    |
| `ilg_http_port`            | 8081                                                                  | HTTP port for InfoLoggerWeb GUI                                              |
| `qcg_http_port`            | 8082                                                                  | HTTP port for QualityConytol GUI                                             |
