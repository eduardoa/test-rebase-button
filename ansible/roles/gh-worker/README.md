# GH worker

An Ansible role that provisions GitHub self-hosted runner. 
It depends on `flp-dev` role.

| Variable                                | Default Value            | Notes                                            |
| ----------------------------------------|--------------------------|--------------------------------------------------|
| `gh_worker_repo`                        | -                        | Required per each worker                         |
| `gh_worker_token`                       | -                        | Required per each worker                         |