# influxdb

An Ansible role that installs and configures [influxdb](https://docs.influxdata.com/influxdb).
By default:
- HTTP endpoint available on port `8086`
- HTTPS disabled (requires certificate and private key)
- Basic Auth enabled: `admin` and `grafana` read-only accounts created
- `readout` database and UDP endpoint (`8088`) configured
- Firewall configured

## Role Variables (optional)

| Variable                   | Default value                        | Notes                                  |
| -------------------------- | ------------------------------------ | -------------------------------------- |
| influxdb_https_enabled     | `true`                               | HTTPS enabled by default               |
| influxdb_https_certificate | `/etc/pki/tls/certs/certificate.crt` | Required for HTTPS                     |
| influxdb_https_private_key | `/etc/pki/tls/private/private.key`   | Required for HTTPS                     |
| influxdb_auth_enabled      | `true`                               | Basic Auth                             |
| influxdb_admin_user        | admin                                | In console mode: `influx [-ssl] -username <user> -password <pass>` |
| influxdb_admin_pass        | ---                                  | Admin password                         |
| influxdb_udp_config        | `readout` configuration              | Array storing UDP endpoints            |
|  - bind_address            | -                                    | Port number                            |
|  - database                | -                                    | Database name                          |
