DataDistribution
=========

This is the role to install the DataDistribution package.
It provides StfBuilder, StfSender as well as example configuration files.

Requirements
------------

None.

Role Variables
--------------

| Variable                        | Default value       | Notes                                         |
| ------------------------------- | --------------------|---------------------------------------------- |
| flp_datadist_packagebasename    | DataDistribution    | base name of alisw RPM to be installed        |

Dependencies
------------

o2-base, in order to setup the YUM repo to get RPMs from.
