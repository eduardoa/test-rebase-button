# dim

An Ansible role to install and configure DIM for the needs of ALF.

- It deploys a DIM Nameserver.
- It deploys the DIM WebDID GUI.
- It opens the necessary ports for the above.


Available variables are listed below, along with default values:

| Variable                                | Default Value            | Notes                                                                            |
| ----------------------------------------|--------------------------|----------------------------------------------------------------------------------|
| `dim_packagename`                       | `dim`                    | The DIM package name                                                             |
| `dim_target_dir`                        | /opt/alisw/el7/dim/v20r26 | The directory under which DIM will be installed                                  |
| `dim_module`                            | dim/v20r26-1             | DIM module name for module load                                  |

See [dim/vars](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/master/ansible/roles/dim/vars/main.yml)

## Dependencies

`o2-base`

`basevars`

See [dim/meta](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/master/ansible/roles/dim/meta/main.yml)
