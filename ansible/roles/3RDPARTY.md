# Third party modules

The following modules were pulled from ansible-galaxy and are kept here for ease of use:
* https://github.com/AnsibleShipyard/ansible-mesos
* https://github.com/AnsibleShipyard/ansible-zookeeper
