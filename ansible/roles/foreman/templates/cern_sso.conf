#
# CERN configuration for mod_auth_mellon SAML 2.0
# authentication service
#
# Preconfigures whole apache server tree to use Mellon.
#
<Location />

    # enable mellon for entire site served by apache
    AuthType Mellon
    MellonEnable "info"

    #
    # enable multi valued variables (for group authentication)
    MellonEnvVarsSetCount On
    MellonEnvVarsIndexStart 1

    #
    # must correspond to path registered with CERN SSO service
    # at: https://cern.ch/sso-management
    #
    MellonEndpointPath /mellon

    #
    # Mellon metadata files created using:
    # cd /etc/httpd/conf.d/mellon/
    # /usr/libexec/mod_auth_mellon/mellon_create_metadata.sh \
    # https://HOSTNAME.cern.ch/mellon https://HOSTNAME.cern.ch/mellon
    #
    # change HOSTNAME to your system hostname.
    MellonSPPrivateKeyFile /etc/httpd/conf.d/mellon/https_{{ normalized_hostname }}_mellon.key
    MellonSPCertFile /etc/httpd/conf.d/mellon/https_{{ normalized_hostname }}_mellon.cert
    MellonSPMetadataFile /etc/httpd/conf.d/mellon/https_{{ normalized_hostname }}_mellon.xml

    #
    # IdP (Identity Provider) metadata downloaded as:
    # curl -o /etc/httpd/conf.d/mellon/ \
    # https://login.cern.ch/FederationMetadata/2007-06/FederationMetadata.xml
    #
    MellonIdPMetadataFile /etc/httpd/conf.d/mellon/FederationMetadata.xml

    MellonSetEnvNoPrefix "REMOTE_USER" "http://schemas.xmlsoap.org/claims/CommonName"
    MellonSetEnvNoPrefix "REMOTE_USER_EMAIL" "http://schemas.xmlsoap.org/claims/EmailAddress"
    MellonSetEnvNoPrefix "REMOTE_USER_GROUP" "http://schemas.xmlsoap.org/claims/Group"
    MellonSetEnvNoPrefix "REMOTE_USER_FIRSTNAME" "http://schemas.xmlsoap.org/claims/Firstname"
    MellonSetEnvNoPrefix "REMOTE_USER_LASTNAME" "http://schemas.xmlsoap.org/claims/Lastname"

    #
    # 'user' variable - MUST be set.
    #
    MellonUser "REMOTE_USER"

    #
    # for debugging only
    #
    #MellonSamlResponseDump On

</Location>

<Location /mellon>
    AuthType Mellon
    MellonEnable "auth"
    Require valid-user
</Location>

<Location /users/extlogin>
    AuthType Mellon
    MellonEnable "auth"
    Require valid-user

    # Restrict access to members of the specified e-group
    # Explanation of the tags:
    # - OR means that following MellonCond directives are checked
    #   if verification of this one fails.
    #   NOTE: when the last MellonCond directive has an OR
    #   tag the entire condition evaluates to true, so the last
    #   line should omit OR
    # - MAP means that REMOTE_USER_GROUP is a variable mapped by
    #   MellonSetEnv{,NoPrefix}
    #   Equivalently removing MAP and using
    #   "http://schemas.xmlsoap.org/claims/Group" instead of
    #   REMOTE_USER_GROUP also works
    #
    # To extend login to other groups, just duplicate the line below
    # replacing ep-dep-aid-da with the corresponding groups and add
    # the OR tag to every line but the last.
    MellonCond REMOTE_USER_GROUP alice-o2-flp-prototype [MAP]
</Location>
