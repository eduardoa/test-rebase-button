# Set up Foreman with Smart Proxy and Ansible support

Ansible incantation with default inventory:
```
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -b foreman.yml -i inventory-o2-lab -u root --ask-pass -e foreman_url="https://aidrefsrv20.cern.ch"
```
