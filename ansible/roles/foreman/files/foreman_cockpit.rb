require 'foreman_cockpit/engine'

# Module required to start the Rails engine
# We store all cockpit actions in a constant here to use them later
module ForemanCockpit
  COCKPIT_ACTIONS = %w(system terminal journal
                       services networking machines
                       accounts diagnostics kdump
                       selinux)

  COCKPIT_SUBURL = { :system => 'system/index.html',
                     :terminal => 'system/terminal.html',
                     :journal => 'system/logs.html',
                     :services => 'system/services.html',
                     :networking => 'network/index.html',
                     :machines => 'machines/index.html',
                     :accounts => 'users/index.html',
                     :diagnostics => 'sosreport/index.html',
                     :kdump => 'kdump/index.html',
                     :selinux => 'selinux/setroubleshoot.html' }
end
