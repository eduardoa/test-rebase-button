# flp-standalone

An Ansible role that installs and configure a standalone FLP, comprised of the roles listed in the dependencies section.

A convenience script can be found under `ansible/utils/o2-install-flp-standalone`. It allows for an ad-hoc execution of the role by
passing the target hostname as an argument.

## Dependencies

`o2-cleanup`
`ccdb-sql`
`infologger-db`
`infologger-server`
`infologger-client`
`infologger-gui`
`flp-readoutcard`
`dim`
`flp-alf`
`flp-readout.exe`
`flp-readout.exe-config`
`control-core`
`nginx`
`control-node`
`control-gui`
`consul`
`quality-control`
`qc-gui`
`influxdb`
`telegraf`
`grafana`
`misc`
`test`
