# o2-users

An Ansible role to populate control default settings in consul.


## Role Variables

Available variables are listed below, along with default values:

``` 
control_default_consul_values:
    - key: o2/control/default_revision
      value: master
    - key: o2/control/default_revisions
      value: '{"github.com/AliceO2Group/ControlWorkflows/" : "master"}'
```