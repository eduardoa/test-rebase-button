readout.exe
=========

This is the role to install the Readout package.
It provides readout.exe and related utilities, as well as example configuration files.

Requirements
------------

None.

Role Variables
--------------

| Variable                        | Default value       | Notes                                         |
| ------------------------------- | --------------------|---------------------------------------------- |
| flp_readout_exe_packagebasename | Readout             | base name of alisw RPM to be installed        |        

Dependencies
------------

o2-base, in order to setup the YUM repo to get RPMs from.
