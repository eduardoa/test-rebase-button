# ccdb-sql

An Ansible role that installs the sql-based CCDB to be used as QC backend. 

## Difference with the ccdb Ansible role

The `ccdb` ansible role install the so-called "local" version of the CCDB, a light-weight CCDB.

## Requirements

CC7 with working Yum and an access to alimonitor.cern.ch.

## Role Variables

| Variable            | Default value                                | Notes                                     |
|---------------------|----------------------------------------------|-------------------------------------------|
| None                |                                              |                                           |

See [ccdb/defaults](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/master/ansible/roles/ccdb/defaults/main.yml)

## Dependencies

`basevars`
`postgresql`

See [quality-control/meta](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/master/ansible/roles/ccdb/meta/main.yml)
