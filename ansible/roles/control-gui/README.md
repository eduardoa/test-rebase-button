# Control GUI

An Ansible role that installs and configures [Control GUI](https://github.com/AliceO2Group/WebUi/tree/dev/Control).
By default:
 - Installs NodeJS 10.x
 - Configures GUI to run over HTTP on port `8080` (File: `/etc/o2.d/o2-cog.js`)
 - Runs under systemd: `systemctl start o2-cog`

Optionally allows to set up TLS and CERN SSO.

## Host Variables (optional)

| Variable          | Default value  | Notes                  |
|-------------------|----------------|------------------------|
| cog_hostname      | `Ansible FQDN` | -                      |
| cog_grpc_hostname | `localhost`    | Control gRPC endpoint  |
| cog_grpc_port     | `47102`        | Admin password         |
| cog_tls           | `false`        | TLS enable flag        |
| cog_https_port    | `8443`         | HTTPS port             |
| cog_tls_key       | -              | TLS key filepath       |
| cog_tls_cert      | -              | TLS cert filepath      |
| cog_oauth_enable  | `false`        | CERN OAuth enable flag |
| cog_oauth_id      | -              | OAuth ID               |
| cog_oauth_secret  | -              | OAuth secret           |
