# ccdb

An Ansible role that installs the CCDB to be used as QC backend.

## Requirements

CC7 with working Yum and an access to alimonitor.cern.ch.

## Role Variables

| Variable            | Default value                                | Notes                                     |
|---------------------|----------------------------------------------|-------------------------------------------|
| ccdb_local_dir      | /tmp                                         | Where to store local data (jar and files) |

See [ccdb/defaults](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/master/ansible/roles/ccdb/defaults/main.yml)

## Dependencies

`basevars`

See [quality-control/meta](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/master/ansible/roles/ccdb/meta/main.yml)
