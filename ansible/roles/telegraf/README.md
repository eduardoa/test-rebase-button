# Telegraf

An Ansible role that installs and configures [telegraf](https://github.com/influxdata/telegraf).
By default provides following plugins:
- cpu
- mem
- net
- kernel (interrupts and context switches)
- system (uptime)

In addition it may collect local metrics over Unix socket or run CRU monitoring script.

## Role Variables

Available variables are listed below, along with default values.  

| Variable                   | Default value         | Notes                   |
| -------------------------- | --------------------- | ----------------------- |
| telegraf_output_plugin     | `influxdb`            | `influxdb` or `kafka`   |
| telegraf_output_url        | http://localhost:8086 | Output endpoint         |
| telegraf_interval          | 30s                   | Refresh rate            |
| telegraf_unixsocket_input_enable  | false          | Unix socket (``/tmp/telegraf.sock`)        |
| telegraf_kafka_topic       | `telegraf`            | If `kafka` plugin is used this must be set |
| telegraf_cru_enabled       | `false`               | CRU card monitoring     |
| telegraf_readoutcard_version | -                   | ReadoutCard package version required for CRU monitoring |
