# infologger-server

An Ansible role that installs and configures the InfoLogger server. It deploys a
configuration file which depends on `basevars` values, and it sets up
the systemd unit to run at startup. It also takes care of opening the relevant
ports in the firewall and makes the generated configuration file available on
the local system.

## Requirements

A non-empty `infologger-server` group must be present in the inventory, as well as a working MariaDB instance and working CC7 `firewalld`.

## Role Variables

None.

## Dependencies

`infologger-base`

See [infologger-server/meta](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/master/ansible/roles/infologger-server/meta/main.yml)
