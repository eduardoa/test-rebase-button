# kafka

An Ansible role that installs and configures [kafka](https://kafka.apache.org).
By default it:
- Installs and configures Kafka broker

## Dependencies
 - zookeeper
 - kafka-base

## Role Variables

Available variables are listed below, along with default values.

| Variable                   | Default value         | Notes                   |
| -------------------------- | --------------------- | ----------------------- |
| kafka_broker_id            | `0`                   | This must be unique within cluster, set it in `host_vars` |
| kafka_data_dir             | `/local/data/kafka`   | - |
