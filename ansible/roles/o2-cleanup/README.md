# o2-cleaup

An Ansible role that cleans up the relevant Yum repositories as a workaround for the [O2-877](https://alice.its.cern.ch/jira/browse/O2-877) issue.


## Requirements

CC7 and Yum.

## Role Variables

None.

## Dependencies

None.
