# Quality Control GUI

An Ansible role that installs and configures [QualityControl GUI](https://github.com/AliceO2Group/WebUi/tree/dev/QualityControl).
By default:
 - Installs NodeJS 10.x
 - Configures GUI to run over HTTP on port `8082`
 - Runs under systemd

Installs Quality Control GUI using aliBuild generated RPMs.
 - Config install under `/etc/o2.d/o2-qcg.js`
 - Launch: `systemctl start o2-qcg`

## Dependencies
`quality-control`

## Role Variables (optional)

| Variable           | Default value     | Notes                  |
|--------------------|-------------------|------------------------|
| qcg_http_hostname  | `Ansible FQDN`    | HTTP hostname          |
| qcg_tls            | `false`           | TLS enable flag        |
| qcg_https_port     | `8445`            | HTTPS port             |
| qcg_tls_key        | -                 | TLS key filepath       |
| qcg_tls_cert       | -                 | TLS cert filepath      |
| qcg_mysql_hostname | `Ansible FQDN`    | MySQL hostname         |
| qcg_mysql_user     | -                 | MySQL user             |
| qcg_mysql_password | -                 | MySQL password         |
| qcg_mysql_database | `quality_control` | MySQL database         |
| qcg_amore_hostname | `Ansible FQDN`    | Amore hostname         |
| qcg_amore_user     | -                 | Amore user             |
| qcg_amore_password | -                 | Amore password         |
| qcg_oauth_enable   | `false`           | CERN OAuth enable flag |
| qcg_jwt_secret     | -                 | -                      |
| qcg_jwt_expiration | -                 | -                      |
| qcg_oauth_secret   | -                 | OAuth secret           |
| qcg_oauth_id       | -                 | OAuth ID               |
| qcg_oauth_egroup   | `alice-member`    | -                      |