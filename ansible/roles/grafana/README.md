# Grafana

An Ansible role that installs and configures [Grafana](https://grafana.com).
By default:
 - Configured Grafana to run over HTTP on port `3000`
 - Creates "Public" organisation with anonymous dashboard access
 - Create datasources to InfluxDB `readout` and `telegraf` databases
 - Default dashboards installed

## Host Variables (optional)

| Variable                     | Default value       | Notes                                         |
| ---------------------------- | --------------------|---------------------------------------------- |
| grafana_protocol             | `http`              | `http` or `https`                             |
| grafana_admin_password       | -                   | Admin password                                |
| grafana_oauth_enabled        | false               | CERN SSO OAuth, `https` is required           |
| grafana_oauth_client_id      | -                   | OAuth ID                                      |
| grafana_oauth_client_secret  | -                   | OAuth secret                                  |
| grafana_disable_login_form   | false               | Whether logging form is displayed             |
| grafana_influxdb_datasources | `readout` and `telegraf` sources | Array defining InfluxDB sources  |
|  - protocol                  |                     | |
|  - host                      |                     | |
|  - port                      |                     | |
|  - username                  |                     | |
|  - password                  |                     | |
|  - database                  |                     | |

## Host files (optional)

- Dashboards files should be located in the `<inventory>/host_files/<hostname>/grafana_dashboards` directory. They will be copied to the `/var/lib/grafana/dashboards` directory.
