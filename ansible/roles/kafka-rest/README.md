# Kafka REST Proxy

An Ansible role that installs and configures [Kafka REST Proxy](https://docs.confluent.io/current/kafka-rest/index.html).
By default it:
- Installs and configures Kafka REST API

## Role Variables

Available variables are listed below, along with default values.

| Variable                   | Default value         | Notes                   |
| -------------------------- | --------------------- | ----------------------- |
| kafka_rest_hostname        | `8079`                | - |
| kafka_rest_port            | `{{ ansible_fqdn }}`  | - |
| kafka_rest_user            | `kafka`               | - |
| kafka_rest_broker_hostname | `groups["kafka-brokers"][0]` or `localhost` | - |
| kafka_rest_broker_port     | `9092` | - |
| kafka_rest_zookeeper_hostname | `groups["zookeepers"][0]` or `localhost` | - |
| kafka_rest_zookeeper_port | `2181` | - |


## Notification Service configuration

By default, this role will configure the service to send notifications to the Mattermost channel [MonitoringAlarms](https://mattermost.web.cern.ch/alice/channels/monitoringalarms)@https://mattermost.web.cern.ch/alice

All necessary steps to setup service and prepare HTTP request are below:

### Deploy the roles (single machine)

1. Add Kafka and Kafka REST API roles to an inventory - `<INVENTORY_PATH>/hosts` file:
```
[kafka-rest]
<NAME>.cern.ch

[kafka-brokers]
<NAME>.cern.ch
```

2. Run Anisble
```
ansible-playbook -i <INVENTORY_PATH> monitoring.yml -t kafka,kafka-rest -u root
```


### REST API

Request specification:

| Name                       | Value                 |
| -------------------------- | --------------------- |
| Request method             | `POST`                |
| URL (w/o path)             | `http://<NAME>.cern.ch:8079` |
| Path                       | `/topics/<mattermost\|email\|webui>` |
| Accept                     | `application/vnd.kafka.v2+json` |
| Content-Type               | `application/vnd.kafka.json.v2+json` |

#### Mattermost

URL path must be set to `/topics/mattermost`.

Body of the request (JSON encoded):
- client_url - URL of a sender (accessible from browser)
- description - short description
- details - message body


Sample request:
```bash
curl -X POST \
  -H "Content-Type: application/vnd.kafka.json.v2+json" \
  --data '{"records":[{"value":{"client_url":"https://test.cern.ch:3000","description":"Test notification","details":"High value: 100.000"}}]}' \
  "http://<NAME>.cern.ch:8079/topics/mattermost"
```

#### Email

URL path must be set to `/topics/email`.

Body of the request (JSON encoded):
- subject - email subject
- body - message body
- to_addresses - list of comma separated email addresses

Sample request:
```bash
curl -X POST \
  -H "Content-Type: application/vnd.kafka.json.v2+json" \
  --data '{"records":[{"value":{"subject": "test","body":"test","to_addresses":"test@cern.ch"}}]}' \
  "http://<NAME>.ch:8079/topics/email"
```

#### WebUI: Web Push Notifications

URL path must be set to `/topics/webui`.

The instructions to configure Web UI in order to connect to Kafka cluster are available [in here](https://github.com/AliceO2Group/WebUi/tree/dev/Control#integration-with-notification-service).

Body of the request (JSON encoded):
- description - short description

Sample request:
```bash
curl -X POST \
  -H "Content-Type: application/vnd.kafka.json.v2+json" \
  --data '{"records":[{"value":{"description":"Test notification"}}]}' \
  "http://<NAME>.cern.ch:8079/topics/webui"
```
