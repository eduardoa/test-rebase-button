# FLP Deployment Checks

This role is used to perform checks after FLP Suite deployment. it is mainly used in the CI in the `launch` stage.

The checks are following:
1. Readout dummy data
- Start run: Readout + QualityControl
- Stop run
- Verify if data was read out

## Role Variables

Default values are available in here: [defaults/main.yml](defaults/main.yml).

| Variable                   | Notes                   |
| -------------------------- | ----------------------- |
| flp_checks_workflow        | Workflow name to be used during checks  |
