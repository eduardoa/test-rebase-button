# o2-base

An Ansible role that sets up the relevant Yum repositories and alisw modulefiles
for installing and running O² software.

## Requirements

CC7 and Yum. Network access to ALICE DAQ repo, CERN-CA-certs and alisw-el7 (alibuild CI Yum repo).

## Role Variables

| Variable                     | Default value                      | Notes                                                                         |
| ---------------------------- | -----------------------------------| ----------------------------------------------------------------------------- |
| o2_base_skip_yum_config      | false                              | If true, skip YUM repo setup (useful for setups without access to CERN GPN    |

See [o2-base/defaults](defaults/main.yml)

## Dependencies

None.
