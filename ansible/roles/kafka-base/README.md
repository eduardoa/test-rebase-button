# kafka-base

An Ansible role that sets common variables for Kafka roles.

## Dependencies
 - basevar

## Defaults

| Variable                   | Default value         | Notes                   |
| -------------------------- | --------------------- | ----------------------- |
| kafka_zookeeper_hostname   | `groups["zookeepers"][0]` or `localhost` | - |
| kafka_zookeeper_port       | `2181`                | - |
| kafka_user                 | `kafka`               | - |
| kafka_influxdb_topic       | `influxdb`            | - |
| kafka_stats_type           | `influxdb`            | - |
| kafka_stats_period_ms      | `10000`               | - |

## Custom JAR files

Custom JARs should be uploaded to: `/eos/project/a/alice-o2-system-configuration/www/kafka`
