# kafka-influxdb

An Ansible role that configures [MonitoringCustomComponents](https://github.com/AliceO2Group/MonitoringCustomComponents/tree/master/kafka-components) as InfluxDB consumer.

## Dependencies
 - basevar
