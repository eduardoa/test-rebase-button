# Apache Spark

An Ansible role that installs and configures [Apache Spark](https://spark.apache.org).

## Requirements
Ansible machine:
- Internet connectivity (to download Spark and custom libraries)
- GNU tar (to unarchive flume tar file)

Remote machine:
- \<none\>

## Role Variables

| Variable              | Description                               | Possible value                              |
| ----------------------| ----------------------------------------- | ------------------------------------------- |
| `spark_custom_libs`   | List of custom Spark libraries            | `spark-streaming-pass-through-1.0-SNAPSHOT` |
