# o2-users

An Ansible role that creates o2 users.

- The role will retrieve all machine users.
- It will add o2-users,if they are not previously created, with specified `uid` and password .
- It expires the newly created users.
- It will add epn to /etc/sudoers file.
- It will deploy the provided ssh keys for each user.

## Install 

In order to create o2-users you need to execute the `flp-post-installation.yml` palybook
```
### install (without SSH Key on targeted machine)
ansible-playbook -i /tmp/inventory-test flp-post-installation.yml -u root --ask-pass

### install (with SSH Key on targeted machine)
ansible-playbook -i /tmp/inventory-test flp-post-installation.yml -u root

```

For example, in vertical slices:
```
### install (without SSH Key on targeted machines)
ansible-playbook -i inventory-vertical-slice flp-post-installation.yml -u root --ask-pass

### install (with SSH Key on targeted machines)
ansible-playbook -i inventory-vertical-slice flp-post-installation.yml -u root
```

if you want to specify target hostnames via the command line:
```
### install (without SSH Key on targeted machines)
ansible-playbook -i my-host1,my-host2, flp-post-installation.yml -u root --ask-pass

### install (with SSH Key on targeted machines)
ansible-playbook -i my-host1,my-host2, flp-post-installation.yml -u root
```

## Role Variables

Available variables are listed below, along with default values:

``` 
o2_groups:
    - { name: flp, gid: 1100 }
    - { name: epn, gid: 1101 }
    - { name: pdp, gid: 1102 }
    - { name: qc, gid: 1103 }
    - { name: rc, gid: 1104 }

o2-users:
    - { name: flp, uid: 1100, password: "$6$/kceCcGNCBypfBl6$BpcA.b9hfxLJVL2kz24e41DBKciEOgEYZsQzA1xQN8ZvcdaqpTCr329RioNAS8bOphWLdj0QEU1p.co7cNONu0", groups: "wheel" }
    - { name: epn, uid: 1101, password: "$6$L7iGd6XcSOglP7Ww$y2Ho2XZrKmSHuzlgE5BKA/qnBX8aMZGXKiZjhW6O6vRPqJRNLpqDK7ufwownQpzdVs4kq4/4KBqu8QOs38qTy0", groups: "wheel" }
    - { name: pdp, uid: 1102, password: "$6$QiGI3MvdXNoagyfv$7D.kI.IM51vAUtkHEnoCvlHp4k.soaam9AdpPkWeVH6Coct..S8S3Ht1CgXGkqCy5o8hTo7G1X3/FMgrFSMfT1", groups: "wheel" }
    - { name: qc, uid: 1103, password:  "$6$UrL3dT3qkqtA7ZEL$YEfpaNJZpfpDPzTC9CYt3v6vti7ELXOV1zQDXIt5LsV22ec3At5GZGvf05g6Z6wYycpH3gL9fuheYF2suBq3c.", groups: "wheel" }
    - { name: rc, uid: 1104, password:  "$6$FNgigw1deACXye1H$R59ZLeS9lRqoyl3T45n7bn9a8zDJ7BjNou6eEFpCWWTwf3ySElNfpa0TyfgmPUrZZAuCaPLE3jUM3/PQ/i4fY/", groups: "wheel" }
```

## Role Files

Inside the `files/ssh-keys` folder we store the ssh keys we wish to deploy for each user, in order to provide passwordless access.

```
└── files/
│   └── ssh-keys/ 
│       └── flp              keys for user flp
│       └── epn              keys for user epn
│       └── pdp              keys for user pdp
│       └── qc               keys for user qc
│       └── rc               keys for user rc
```