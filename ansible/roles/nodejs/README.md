# nodejs

An Ansible role that installs and configures [nodejs](https://nodejs.org).
By default:
 - Installs NodeJS

## Host Variables (optional)

| Variable             | Default value  | Notes                  |
|----------------------|----------------|------------------------|
| nodejs_version       |                | In `basevar` defaults  |
