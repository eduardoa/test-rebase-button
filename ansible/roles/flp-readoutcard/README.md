# flp-readoutcard

An Ansible role that installs the ReadoutCard package and configures the system accordingly.

- The role installs the ReadoutCard RPM, the PDA driver and their dependencies.
- It adds the `flp` user, who holds the necessary permissions to use ReadoutCard/Readout.
- It sets up a systemd unit which handles hugepage set up on boot time.
- It optionally sets up a systemd unit which configures the CRU on boot time.

For certain changes to take effect it is necesasry to reboot the target machine after the role has been executed.

## Role Variables

Available variables are listed below, along with default values:

| Variable                                | Default Value            | Notes                                                                               |
| ----------------------------------------|--------------------------|-------------------------------------------------------------------------------------|
| `flp_readoutcard_confdir`               | `/etc/flp.d/readoutcard` | Configuration directory for flp configuration including readout and hugepages       |
| `flp_readoutcard_packagebasename`       | ReadoutCard              | ReadoutCard package name to install                                                 |
| `flp_readoutcard_default_hugepagsz`     | 2M                       | Default hugepage size to allocate (2M or 1G)                                        |
| `flp_readoutcard_small_hugepages_count` | 128                      | Number of small hugepages to allocate. Defined in `basevars`.                       |
| `flp_readoutcard_large_hugepages_count` | 8                        | Number of large hugepages to allocate, defaults to 2 when no ReadoutCard present. Defined in `basevars`.   |
| `flp_readoutcard_configure_cru_onboot`  | false                    | Flag to enable/disable `roc_config.service` for CRU configuration on boot time      |
| `flp_username`                          | flp                      | Name of user to perform readout. Defined in `basevars`                              |
| `flp_user_password`                     | -                        | Password of flp user. Defined in `basevars`. Contact the flp team for the password. |

See [flp-readoutcard/vars](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/master/ansible/roles/flp-readoutcard/vars/main.yml)

See [flp-readoutcard/files](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/master/ansible/roles/flp-readoutcard/files)

## Dependencies

`o2-base`

`basevars`

See [flp-readoutcard/meta](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/master/ansible/roles/flp-readoutcard/meta/main.yml)

## Misc

The `roc_config.service` is disabled by default. To enable it either run the ansible installation with the `configure_cru_boot`
flag set to `true`, or run `systemctl enable roc_config.service` as root on the target machine. Take care to tailor the `roc_config_startup.cfg` file to your needs.
