---
- name: "Ensure running kernel headers and sources are installed for DKMS"
  yum:
    name: kernel-devel-{{ ansible_kernel }}, kernel-headers-{{ ansible_kernel }}
    state: present
    update_cache: yes
  tags: installation

- name: "Ensure relevant utils are installed"
  yum:
    name: libhugetlbfs, libhugetlbfs-utils, jq
    state: present
    update_cache: yes
  tags: installation

- name: "Ensure pda-kadapter-dkms is installed (usually a couple of minutes)"
  yum:
    name: pda-kadapter-dkms-{{ pda_packageversion }}.noarch
    state: present
    update_cache: yes
  tags: installation

- name: "Install ReadoutCard RPM"
  yum:
    name: alisw-{{ flp_readoutcard_packagebasename }}+v{{ flp_readoutcard_packageversion }}.x86_64
    state: present
    update_cache: yes
  tags: installation

- name: "Ensure configuration dir exists"
  file:
    path: "{{ flp_readoutcard_confdir }}"
    state: directory
  tags: installation, configuration

# If one of the next two actions result in a "changed" status, a handler will update grub.cfg

- name: "Add IOMMU support in the kernel command line"
  lineinfile:
    state: present
    dest: /etc/default/grub
    backrefs: yes
    regexp: '^(GRUB_CMDLINE_LINUX=(?!.*intel_iommu)\"[^\"]+)(\".*)'
    line: '\1 intel_iommu=on\2'
  notify:
    - "Write grub cfg file"
  tags: configuration

- name: "Disable GHES through the kernel command line"
  lineinfile:
    state: present
    dest: /etc/default/grub
    backrefs: yes
    regexp: '^(GRUB_CMDLINE_LINUX=(?!.*ghes\.disable)\"[^\"]+)(\".*)'
    line: '\1 ghes.disable=1\2'
  notify:
    - "Write grub cfg file"
  tags: configuration

# Hard mlock limit

# ANSI C quotes for bash to read \t
- name: "Check for hard memlock limit for pda users"
  shell: grep $'^@pda\t\thard\tmemlock.*$' /etc/security/limits.conf
  register: grepHard
  changed_when: false
  failed_when: false
  tags: configuration

- name: "Replace hard memlock limit for pda users"
  lineinfile:
    state: present
    dest: /etc/security/limits.conf
    backrefs: yes
    regexp: "^@pda\t\thard\tmemlock.*$"
    line: "@pda\t\thard\tmemlock\t\tunlimited"
  when: grepHard.stdout
  tags: configuration

- name: "Increase hard memlock limit for pda users"
  lineinfile:
    state: present
    dest: /etc/security/limits.conf
    line: "@pda\t\thard\tmemlock\t\tunlimited"
    insertbefore: '^# End of file'
  when: not grepHard.stdout
  tags: configuration

# Soft mlock limit

# ANSI C quotes for bash to read \t
- name: "Check for soft memlock limit for pdausers"
  shell: grep $'^@pda\t\tsoft\tmemlock.*$' /etc/security/limits.conf
  register: grepSoft
  changed_when: false
  failed_when: false
  tags: configuration

- name: "Replace soft memlock limit for all users"
  lineinfile:
    state: present
    dest: /etc/security/limits.conf
    backrefs: yes
    regexp: "^@pda\t\tsoft\tmemlock.*$"
    line: "@pda\t\tsoft\tmemlock\t\tunlimited"
  when: grepSoft.stdout
  tags: configuration

- name: "Increase soft memlock limit for all users"
  lineinfile:
    state: present
    dest: /etc/security/limits.conf
    line: "@pda\t\tsoft\tmemlock\t\tunlimited"
    insertbefore: '^# End of file'
  when: not grepSoft.stdout
  tags: configuration


# Configure pda

- name: "Deploy hugepage configuration files"
  template:
    src: "{{ item }}"
    dest: "{{ flp_readoutcard_confdir }}/{{ item | basename | regex_replace('.j2','') }}"
    mode: 0644
    backup: yes
  with_fileglob: "templates/hugepages*.j2"
  notify:
    - "Run roc_hugepages_config service"
  tags: configuration

- name: "Ensure group pda exists"
  group:
    name: pda
    state: present
  tags: configuration

- name: Get users
  getent:
    database: passwd

- name: "Create {{ flp_username }} user"
  user:
    name: "{{ flp_username }}"
    uid: "{{ flp_uid }}"
    password: "{{ flp_user_password }}"
  tags: configuration
  when: flp_username not in ansible_facts.getent_passwd

- name: "Add {{ flp_username }} to wheel and pda groups"
  user:
    name: "{{ flp_username }}"
    groups: wheel, pda
  tags: configuration

- name: "Install driver module"
  modprobe:
    name: uio_pci_dma
    state: present
  tags: installation

- name: "Ensure driver module is inserted on boot"
  template:
    src: templates/uio-pci-dma.conf.j2
    dest: /etc/modules-load.d/uio-pci-dma.conf
    owner: root
    group: root
    mode: 0755
    backup: yes
  tags: installation, configuration

# Deploy hugepages systemd unit that properly configures permissions after every reboot

- name: "Deploy roc_hugepages_startup script used by the roc_hugepages_config systemd unit"
  template:
    src: "{{ item }}"
    dest: "{{ flp_readoutcard_confdir }}/{{ item | basename | regex_replace('.j2','') }}"
    mode: 0755
    backup: yes
  with_fileglob: "{{'templates/roc_hugepages_startup.sh.j2'}}"
  notify:
    - "Run roc_hugepages_config service"
  tags: installation

- name: "Deploy hugepages systemd unit"
  template:
    src: "{{ item }}"
    dest: "/etc/systemd/system/{{ item | basename | regex_replace('.j2','') }}"
    mode: 0644
    backup: yes
  with_fileglob: "{{'templates/roc_hugepages_config.service.j2'}}"
  notify:
    - "Run roc_hugepages_config service"
  tags: installation

# Deploy RoC config systemd unit that configures the cards after every reboot

- name: "Deploy cru_template.cfg config file to be used by the roc_config_startup script"
  copy:
    remote_src: yes
    src: "{{ '/opt/alisw/el7/ReadoutCard/v'+flp_readoutcard_packageversion+'/etc/cru_template.cfg' }}"
    dest: "{{ flp_readoutcard_confdir }}/roc_config_startup.cfg"
    backup: yes
  notify:
    - "Run roc_config service"
  tags: installation

- name: "Deploy roc_config_startup script used by the roc_config systemd unit"
  template:
    src: "{{ item }}"
    dest: "{{ flp_readoutcard_confdir }}/{{ item | basename | regex_replace('.j2','') }}"
    mode: 0755
    backup: yes
  with_fileglob: "{{'templates/roc_config_startup.sh.j2'}}"
  notify:
    - "Run roc_config service"
  tags: installation

- name: "Deploy roc_config systemd unit"
  template:
    src: "{{ item }}"
    dest: "/etc/systemd/system/{{ item | basename | regex_replace('.j2','') }}"
    mode: 0644
    backup: yes
  with_fileglob: "{{'templates/roc_config.service.j2'}}"
  notify:
    - "Run roc_config service"
  tags: installation

- name: "Enable roc_hugepages_config service"
  systemd:
    name: roc_hugepages_config
    enabled: yes
    daemon_reload: yes
  tags: installation

- name: "Enable roc_config service"
  systemd:
    name: roc_config
    enabled: yes
    daemon_reload: yes
  tags: installation
  when: flp_readoutcard_configure_cru_onboot

- name: "Check if consul is up at http://{{ consul_bind_address }}:{{ consul_port_ui }}"
  uri:
    url: http://{{ consul_bind_address }}:{{ consul_port_ui }}
    method: GET
    return_content: yes
  ignore_errors: true
  register: consul_deployed
  tags: consul_populate_with_cards

- name: "Check if consul already populated"
  uri:
    url: http://{{ consul_bind_address }}:{{ consul_port_ui }}/v1/kv/{{ consul_configs_key_prefix }}/{{ ansible_hostname }}/cards/1/cru/loopback # noqa 204 # An arbitrary key to check if consul is populated
    method: GET
  ignore_errors: true
  register: consul_populated_check
  tags: consul_populate_with_cards

- name: "Push card information to Consul, if available"
  block:
    - name: "Register output of roc-list-cards to file"
      command: >
        sh -c "eval `aliswmod load ReadoutCard`; roc-list-cards --json | tee /tmp/roc-list-cards.json"
      register: roc_list_cards_output

    - name: "Register a roc-list-cards dict for internal use"
      set_fact:
        flp_readoutcard_cards_dict: "{{ roc_list_cards_output.stdout | from_json }}"
      when: roc_list_cards_output.stdout|length > 0

    - name: "Push the roc-list-cards json to the Consul KV store"
      uri:
        url: http://{{ consul_bind_address }}:{{ consul_port_ui }}/v1/kv/{{ consul_flps_key_prefix }}/{{ ansible_hostname }}/cards
        method: PUT
        src: /tmp/roc-list-cards.json
        remote_src: 'yes'
      when: flp_readoutcard_cards_dict.keys()|length > 0

    - name: "Push CRU default configurations to Consul KV store"
      include_tasks: cru_consul_configs.yml
      vars:
        cru_item: "{{ outer_item }}"
      with_dict: "{{ flp_readoutcard_cards_dict }}"
      loop_control:
        loop_var: outer_item # avoid conflicts with the "item" var in included block

    - name: "Clean files in /tmp"
      file:
        path: /tmp/roc-list-cards.json
        state: absent
  ignore_errors: true
  when: consul_deployed is defined and consul_deployed.status == 200 and consul_populated_check.failed
  tags: consul_populate_with_cards
