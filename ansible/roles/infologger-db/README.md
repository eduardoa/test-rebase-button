# infologger-db

An Ansible role that configures MariaDB for infoLogger. It creates a database and setup corresponding users.

## Requirements

Working MariaDB instance from `mariadb` role and database configuration variables from `basevars`.

## Role Variables

None.

## Dependencies

`mariadb`
`basevars`

See [infologger-db/meta](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/master/ansible/roles/infologger-db/meta/main.yml)
