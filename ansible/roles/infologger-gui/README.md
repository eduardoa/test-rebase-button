# InfoLogger GUI

An Ansible role that installs and configures [InfoLogger GUI](https://github.com/AliceO2Group/WebUi/tree/dev/InfoLogger).
By default:
 - Installs NodeJS 10.x
 - Configures GUI to run over HTTP on port `8081` (File: `/etc/o2.d/o2-ilg.js`)
 - Runs under systemd: `systemctl start o2-ilg`

Optionally allows to set up TLS and CERN SSO.

## Dependencies

`infologger-base`


## Host Variables (optional)

| Variable                  | Default value  | Notes                  |
|---------------------------|----------------|------------------------|
| ilg_hostname              | `Ansible FQDN` | -                      |
| infologger_mysql_port     | `3306`         | MySQL port             |
| infologger_server_port_tx | `6102`         | InfoLogger server port |
| ilg_tls                   | `false`        | TLS enable flag        |
| ilg_https_port            | `8443 `        | HTTPS port             |
| ilg_tls_key               | -              | TLS key filepath       |
| ilg_tls_cert              | -              | TLS cert filepath      |
| ilg_oauth_enable          | `false`        | CERN OAuth enable flag |
| ilg_oauth_id              | -              | OAuth ID               |
| ilg_oauth_secret          | -              | OAuth secret           |
