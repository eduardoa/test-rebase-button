# quality-control

An Ansible role that installs the O² QualityControl and deploys a default configuration to get data from the readout.

## Requirements

CC7 with working Yum and alisw repositories (set up by role `o2-base`).

## Role Variables

| Variable                | Default value                               | Notes                                   |
|-------------------------|---------------------------------------------|-----------------------------------------|
| flpprototype_qc_confdir | /etc/flp.d/qc                               | the path of the configuration directory |

See [quality-control/defaults](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/master/ansible/roles/quality-control/defaults/main.yml)

## Dependencies

`o2-base`, `ccdb-sql`, `basevars`

See [quality-control/meta](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/master/ansible/roles/quality-control/meta/main.yml)
