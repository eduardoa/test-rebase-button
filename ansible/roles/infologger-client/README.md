# infologger-client

An Ansible role that installs and configures the InfoLogger client component (infoLoggerD).

## Requirements

InfoLogger previously installed by `infologger-base`.

## Role Variables

`infologger_config_file: "{{ infologger_confdir }}/infoLoggerD.cfg"` - default depends on `infologger_confdir` from `infologger-base` role

See [infologger-client/vars](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/master/ansible/roles/infologger-client/vars/main.yml)

## Dependencies

`infologger-base`

See [infologger-client/meta](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/master/ansible/roles/infologger-client/meta/main.yml)
