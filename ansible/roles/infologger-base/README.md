# infologger-base

An Ansible role that installs the base InfoLogger package.

## Requirements

No outstanding requirements. This role should run on any CC7 with YUM repositories configured by `o2-base`.

## Role Variables

`infologger_install_path: /opt/o2-InfoLogger` - base install directory for InfoLogger

`infologger_confdir: /etc` - configuration directory path

See [infologger-base/vars](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/master/ansible/roles/infologger-base/vars/main.yml)

## Dependencies

`o2-base`

`basevars`

See [infologger-base/meta](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/master/ansible/roles/infologger-base/meta/main.yml)
