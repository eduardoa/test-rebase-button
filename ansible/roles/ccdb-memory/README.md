# ccdb-memory

An Ansible role that installs the in memory, multicast receiving, CCDB service, to be deployed on all online nodes that need CCDB access

## Requirements

CC7 with working Yum and an access to alimonitor.cern.ch.

## Role Variables

| Variable            | Default value                                | Notes                                     |
|---------------------|----------------------------------------------|-------------------------------------------|
| ccdb_local_dir      | /home/ccdb                                   | Where to store local data (jar and logs)  |

See [ccdb-memory/defaults](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/master/ansible/roles/ccdb-memory/defaults/main.yml)

## Dependencies

`basevars`

See [quality-control/meta](https://gitlab.cern.ch/AliceO2Group/system-configuration/blob/master/ansible/roles/ccdb/meta/main.yml)
