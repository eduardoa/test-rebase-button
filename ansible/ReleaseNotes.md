# Release Notes
All notable changes to the FLP Suite will be documented in this file.   
Components marked as "3<sup>rd</sup> party" are not developed by the ALICE Collaboration.  


## 0.7.0 (2020-04-24)

### Data flow roles
- [flp-readoutcard]: bump ReadoutCard to [v0.19.3](https://github.com/AliceO2Group/ReadoutCard/releases)
- [flp-readout.exe]: bump Readout to [v1.3.9](https://github.com/AliceO2Group/Readout/blob/v1.3.9/doc/releaseNotes.md#v139---31032020)
- [flp-datadist]: deploys Data Distribution [v0.7.4](https://github.com/AliceO2Group/DataDistribution/releases) (NEW) (PROVIDED BY EPN PROJECT)

### Quality Control roles
- [quality-control]: bump Quality Control framework to [v0.24.1](https://github.com/AliceO2Group/QualityControl/releases)
- [qc-gui]: bump Quality Control GUI to [v1.7.0](https://github.com/AliceO2Group/WebUi/releases)
- [ccdb-sql]: bump ccdb to v1.0.5 

### Control roles
- [control-core|node]: bump AliECS to [v0.13.3](https://github.com/AliceO2Group/Control/releases)
- [control-gui]: bump AliECS GUI to [v1.6.2](https://github.com/AliceO2Group/WebUi/releases)

### Slow Control roles
- [flp-alf]: bump ALF to [v0.5.0](https://github.com/AliceO2Group/ALF/releases)

### Infrastructure roles
##### Configuration
- [consul]: bump Consul to [v1.7.2](https://github.com/hashicorp/consul/blob/master/CHANGELOG.md#172-march-16-2020) ("3<sup>rd</sup> party")

##### Logging
- [infologger-base]: bump InfoLogger to [v1.3.9](https://github.com/AliceO2Group/InfoLogger/blob/v1.3.9/doc/releaseNotes.md#v139---09032020)
- [infologger-gui]: bump InfoLogger web-based GUI to [v1.2.3](https://github.com/AliceO2Group/WebUi/releases)

### Other changes
- `o2-flp-setup` bumped to [v0.4.1](../utils/o2-flp-setup/doc/RELEASE.md#041-2020-03-30)
- `o2-install-flp-standalone` deprecated in favour of `o2-flp-setup`
- new [infiniband] role to configure IB cards on FLPs (PROVIDED BY EPN PROJECT)
- several improvements to the [ltu] role
- Apache Mesos verbosity reduced
- FLP Suite CI now runs a nightly job on a server with a CRU

## 0.6.0 (2020-02-11)

### Data flow roles
- [flp-readoutcard]: bump ReadoutCard to [v0.18.7](https://github.com/AliceO2Group/ReadoutCard/releases)
- [flp-readout.exe]: bump Readout to [v1.3.5](https://github.com/AliceO2Group/Readout/blob/v1.3.5/doc/releaseNotes.md#v135---04022020)

### Control roles
- [control-core|node]: bump AliECS to [v0.12.2](https://github.com/AliceO2Group/Control/releases)

### Other changes
- `o2-flp-setup` added support for sysadmin operations (reboot, yum, etc)
- explicit `control_mesos_hostname` ansible variable no longer needed (info extracted from facts)
- `readoutAutoConfigure` execution disabled by default 
- hugepages defaults changed to 8 GB (when readout cards present) and 2 GB (when readout cards not present)


## 0.5.0 (2020-01-20)

### Data flow roles
- [flp-readoutcard]: bump ReadoutCard to [v0.15.5](https://github.com/AliceO2Group/ReadoutCard/releases)
- [flp-readout.exe]: bump Readout to [v1.2.5](https://github.com/AliceO2Group/Readout/blob/v1.2.5/doc/releaseNotes.md#v125---16122019)

### Quality Control roles
- [quality-control]: bump Quality Control framework to [v0.20.1](https://github.com/AliceO2Group/QualityControl/releases)
- [qc-gui]: bump Quality Control GUI to [v1.6.9](https://github.com/AliceO2Group/WebUi/releases)

### Control roles
- [control-gui]: bump AliECS GUI to [v1.4.4](https://github.com/AliceO2Group/WebUi/releases)

### Infrastructure roles
##### Monitoring
- [grafana]: bump Grafana to [v6.5.1](https://github.com/grafana/grafana/blob/master/CHANGELOG.md#651-2019-11-28) ("3<sup>rd</sup> party")

##### Logging
- [infologger-gui]: bump InfoLogger web-based GUI to [v1.2.0](https://github.com/AliceO2Group/WebUi/releases)

### Other roles
- [ltu] deploys LTU software (NEW)

### Other changes
- Set up Logrotate to remove stale Mesos Master+Slave logs
- Addition of ansible inventory for vertical slice


## 0.4.0 (2019-12-04)

### Data flow roles
- [flp-readoutcard]: bump ReadoutCard to [v0.15.0](https://github.com/AliceO2Group/ReadoutCard/releases)
- [flp-readout.exe]: bump Readout to [v1.2.4](https://github.com/AliceO2Group/Readout/blob/v1.2.4/doc/releaseNotes.md#v124---2122019)

### Quality Control roles
- [qc-gui]: bump Quality Control GUI to [v1.6.6](https://github.com/AliceO2Group/WebUi/releases)

### Slow Control roles
- [flp-alf]: bump ALF to [v0.4.0](https://github.com/AliceO2Group/ALF/releases)

### Other roles
- [test]: add checks for AliECS components and support for setups running behind HTTP proxy


## 0.3.0 (2019-11-20)

### Data flow roles
- [flp-readoutcard]: bump ReadoutCard to [v0.14.5](https://github.com/AliceO2Group/ReadoutCard/releases)
- [flp-readout.exe]: bump Readout to [v1.1.1](https://github.com/AliceO2Group/Readout/blob/v1.1.1/doc/releaseNotes.md#v111---12112019)

### Quality Control roles
- [qc-gui]: bump Quality Control GUI to [v1.6.4](https://github.com/AliceO2Group/WebUi/releases)
- [quality-control]: bump Quality Control framework to [v0.19.5](https://github.com/AliceO2Group/QualityControl/releases)

### Control roles
- [control-core|node]: bump AliECS to [v0.11.1](https://github.com/AliceO2Group/Control/releases)
- [control-gui]: bump AliECS GUI to [v1.4.3](https://github.com/AliceO2Group/WebUi/releases)

### Slow Control roles
- no changes

### Infrastructure roles
##### Configuration
- `ReadoutCard` parameters changed (see [here](docs/flp-suite/upgrade-0.2.1-to-0.3.0.md#readoutcard-changes-in-configuration-parameters))
- `readout` parameters changed (see [here](docs/flp-suite/upgrade-0.2.1-to-0.3.0.md#readout-changes-in-configuration-parameters))

##### Logging
- [infologger-base]: bump InfoLogger to [v1.3.7](https://github.com/AliceO2Group/InfoLogger/blob/v1.3.7/doc/releaseNotes.md#v137---13112019)
- [infologger-gui]: bump InfoLogger web-based GUI to [v1.1.9](https://github.com/AliceO2Group/WebUi/releases)

##### Monitoring
- [grafana]: minor fixes in Readout dashboard 

### Other roles
- [test]: added checks for Monitoring components

### Other changes
- user `flp` is created with default password and has `sudo` access via `wheel` group
- new `o2-flp-setup` installer supporting multinode deployments (NEW) (PREVIEW) 
- fixed bug when deploying Apache Mesos in single node setups (after reinstallation, `mesos-master` would no longer be started at boot time)
- changed to `unixy` Ansible output format
- improvements to tasks reporting when changes are made in target systems (removed numerous false positives) 

## 0.2.1 (2019-10-23)

### Other roles
- [nginx]: bump nginx to 1.16.1 and use package provided in epel


## 0.2.0 (2019-10-18)

### Data flow roles
- [flp-readoutcard]: bump ReadoutCard to [v0.13.0](https://github.com/AliceO2Group/ReadoutCard/releases)
- [flp-readout.exe]: bump Readout to [v1.0.9](https://github.com/AliceO2Group/Readout/blob/master/doc/releaseNotes.md#v109---11102019)
- [flp-readout.exe-config]: add auto-generated Readout config files based on available resources

### Quality Control roles
- [qc-gui]: bump Quality Control GUI to [v1.6.3](https://github.com/AliceO2Group/WebUi/releases)
- [quality-control]: bump Quality Control framework to [v0.19.2](https://github.com/AliceO2Group/QualityControl/releases)

### Control roles
- [control-core]: bump AliECS to [v0.11.0](https://github.com/AliceO2Group/Control/releases)
- [control-gui]: bump AliECS GUI to [v1.4.1](https://github.com/AliceO2Group/WebUi/releases)
- [control-node]: bump AliECS to [v0.11.0](https://github.com/AliceO2Group/Control/releases)

### Slow Control roles
- [dim]: deploys DIM [v20r25](https://dim.web.cern.ch/dim_v20r25.readme.txt), including DIM DNS and WebDID (NEW) ("3<sup>rd</sup> party")
- [flp-alf]: deploys ALF [v0.3.1](https://github.com/AliceO2Group/ALF/releases) (NEW)

### Infrastructure roles
##### Configuration
No changes. 

##### Logging
- [infologger-base]: bump InfoLogger to [v1.3.5](https://github.com/AliceO2Group/InfoLogger/blob/v1.3.5/doc/releaseNotes.md#v135---06092019)
- [infologger-gui]: bump InfoLogger web-based GUI to [v1.1.7](https://github.com/AliceO2Group/WebUi/releases)
- [mariadb]: bump MariaDB to [v5.5.64](https://mariadb.com/kb/en/library/mariadb-5564-release-notes/) ("3<sup>rd</sup> party")

##### Monitoring
No changes.

### Other roles
- [test]: test and diagnosis role, currently includes checks for ALF, Consul, GUIs, InfoLogger, Quality Control and ReadoutCard (NEW)

### Other changes
- improvements for DHCP setups where the IP address might change
- improvements for multinode setups
- improvements for setups running behind HTTP proxy
- all roles now use explicit package versions when installing software for better reproducibility
- recommended Ansible version bumped to v2.8.4


## 0.1.0 (2019-07-25)

### Data flow roles
- [flp-readoutcard]:deploys ReadoutCard [v0.11.7](https://github.com/AliceO2Group/ReadoutCard/releases)
- [flp-readout.exe]: deploys Readout [v1.0.1](https://github.com/AliceO2Group/Readout/blob/master/doc/releaseNotes.md#v101---13062019)
- [flp-readout.exe-config]: configures Readout

### Quality Control roles
- [ccdb]: deploys CCDB local flavour v1.0.4 
- [ccdb-sql]: deploys CCDB PostgreSQL flavour v1.0.4
- [postgresql]: deploys PostgreSQL database ("3<sup>rd</sup> party") 
- [qc-gui]: deploys Quality Control GUI [v1.5.0](https://github.com/AliceO2Group/WebUi/releases)
- [quality-control]: deploys Quality Control framework [v0.16.0](https://github.com/AliceO2Group/QualityControl/releases)

### Control roles
- [ansible-zookeeper]: deploys Apache Zookeeper [v3.4.14](https://zookeeper.apache.org/doc/r3.4.14/releasenotes.html) ("3<sup>rd</sup> party")
- [ansible-mesos]: deploys Apache Mesos [v1.7.2](https://issues.apache.org/jira/secure/ReleaseNote.jspa?projectId=12311242&version=12344656) ("3<sup>rd</sup> party")
- [control-base]: prepares AliECS deployment
- [control-core]: deploys AliECS server-side component [v0.9.3](https://github.com/AliceO2Group/Control/releases)
- [control-gui]: deploys AliECS GUI [v1.2.0](https://github.com/AliceO2Group/WebUi/releases)
- [control-node]: deploys AliECS client-side component [v0.9.3](https://github.com/AliceO2Group/Control/releases)

### Infrastructure roles
##### Configuration
- [consul]: deploys Consul [v1.5.1](https://github.com/hashicorp/consul/blob/master/CHANGELOG.md#151-may-22-2019) ("3<sup>rd</sup> party")

##### Logging
- [infologger-base]: installs InfoLogger software [v1.3.2](https://github.com/AliceO2Group/InfoLogger/blob/v1.3.2/doc/releaseNotes.md#v132---17062019)
- [infologger-client]: deploys InfoLogger client-side component [v1.3.2](https://github.com/AliceO2Group/InfoLogger/blob/v1.3.2/doc/releaseNotes.md#v132---17062019)
- [infologger-db]: deploys InfoLogger database [v1.3.2](https://github.com/AliceO2Group/InfoLogger/blob/v1.3.2/doc/releaseNotes.md#v132---17062019)
- [infologger-gui]: deploys InfoLogger web-based GUI [v1.1.5](https://github.com/AliceO2Group/WebUi/releases)
- [infologger-server]: deploys InfoLogger server-side component [v1.3.2](https://github.com/AliceO2Group/InfoLogger/blob/v1.3.2/doc/releaseNotes.md#v132---17062019)
- [mariadb]: deploys MariaDB database ("3<sup>rd</sup> party") 

##### Monitoring
- [grafana]: deploys Grafana (visualization) [v6.2.5](https://github.com/grafana/grafana/blob/master/CHANGELOG.md#625-2019-06-25) ("3<sup>rd</sup> party")
- [influxdb]: deploys InfluxDB (storage) [v1.7.6](https://docs.influxdata.com/influxdb/v1.7/about_the_project/releasenotes-changelog/) ("3<sup>rd</sup> party")
- [telegraf]: deploys Telegraf (sensors) [v1.10.4](https://github.com/influxdata/telegraf/blob/master/CHANGELOG.md#v1104-2019-05-14) ("3<sup>rd</sup> party")

### Other roles
- [basevars]: parameters shared across roles and autodiscovery of CRUs 
- [flp-standalone]: metarole for single node deployment
- [misc]: installs firefox and X11 for out-of-the-box experience on CERN Cloud
- [nginx]: deploys O2 Web UI directory webpage for simple access to the different GUIs
- [nodejs]: deploys NodeJS v10.x ("3<sup>rd</sup> party")
- [o2-base]: sets up the different YUM repos required for software installation

### Other changes
- [ci] basic tests on openstack
- [docs]: initial installation and developer documentation
- [utils]: installation script for single node deployment
