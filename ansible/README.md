Ansible configuration
===

This is the global Ansible configuration of the FLP Suite, making extensive use of roles.

About Ansible
---
Ansible is an configuration management developed and promoted by Red Hat. It is similar to Puppet and Chef. The goal is to use Ansible in O2 to automate the deployment and configuration of the different tools used (monitoring, configuration, control, logging, etc). Ansible documentation can be found [here](http://docs.ansible.com/ansible/latest/index.html). 

Basic Ansible principles
---
* **Push vs Pull**: by default, Ansible runs in push mode. A controlling node connects via ssh to a target node and executes a sequence of operations.
* **Roles**: reusable blocks that group tasks, variables and configuration files. O2 examples include influxdb, collectd or infologgerdb.
* **Inventories**: list of hosts that belong to a specific setup or cluster.
* **Playbooks**: assign roles to hosts or groups of hosts.

Add a new role quick list
---
1. Create a new git branch
2. Develop your role
3. Add your role to a playbook
4. Create a merge request

Instructions for users
---
1. [O²/FLP Suite software installation](docs/users/flp-suite/flp-suite-installation.md)
2. [List of services running in FLP Suite](docs/users/flp-suite/flp-suite-services.md)


Instructions for developers
---

**Ansible**
1. [Getting started](docs/developers/ansible/getting-started.md)
2. [Create your own inventory for testing (CERN Openstack)](docs/developers/ansible/create-test-inventory.md)
3. [Setup authentication](docs/developers/ansible/authentication.md)
4. [Create a role](docs/developers/ansible/create-role.md)
5. [Bringing roles and hosts together in playbooks](docs/developers/ansible/create-playbook.md)
6. [Best practices and guidelines](docs/developers/ansible/best-practices.md)
7. [Examples of executing Ansible playbooks](docs/developers/ansible/examples-commands.md)
8. [Linting](docs/developers/ansible/lint.md)

**FLP Suite**
1. [Testing FLP Suite releases](docs/developers/flp-suite/test-flp-suite.md)
9. [Creating a diagnosis test for a role](docs/developers/flp-suite/test-role.md)


Official inventories
---
Inventories define the list of target machines. The following inventories are currently defined:

* **[inventory-vertical-slice](inventory-vertical-slice)**: O2/FLP contribution to the Vertical Slice, located in CR1.
