# Release Notes
All notable changes to the o2-flp-setup will be documented in this file.   

## 0.4.1 (2020-03-30)
- installation script should install o2-flp-setup binary on CC8
- fix issue with ~/.ssh/known_hosts file

## 0.4.0 (2020-03-27)
- refactor ssh checks
- handle ssh host key identification


## 0.3.1 (2020-03-03)
- fix installation print message on fail
- fix issue with proxy in CR1
- use go-yaml to create group_vars files


## 0.3.0 (2020-02-28)
- add timeout to reboot subcommand
- support paths starting with tilde (~)
- add --path flag for generate subcommand to ouput inventory to the specified path
- workaround issue with "reference has changed concurrently"
- status prints branch/tag name along with the short hash
- fix issue with double printing module's role
- add dev and post-installation as non default modules


## 0.2.0 (2020-02-13)
- installer should continue deployment on unreachable nodes
- enable readout-autoconf module
- add release target in Makefile
- store binaries on S3
- change description for --proxy flag
- stop setting control_mesos_hostname on every deployment
- add reboot, poweroff, sysupdate, ping, and exec subcommands
- mark version as improperly when building without make


## 0.1.0 (2020-01-21)
- initial release for o2-flp-setup installer