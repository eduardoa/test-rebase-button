## o2-flp-setup deploy

deploy O²/FLP suite

### Synopsis

The deploy command performs a deployment of the FLP Suite on the specified hosts.

```
o2-flp-setup deploy [flags]
```

### Options

```
      --debug                print debug info
  -e, --extra-vars strings   set Ansible variables
      --flps strings         hosts to deploy as FLPs (comma-separated list of hostnames)
      --head string          host to deploy as head node
  -h, --help                 help for deploy
  -i, --inventory string     specify inventory directory path
      --modules strings      modules to install
      --proxy string         proxy setting
      --user string          user (default "root")
      --verbose              ansible verbose output
```

### Options inherited from parent commands

```
      --config string   optional configuration file for o2-flp-setup (default $HOME/.config/o2-flp-setup/settings.yaml)
```

### SEE ALSO

* [o2-flp-setup](o2-flp-setup.md)	 - flpsetup

###### Auto generated by spf13/cobra on 28-Jan-2020
