module gitlab.cern.ch/AliceO2Group/system-configuration/utils/o2-flp-setup

go 1.13

require (
	github.com/aws/aws-sdk-go v1.28.9
	github.com/fatih/color v1.9.0
	github.com/google/uuid v1.1.1
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v0.0.5
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.6.2
	github.com/xlab/treeprint v0.0.0-20181112141820-a009c3971eca
	golang.org/x/crypto v0.0.0-20200115085410-6d4e4cb37c7d
	gopkg.in/src-d/go-git.v4 v4.13.1
	gopkg.in/yaml.v3 v3.0.0-20191120175047-4206685974f2
)
