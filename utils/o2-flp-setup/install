#!/usr/bin/env bash

C_WARN="\033[1;33m==> WARNING: \033[0m"
C_YELL="\033[1;33m==> \033[0m"
C_QUEST=$C_YELL
C_ERR="\033[1;31m==> ERROR: \033[0m"
C_RED="\033[1;31m==> \033[0m"
C_MSG="\033[1;32m==> \033[0m"
C_ITEM="\033[1;34m  -> \033[0m"

bail()
{
    echo -e "${C_ERR}$1"
    echo -e "${C_RED}o2-flp-setup will now quit."
}

message()
{
    echo -e "${C_MSG}$1"
}


setup()
{

    # (1) SUPPORTED OS
    message "Checking for supported operating system..."
    IS_OS=$(uname -s)

    
    # (2) CREATE SCRIPT  
    createScript $IS_OS
    if [[ "$?" != 0 ]]; then
        bail "could not create binary."
        exit $?
    fi

    message "Installation Completed"
}

createScript()
{
IS_OS="$1"
O2_BIN_FILE="/usr/local/bin/o2-flp-setup"
O2_BIN_SHORTNAME="/usr/local/bin/flpsetup"

message "Creating binary at /usr/local/bin..."
message "You may need to provide sudo password"
sudo bash -c 'cat > "$0"' $O2_BIN_FILE <<EOM 
#!/usr/bin/env bash

O2_FLP_SETUP_DATA_DIR="\$HOME/.local/share/o2-flp-setup"
O2_FLP_SETUP_BIN_PATH="\$O2_FLP_SETUP_DATA_DIR/bin/o2-flp-setup"

if [[ $IS_OS == "Darwin" ]]; then
    O2_FLP_SETUP_OLD_BIN_PATH="\$O2_FLP_SETUP_DATA_DIR/system-configuration/utils/o2-flp-setup/bin/o2-flp-setup.macos"
elif [[  $IS_OS == "Linux" ]]; then
    O2_FLP_SETUP_OLD_BIN_PATH="\$O2_FLP_SETUP_DATA_DIR/system-configuration/utils/o2-flp-setup/bin/o2-flp-setup.linux"
else
    bail "o2-flp-setup doesn't support $IS_OS"
    exit 1
fi


C_WARN="\033[1;31m==> WARNING: \033[0m"
C_YELL="\033[1;33m==> \033[0m"
C_QUEST=\$C_YELL
C_ERR="\033[1;31m==> ERROR: \033[0m"
C_RED="\033[1;31m==> \033[0m"
C_MSG="\033[1;32m==> \033[0m"
C_ITEM="\033[1;34m  -> \033[0m"

bail()
{
    echo -e "\${C_ERR}\$1"
    echo -e "\${C_RED}o2-flp-setup will now quit."
}

message()
{
    echo -e "\${C_MSG}\$1"
}

check_dependencies()
{
    message "Checking dependencies..."

    which git &>/dev/null
    HAS_GIT="\$?"

    which ansible &>/dev/null
    HAS_ANSIBLE="\$?"

    which ssh &>/dev/null
    HAS_OPENSSH="\$?"


    if [[ "\$HAS_GIT" != "0" ||
          "\$HAS_ANSIBLE" != "0" ||
          "\$HAS_OPENSSH" != "0" ]]; then
        if [[ "\$1" == "Linux" ]]; then
            centos_version=\$(rpm --eval %{centos_ver} 2>/dev/null)
            if [[ "\$centos_version" == "7" ]]; then
                message "Installing dependencies..."
                sudo yum -y install git python3 python-{cryptography,six,jinja2} PyYAML nmap-ncat epel-release
                sudo yum --disablerepo=* --enablerepo=epel -y install ansible
                if [[ "\$?" != 0 ]]; then
                    bail \$'could not install dependencies. To install the required packages manually, try running the following commands \033[1mas root\033[0m:\nyum -y install git python-{cryptography,six,jinja2} PyYAML nmap-ncat epel-release\nyum --disablerepo=* --enablerepo=epel -y install ansible'
                    exit 6
                fi
            elif [[ "\$centos_version" == "8" ]]; then
                message "Installing dependencies..."
                sudo yum -y install git python3 python3-jmespath epel-release
                sudo yum --disablerepo=* --enablerepo=epel -y install ansible
                if [[ "\$?" != 0 ]]; then
                    bail \$'could not install dependencies. To install the required packages manually, try running the following commands \033[1mas root\033[0m:\nyum -y install git python3 python3-jmespath epel-release\nyum --disablerepo=* --enablerepo=epel -y install ansible'
                    exit 6
                fi
            else
                echo -e "You should install Ansible and git, in order to run o2-flp-setup"
            fi
        elif [[ "\$1" == "Darwin" ]]; then
            message "Installing dependencies..."
            brew install git ansible 
            if [[ "\$?" != 0 ]]; then
                bail \$'could not install dependencies. To install the required packages manually, try running the following command:\nbrew install git ansible'
                exit 6
            fi
        fi
    fi
}

install () {
    # (1) DEPENDENCIES
    check_dependencies $IS_OS

    # (2) DIRECTORIES
    message "Setting up directories..."
    E_BAD_PATH=3

    if [[ ! -d \$O2_FLP_SETUP_DATA_DIR ]]; then
        message "Creating data directory at \$O2_FLP_SETUP_DATA_DIR..."
        mkdir -p \$O2_FLP_SETUP_DATA_DIR
        if [[ "\$?" != 0 ]]; then
            bail "cannot create o2-flp-setup data directory: \$O2_FLP_SETUP_DATA_DIR."
            exit \$E_BAD_PATH
        fi
    fi
    if [[ ! -d \$O2_FLP_SETUP_DATA_DIR/bin ]]; then
        message "Creating bin directory at \$O2_FLP_SETUP_DATA_DIR/bin..."
        mkdir -p \$O2_FLP_SETUP_DATA_DIR/bin
        if [[ "\$?" != 0 ]]; then
            bail "cannot create o2-flp-setup bin directory: \$O2_FLP_SETUP_DATA_DIR/bin."
            exit \$E_BAD_PATH
        fi
    fi
    if [[ ! -w \$O2_FLP_SETUP_DATA_DIR ]]; then
        bail "data directory \$O2_FLP_SETUP_DATA_DIR is not writable."
        exit \$E_BAD_PATH
    fi
    if [[ ! -w \$O2_FLP_SETUP_DATA_DIR/bin ]]; then
        bail "bin directory \$O2_FLP_SETUP_DATA_DIR/bin is not writable."
        exit \$E_BAD_PATH
    fi

    # (3) GIT REPOSITORIES
    message "Fetching system configuration..."

    O2_FLP_SETUP_ANSIBLE_REPOSITORY_SSH="ssh://git@gitlab.cern.ch:7999/AliceO2Group/system-configuration.git"
    O2_FLP_SETUP_ANSIBLE_REPOSITORY_HTTPS="https://gitlab.cern.ch/AliceO2Group/system-configuration.git"
    O2_FLP_SETUP_ANSIBLE_REPOSITORY_PROVIDER="CERN GitLab"

    git_clone()
    {
        SSH_URL="\$1"
        HTTPS_URL="\$2"
        KEYS_URL="\$3"
        DEST_PATH="\$4"
        REPO_PROVIDER="\$5"
        WORK_DIR=\$PWD
        cd ~

        if [[ -d "\$DEST_PATH" ]]; then
            while true; do
                echo -ne "\${C_QUEST}\${DEST_PATH} already exists. Overwrite [y/n]?\n\${C_QUEST}------------------------------------\n\${C_QUEST}"
                read yn
                case \$yn in
                    [Yy]* ) rm -rf "\$DEST_PATH" &>/dev/null; break;;
                    [Nn]* ) message "Skipping repository setup..."; return 0;;
                    * ) echo -e "\${C_QUEST}Please answer yes or no [y/n].";;
                esac
            done
        fi
        git clone "\$SSH_URL" "\$DEST_PATH"
        if [[ "\$?" != 0 ]]; then
            echo -e "\${C_WARN}cannot fetch system-configuration repository via SSH. Your SSH key must be listed on $KEYS_URL for this to work.\n\${C_YELL}Falling back to HTTPS, you will be prompted for your \$REPO_PROVIDER credentials."
            git clone "\$HTTPS_URL" "\$DEST_PATH"
            if [[ "\$?" != 0 ]]; then
                exit 4
            fi
        fi
        cd \$WORK_DIR
    }

    git_clone \$O2_FLP_SETUP_ANSIBLE_REPOSITORY_SSH \$O2_FLP_SETUP_ANSIBLE_REPOSITORY_HTTPS "https://gitlab.cern.ch/profile/keys" "\$O2_FLP_SETUP_DATA_DIR/system-configuration" "\${O2_FLP_SETUP_ANSIBLE_REPOSITORY_PROVIDER}"
    if [[ "\$?" != 0 ]]; then
        bail "could not fetch git repository system-configuration. Please make sure that your CERN credentials are correct, and that you are a member of the alice-o2-detector-teams e-group (https://e-groups.cern.ch/e-groups/EgroupsSearch.do?searchValue=alice-o2-detector-teams)."
        exit \$?
    fi

    if [[ ! -w \$O2_FLP_SETUP_DATA_DIR ]]; then
        bail "data directory \$O2_FLP_SETUP_DATA_DIR is not writable."
        exit \$E_BAD_PATH
    fi

    # (5) GIT CHECKOUT 
    latestTag "\$O2_FLP_SETUP_DATA_DIR/system-configuration"

    # (6) Get Binary
    fetchBinary
}

fetchBinary() {
    if [[ -f \$O2_FLP_SETUP_DATA_DIR/system-configuration/utils/o2-flp-setup/binaries.txt ]]; then
        downloadBinary
    elif [[ -x \$O2_FLP_SETUP_OLD_BIN_PATH ]]; then
        cp \$O2_FLP_SETUP_OLD_BIN_PATH \$O2_FLP_SETUP_BIN_PATH
    fi    
    if [[ "\$?" != 0 ]]; then
        bail "could not fetch binary."
        exit \$?
    fi

    chmod +x \$O2_FLP_SETUP_BIN_PATH
    if [[ "\$?" != 0 ]]; then
        exit \$?
    fi
}

latestTag()
{
    GIT_PATH="\$1"
    CURR_DIR=\$PWD
    cd \$GIT_PATH
    latesttag=\$(git describe --tags \$(git rev-list --tags --max-count=1))
    message "Checking out \$latesttag..."
    git checkout \$latesttag  &>/dev/null
    if [[ "\$?" != 0 ]]; then
        bail "could not fetch latest tag for git repository system-configuration."
        exit 5
    fi
    cd \$CURR_DIR
}

uninstall()
{
    while true; do
        echo -ne "\${C_WARN}Do you want to remove for \033[1;31mALL \033[0musers [y/n]?\n\${C_RED}------------------------------------\n\${C_RED} "
        read yn
        case \$yn in
            [Yy]* ) removeForUsers; break;;
            [Nn]* ) rm -rf \$O2_FLP_SETUP_DATA_DIR &>/dev/null;  if [[ "\$?" == 0 ]]; then echo -e "\${C_MSG}Successfully removed system-configuration for \$(whoami)"; break; else exit "\$";fi;;
            * ) echo -e "\${C_RED}Please answer yes or no [y/n].";;
        esac
    done
}

removeForUsers()
{
    while [[ (-f $O2_BIN_FILE  || -f $O2_BIN_SHORTNAME) ]]; do
        REMOVECOMMAND="rm -f $O2_BIN_FILE $O2_BIN_SHORTNAME"
        echo -ne "\${C_WARN}The following command is about to be executed: sudo \$REMOVECOMMAND \n\${C_RED}Do you wish to proceed [y/n]?\n\${C_RED}------------------------------------\n\${C_RED} "
        read yn
        case \$yn in
            [Yy]* ) echo -e  "\${C_RED}You may need to provide sudo password"; sudo \$REMOVECOMMAND; if [[ "\$?" != 0 ]]; then exit "\$?";fi;;
            [Nn]* ) break;;
            * ) echo -e "\${C_RED}Please answer yes or no [y/n].";;
        esac
    done  
    if [[ $IS_OS == "Darwin" ]]; then
        for i in \$(dscl /Local/Default -list /Users uid | awk '\$2 >= 100 && \$0 !~ /^_/ { print \$1 }')
        do
            homedir=\$(dscl  /Local/Default -read /Users/\$i NFSHomeDirectory | awk '{ print \$2 }');
            if [[ -w "\$homedir/.local/share/o2-flp-setup" ]]; then
                while true; do
                    echo -ne "\${C_WARN}Do you want to remove system configuration for \$i [y/n]?\n\${C_RED}------------------------------------\n\${C_RED} "
                    read yn
                    case \$yn in
                        [Yy]* ) sudo rm -rf "\$homedir/.local/share/o2-flp-setup"; echo -e  "\${C_MSG}Successfully removed system-configuration"; break;;
                        [Nn]* ) echo "Skipping user..."; break;;
                        * ) echo -e "\${C_RED}Please answer yes or no [y/n].";;
                    esac
                done
            fi
        done
    elif [[  $IS_OS == "Linux" ]]; then
        for i in \$(getent passwd | awk -F: '{if (\$3 >= 1000 && \$NF!~/\/nologin\$/) print \$1}')
        do
            homedir=\$(getent passwd \$i | cut -d: -f6)
            if [[ -w "\$homedir/.local/share/o2-flp-setup" ]]; then
                while true; do
                    echo -ne "\${C_WARN}Do you want to remove system configuration for \$i [y/n]?\n\${C_RED}------------------------------------\n\${C_RED} "
                    read yn
                    case \$yn in
                        [Yy]* ) sudo rm -rf "\$homedir/.local/share/o2-flp-setup"; echo -e "\${C_MSG}Successfully removed system-configuration"; break;;
                        [Nn]* ) echo "Skipping user..."; break;;
                        * ) echo -e "\${C_RED}Please answer yes or no [y/n].";;
                    esac
                done
            fi
        done 
    fi
}

downloadBinary(){
        if [[ $IS_OS == "Darwin" ]]; then
                requestFile \$(grep -i "macos" "\$O2_FLP_SETUP_DATA_DIR/system-configuration/utils/o2-flp-setup/binaries.txt")
        elif [[  $IS_OS == "Linux" ]]; then
                requestFile \$(grep -i "linux" "\$O2_FLP_SETUP_DATA_DIR/system-configuration/utils/o2-flp-setup/binaries.txt")
        fi
}

requestFile() {
    url="\$1" 
    status_code=\$(curl  --head --write-out "%{http_code}\n" --silent --output /dev/null \$url)
    if [[ "\$status_code" -ne 200 ]] ; then
        echo -e "\${C_RED}Binary is not available at \$url. Error code: \$status_code"
        exit 1
    else
        curl -s -o \$O2_FLP_SETUP_BIN_PATH \$url > /dev/null
    fi
}

if [[ ("\$1" == "uninstall" && "\$#" == 1)]]; then
    echo -e "\${C_WARN}Uninstalling..."
    while true; do
        echo -ne "\${C_WARN}Do you want to proceed with the uninstallation [y/n]?\n\${C_RED}------------------------------------\n\${C_RED} "
        read yn
        case \$yn in
            [Yy]* ) uninstall; if [[ (! -f $O2_BIN_FILE  && ! -f $O2_BIN_SHORTNAME) ]]; then echo -e  "\${C_MSG}Successfully uninstalled o2-flp-setup"; fi;  break;;
            [Nn]* ) echo "Skipping uninstall..."; exit 0;;
            * ) echo -e "\${C_RED}Please answer yes or no [y/n].";;
        esac
    done
elif [[ "\$1" == "install" ]]; then
    install
else
    if [[ -x \$O2_FLP_SETUP_BIN_PATH ]]; then
        if [[ "\$1" == "checkout" || "\$1" == "co" ]]; then
            var=\$(\$O2_FLP_SETUP_BIN_PATH  "\$@" | tee /dev/tty)
            if [[ \$var == *"Checked out revision"* ]]; then
                  echo "\$2" > "\$O2_FLP_SETUP_DATA_DIR/gitStatus.txt"
                  fetchBinary
            fi
        else
            \$O2_FLP_SETUP_BIN_PATH  "\$@"
        fi
    else
        if [[ ! -d "\$O2_FLP_SETUP_DATA_DIR" ]]; then
            install
        else
            latestTag "\$O2_FLP_SETUP_DATA_DIR/system-configuration"
            fetchBinary
        fi 
        o2-flp-setup "\$@"
    fi
fi
EOM

if [[ "$?" != 0 ]]; then
    exit 6
fi

sudo chmod +x $O2_BIN_FILE
if [[ "$?" != 0 ]]; then
    exit 6
fi
if [[ ! -L "$O2_BIN_SHORTNAME" ]]; then
    sudo ln -s $O2_BIN_FILE $O2_BIN_SHORTNAME
    if [[ "$?" != 0 ]]; then
        exit 6
    fi
fi
}

setup
o2-flp-setup install
