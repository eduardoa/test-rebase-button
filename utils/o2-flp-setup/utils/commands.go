/*
 * === This file is part of ALICE O² ===
 *
 * Copyright 2019 CERN and copyright holders of ALICE O².
 * Author: Miltiadis Alexis <miltiadis.alexis@cern.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In applying this license CERN does not waive the privileges and
 * immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

package utils

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/fatih/color"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/xlab/treeprint"
	"gitlab.cern.ch/AliceO2Group/system-configuration/utils/o2-flp-setup/app"
)

// Options for running ansible
type Options struct {
	SSHPass    string
	WithTests  string
	BecomePass string
	Inventory  string
	Playbook   string
	User       string
	Module     string
	ModuleArgs []string
	Tags       []string
	ExtraVars  []string
}

var options Options

var localpath = fmt.Sprintf("%s/.local/share/o2-flp-setup/system-configuration", os.Getenv("HOME"))

// Deploy performs a deploy operation
func Deploy(cmd *cobra.Command, args []string) {

	err := checkRepo()
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	modules, _ := cmd.Flags().GetStringSlice("modules")
	nodes := []string{}

	// populate viper based on flags used
	cmd.Flags().Visit(checkFlags)

	extravars, _ := cmd.Flags().GetStringSlice("extra-vars")
	options.ExtraVars = extravars

	if viper.IsSet("head") && viper.IsSet("flps") && !viper.IsSet("inventory") {
		nodes = append(viper.GetStringSlice("flps"), viper.GetString("head"))
		nodes = SliceUniqMapString(nodes)

		dirPath, err := createInventory()
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}

		options.Inventory = dirPath

		//remove temp inventory
		defer os.RemoveAll(options.Inventory)

	} else if viper.IsSet("inventory") {
		var err error

		nodes, err = readInventory()
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}

		options.Inventory = viper.GetString("inventory")
	} else {
		fmt.Println(`In order to start a deploy you need to set the "--head" and "--flps" flags, 
or provide an inventory using the "--inventory" flag.`)
		cmd.Help()
		os.Exit(1)
	}

	for _, node := range nodes {
		err := prepareNode(node)
		if err != nil {
			fmt.Println(err.Error())
		}
	}

	options.Playbook = filepath.Join(localpath, "ansible/flp-multinode.yml")

	if len(modules) == 1 && modules[0] == "dataflow" {
		// pass
	} else if len(modules) != 0 {
		for _, module := range modules {
			if module == "logging" || module == "control" || module == "qc" {
				modules = append(modules, "nginx")
				break
			}
		}
	}

	options.Tags = modules

	if viper.GetString("user") != "root" {
		options.BecomePass = "--ask-become-pass"
	}
	options.User = viper.GetString("user")

	err = playbookRun()
	if err != nil {
		os.Exit(1)
	}
	fmt.Println("Please reboot all machines for changes to take effect.")

}

// Checkout to the tag provided
func Checkout(cmd *cobra.Command, args []string) {
	err := checkRepo()
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	repo := Repo{LocalPath: localpath, ReferenceName: args[0]}

	for {
		err = repo.update()
		if err == nil {
			fmt.Printf("\nChecked out revision %s\n", repo.ReferenceName[strings.LastIndex(repo.ReferenceName, "/")+1:])
			os.Exit(0)
		} else if err.Error() != "reference has changed concurrently" {
			fmt.Printf("Checkout failed. Error: %s\n", err.Error())
			os.Exit(1)
		}
	}
}

// Diagnose run a playbook with test role
func Diagnose(cmd *cobra.Command, args []string) {
	err := checkRepo()
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	// populate viper based on flags used
	cmd.Flags().Visit(checkFlags)

	nodes := append(viper.GetStringSlice("flps"), viper.GetString("head"))
	nodes = SliceUniqMapString(nodes)

	for _, node := range nodes {
		err := prepareNode(node)
		if err != nil {
			fmt.Println(err.Error())
		}
	}

	dirPath, err := createInventory()
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	options.Inventory = dirPath

	//remove temp inventory
	defer os.RemoveAll(options.Inventory)

	options.ExtraVars = append(options.ExtraVars, "test=true")

	if err := createTestPlaybook(); err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	//remove playbook created
	defer os.RemoveAll(options.Playbook)

	if viper.GetString("user") != "root" {
		options.BecomePass = "--ask-become-pass"
	}
	options.User = viper.GetString("user")

	err = playbookRun()
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}

// GenerateInventory for Ansible
func GenerateInventory(cmd *cobra.Command, args []string) {
	// populate viper based on flags used
	cmd.Flags().Visit(checkFlags)

	dirPath, err := createInventory()
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	fmt.Printf("\nInventory has successfully generated: %s\n", dirPath)
}

// ListModules prints a tree with modules and the roles for each module
func ListModules() {
	err := checkRepo()
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	err = getModules()
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	tree := treeprint.New()
	defMod := tree.AddBranch("Default Modules")
	buildTree(viper.GetStringSlice("defaultModules"), defMod)
	addMod := tree.AddBranch("Additional Modules")
	buildTree(viper.GetStringSlice("additionalModules"), addMod)
	fmt.Println(tree.String())
}

//Status prints version of the binary and system-configuration revision
func Status(cmd *cobra.Command, args []string) {

	repo := Repo{LocalPath: localpath}
	SysConfBuild = repo.resolveShortRevision()

	gitStatus := filepath.Join(filepath.Dir(localpath), "gitStatus.txt")
	byteGitStatusValue, err := ioutil.ReadFile(gitStatus)
	if err == nil {
		SysConfBuild = strings.TrimSuffix(string(byteGitStatusValue), "\n") + "-" + SysConfBuild
	}

	color.Set(color.FgHiWhite)
	fmt.Print(app.NAME + " *** ")
	color.Set(color.FgHiGreen)
	fmt.Printf("The ALICE %s Tool\n", app.PRETTY_FULLNAME)
	color.Unset()
	fmt.Printf(`
version:                             %s
system configuration revision:       %s 
`, color.HiGreenString(VersionBuild), color.HiGreenString(SysConfBuild))

}

//AdhocOperations performs ansible ad-hoc commands
func AdhocOperations(cmd *cobra.Command, command string, moduleArgs []string) {

	// populate viper based on flags used
	cmd.Flags().Visit(checkFlags)
	nodes := []string{}

	if viper.IsSet("head") && viper.IsSet("flps") && !viper.IsSet("inventory") {
		nodes = append(viper.GetStringSlice("flps"), viper.GetString("head"))
		nodes = SliceUniqMapString(nodes)
		options.Inventory = strings.Join(nodes, ",")
		ensureTrailingComma(&options.Inventory)

	} else if viper.IsSet("inventory") {
		var err error

		nodes, err = readInventory()
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}
		options.Inventory = viper.GetString("inventory")

	} else {
		fmt.Printf(`In order to %s you need to set the "--head" and "--flps" flags, 
or provide an inventory using the "--inventory" flag.`, cmd.Use)
		cmd.Help()
		os.Exit(1)
	}

	for _, node := range nodes {
		err := prepareNode(node)
		if err != nil {
			fmt.Println(err.Error())
		}
	}

	options.Module = command
	options.ModuleArgs = moduleArgs

	options.User = viper.GetString("user")

	if options.User != "root" {
		options.BecomePass = "--ask-become-pass"
	}

	err := adhocRun()
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

}
