/*
 * === This file is part of ALICE O² ===
 *
 * Copyright 2019 CERN and copyright holders of ALICE O².
 * Author: Miltiadis Alexis <miltiadis.alexis@cern.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In applying this license CERN does not waive the privileges and
 * immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

package utils

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/viper"
	"golang.org/x/crypto/ssh"
	kh "golang.org/x/crypto/ssh/knownhosts"
)

const (
	timeoutErr    = "operation timed out"
	conRefusedErr = "connection refused"
	wrongHostErr  = "no such host"
)

type hostKeyCheck struct {
	// key      ssh.PublicKey
	FilePath      string
	LocalHostKey  string
	RemoteHostKey string
}

type addr struct{ host, port string }

// check checks a key against the local host key found in known_hosts file.
func (f *hostKeyCheck) check(address string, remote net.Addr, remoteKey ssh.PublicKey) error {

	host, port, err := net.SplitHostPort(remote.String())
	if err != nil {
		return fmt.Errorf("knownhosts: SplitHostPort(%s): %w", remote, err)
	}

	hostToCheck := addr{host, port}
	if address != "" {
		// Give preference to the hostname if available.
		host, port, err := net.SplitHostPort(address)
		if err != nil {
			return fmt.Errorf("knownhosts: SplitHostPort(%s): %w", address, err)
		}

		hostToCheck = addr{host, port}
	}
	f.RemoteHostKey = kh.Line([]string{address, remote.String()}, remoteKey)

	return f.checkAddr(hostToCheck, remoteKey)
}

// checkAddr checks if we can find the given public key for the
// given address.  If we only find an entry for the IP address,
// or only the hostname, then this still succeeds.
func (f *hostKeyCheck) checkAddr(a addr, remoteKey ssh.PublicKey) error {
	read, err := ioutil.ReadFile(f.FilePath)
	if err != nil {
		return err
	}

	scanner := bufio.NewScanner(strings.NewReader(string(read)))
	var hostKey ssh.PublicKey
	for scanner.Scan() {
		fields := strings.Split(scanner.Text(), " ")
		if len(fields) != 3 {
			continue
		}
		if strings.Contains(fields[0], a.host) {
			var err error
			hostKey, _, _, _, err = ssh.ParseAuthorizedKey(scanner.Bytes())
			if err != nil {
				return err
			}
			f.LocalHostKey = scanner.Text()
			// If the remote host starts using a different, unknown key type, we
			// also interpret that as a mismatch.
			if !bytes.Equal(hostKey.Marshal(), remoteKey.Marshal()) {
				newContents := strings.Replace(string(read), f.LocalHostKey, f.RemoteHostKey, -1)

				err = ioutil.WriteFile(filepath.Join(os.Getenv("HOME"), ".ssh", "known_hosts"), []byte(newContents), 0)
				if err != nil {
					return err
				}
				return nil
			}
			break // scanning line by line, first occurrence will be returned
		}
	}

	if hostKey == nil {
		newContents := string(read) + f.RemoteHostKey + "\n"
		err = ioutil.WriteFile(filepath.Join(os.Getenv("HOME"), ".ssh", "known_hosts"), []byte(newContents), 0)
		if err != nil {
			return err
		}
	}

	return nil
}

func copySSHKeys(node string) {

	commandName := "ssh-copy-id"
	commandArgs := []string{fmt.Sprintf("%s@%s", viper.GetString("user"), node)}
	err := runCmd(commandName, commandArgs)
	if err != nil {
		options.SSHPass = "--ask-pass"
		fmt.Printf("\nCould not copy SSH key. You will be prompted for the password for user %s.\n", viper.GetString("user"))
	}
}

func retrieveSSHKeys(signers *[]ssh.Signer) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		// Skip directories
		if info.IsDir() {
			return nil
		}

		key, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}

		// Create the Signer for this private key
		signer, err := ssh.ParsePrivateKey(key)
		if err != nil {
			return nil
		}

		*signers = append(*signers, signer)

		return nil
	}
}

// HostKeyCheckCallback returns a function for use in
// ClientConfig.HostKeyCallback to accept a path of known_hosts file.
func HostKeyCheckCallback(file string) ssh.HostKeyCallback {
	hkc := &hostKeyCheck{FilePath: file}
	return hkc.check
}

func prepareNode(node string) error {

	var signers []ssh.Signer

	err := filepath.Walk(filepath.Join(os.Getenv("HOME"), ".ssh"), retrieveSSHKeys(&signers))
	if err != nil {
		copySSHKeys(node)
		return err
	}

	knownHostsPath := filepath.Join(os.Getenv("HOME"), ".ssh", "known_hosts")

	sshConfig := &ssh.ClientConfig{
		User: viper.GetString("user"),
		Auth: []ssh.AuthMethod{
			// Use the PublicKeys method for remote authentication.
			ssh.PublicKeys(signers...),
		},
		HostKeyCallback: HostKeyCheckCallback(knownHostsPath),
	}

	addr := fmt.Sprintf("%s:22", node)

	client, err := ssh.Dial("tcp", addr, sshConfig)
	if err != nil {
		switch {
		case strings.Contains(err.Error(), timeoutErr), strings.Contains(err.Error(),
			conRefusedErr), strings.Contains(err.Error(), wrongHostErr):
			return fmt.Errorf("target %s is unreachable", node)
		default:
			copySSHKeys(node)
			return nil
		}
	}
	defer client.Close()

	return nil
}
