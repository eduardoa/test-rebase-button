/*
 * === This file is part of ALICE O² ===
 *
 * Copyright 2019 CERN and copyright holders of ALICE O².
 * Author: Miltiadis Alexis <miltiadis.alexis@cern.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In applying this license CERN does not waive the privileges and
 * immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

package utils

import (
	"fmt"
	"io/ioutil"
	"path/filepath"

	"github.com/spf13/viper"
	"github.com/xlab/treeprint"
	"gopkg.in/yaml.v3"
)

// Modules maps module name to module roles
type Modules map[string][]string

// Plays is a list of plays
type Plays []Play

// Play is a struct where we unmarshal the roles needed for every module
type Play struct {
	Hosts       string `yaml:"hosts"`
	Roles       []Role `yaml:"roles"`
	Environment string `yaml:"environment,omitempty"`
	Become      bool   `yaml:"become"`
}

// Role is a key value struct
type Role struct {
	AnsibleRole string   `yaml:"role"`
	Tags        []string `yaml:"tags,omitempty"`
}

var modules = make(Modules)

func createTestPlaybook() error {
	var ps Plays

	ps = append(ps, Play{Hosts: "all", Roles: []Role{Role{AnsibleRole: "test"}}})

	b, err := yaml.Marshal(ps)
	if err != nil {
		return err
	}
	if viper.GetBool("debug") {
		fmt.Printf("\n%s\n", b)
	}

	options.Playbook = filepath.Join(localpath, "ansible/flp-multinode-temp-test.yaml")

	ioutil.WriteFile(options.Playbook, b, 0644)

	return nil
}

//MarshalYAML based on yaml.v3 package
func (p Play) MarshalYAML() (interface{}, error) {
	type _Play Play

	play := _Play(p)

	if viper.GetString("user") == "root" {
		play.Become = true
	}
	if viper.GetString("proxy") != "" {
		play.Environment = "{{ proxy_env }}"
	}
	return play, nil
}

//getModules from flp-multinode.yml
func getModules() error {
	var ps Plays

	bytes, err := ioutil.ReadFile(filepath.Join(localpath, "ansible/flp-multinode.yml"))
	if err != nil {
		return err
	}

	if err := yaml.Unmarshal(bytes, &ps); err != nil {
		return err
	}
	return nil
}

// UnmarshalYAML populates the modules map with the roles for each module
func (r *Role) UnmarshalYAML(unmarshal func(interface{}) error) (err error) {
	type _role Role
	role := _role{}
	err = unmarshal(&role)
	if err == nil {
		*r = Role(role)
		for _, tag := range role.Tags {
			if _, ok := modules[tag]; ok {
				modules[tag] = append(modules[tag], role.AnsibleRole)
			} else {
				modules[tag] = []string{role.AnsibleRole}
			}
		}
	}
	return
}

// build tree branch for modules
func buildTree(parents []string, treebranch treeprint.Tree) {
	for _, v := range parents {
		encountered := map[string]bool{}

		var moduleName treeprint.Tree
		moduleName = treebranch.AddBranch(v)

		// we are using modules from modules.go
		for _, i := range modules[v] {
			if _, ok := encountered[i]; !ok {
				moduleName.AddNode(i)
				encountered[i] = true
			}
		}
	}
}
