/*
 * === This file is part of ALICE O² ===
 *
 * Copyright 2019 CERN and copyright holders of ALICE O².
 * Author: Miltiadis Alexis <miltiadis.alexis@cern.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In applying this license CERN does not waive the privileges and
 * immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

package utils

import (
	"strings"
)

// PlaybookRun is responsible for creating and running the ansible playbook command
func playbookRun() error {

	var commandArgs []string

	if options.Playbook != "" {
		commandArgs = append(commandArgs, options.Playbook)
	}

	if options.Inventory != "" {
		commandArgs = append(commandArgs, []string{"-i", options.Inventory}...)
	}

	commandArgs = append(commandArgs, []string{"-u", options.User}...)

	if options.SSHPass != "" {
		commandArgs = append(commandArgs, options.SSHPass)
	}
	if options.BecomePass != "" {
		commandArgs = append(commandArgs, options.BecomePass)
	}

	if len(options.ExtraVars) > 0 {
		for _, extravar := range options.ExtraVars {
			commandArgs = append(commandArgs, []string{"-e", extravar}...)
		}
	}

	skipTags := []string{"readout-autoconf", "dev", "post-installation"}
	if len(options.Tags) > 0 {
		tags := strings.Join(options.Tags, ",")
		checkTags(&tags, &skipTags)
		commandArgs = append(commandArgs, []string{"-t", tags}...)
	}

	if len(skipTags) > 0 {
		skipTagsString := strings.Join(skipTags, ",")
		commandArgs = append(commandArgs, []string{"--skip-tags", skipTagsString}...)
	}

	ansibleOutput()
	configureAnsible()
	commandName := "ansible-playbook"
	err := runCmd(commandName, commandArgs)
	if err != nil {
		return err
	}
	return err
}

func checkTags(tags *string, skipTags *[]string) {
	isDefault := true
	j := 0
	q := make([]string, len(*skipTags))
	for _, skipTag := range *skipTags {
		if !strings.Contains(*tags, skipTag) {
			q[j] = skipTag
			j++
		} else {
			isDefault = false
		}
	}
	*skipTags = q[:j]
	if isDefault {
		*tags = *tags + ",extra"
	}
}
