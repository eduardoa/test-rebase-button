/*
 * === This file is part of ALICE O² ===
 *
 * Copyright 2019 CERN and copyright holders of ALICE O².
 * Author: Miltiadis Alexis <miltiadis.alexis@cern.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In applying this license CERN does not waive the privileges and
 * immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

package utils

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"

	"github.com/spf13/viper"
)

//Groups of hosts
type Groups map[string][]string

func readInventory() ([]string, error) {

	inventory := viper.GetString("inventory")
	err := checkPath(&inventory)
	if err != nil {
		return nil, err
	}

	b, err := ioutil.ReadFile(filepath.Join(inventory, "hosts"))
	if err != nil {
		return nil, err
	}
	s := string(b[:])

	groups, err := parseString(s)
	if err != nil {
		return nil, err
	}

	hostnames := []string{}
	for _, key := range groups {
		hostnames = append(hostnames, key...)
	}
	viper.Set("inventory", inventory)

	return SliceUniqMapString(hostnames), nil
}

func parseString(s string) (Groups, error) {
	lines := strings.Split(s, "\n")
	groupname := ""
	groups := make(Groups)
	for _, line := range lines {
		orig := line
		if line == "" {
			continue
		}
		if strings.HasPrefix(line, "#") {
			continue
		}
		if strings.HasPrefix(line, "[") && strings.HasSuffix(line, "]") {
			line = strings.TrimRight(line, "]")
			line = strings.TrimLeft(line, "[")
			kv := strings.Split(line, ":")
			if len(kv) > 2 {
				return nil, fmt.Errorf("invalid section: %s", orig)
			}
			groupname = kv[0]
		} else {
			kv := strings.Split(line, " ")
			if val, ok := groups[kv[0]]; ok {
				groups[groupname] = append(groups[groupname], val...)
			} else {
				groups[groupname] = append(groups[groupname], kv[0])
			}
		}
	}
	return groups, nil
}
