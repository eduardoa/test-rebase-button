/*
 * === This file is part of ALICE O² ===
 *
 * Copyright 2019 CERN and copyright holders of ALICE O².
 * Author: Miltiadis Alexis <miltiadis.alexis@cern.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In applying this license CERN does not waive the privileges and
 * immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

package utils

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"strings"

	"github.com/spf13/viper"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/terminal"

	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
	"gopkg.in/src-d/go-git.v4/plumbing/transport"
	gitssh "gopkg.in/src-d/go-git.v4/plumbing/transport/ssh"

	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/transport/http"
)

var gitDir string

// Repo is a representation of the gitlab repository
type Repo struct {
	RemoteURL     string
	LocalPath     string
	ReferenceName string
	Auth          transport.AuthMethod
}

func (r *Repo) exists() bool {
	if _, err := os.Stat(r.LocalPath); err != nil {
		return false
	}
	return true
}

func (r *Repo) clone() error {

	_, err := git.PlainClone(r.LocalPath, false, &git.CloneOptions{
		URL:      r.RemoteURL,
		Auth:     r.Auth,
		Progress: os.Stdout,
	})
	return err

}

// Checkout worktree to referenceName
func (r *Repo) checkout() error {

	repo, err := git.PlainOpen(r.LocalPath)
	if err != nil {
		return err
	}

	w, err := repo.Worktree()
	if err != nil {
		return err
	}

	err = w.Checkout(&git.CheckoutOptions{
		Branch: plumbing.ReferenceName(r.ReferenceName),
		Force:  true,
	})

	if err != nil {
		return err
	}

	return nil
}

func (r *Repo) getLatestTag() error {
	repo, err := git.PlainOpen(r.LocalPath)
	if err != nil {
		return err
	}

	tagRefs, err := repo.Tags()
	if err != nil {
		return err
	}
	var latestTagCommit *object.Commit
	var latestTagName string
	err = tagRefs.ForEach(func(tagRef *plumbing.Reference) error {
		revision := plumbing.Revision(tagRef.Name().String())
		tagCommitHash, err := repo.ResolveRevision(revision)
		if err != nil {
			return err
		}

		commit, err := repo.CommitObject(*tagCommitHash)
		if err != nil {
			return err
		}

		if latestTagCommit == nil {
			latestTagCommit = commit
			latestTagName = tagRef.Name().String()
		}

		if commit.Committer.When.After(latestTagCommit.Committer.When) {
			latestTagCommit = commit
			latestTagName = tagRef.Name().String()
		}

		return nil
	})
	r.ReferenceName = latestTagName

	return nil
}

func (r *Repo) update() error {

	repo, err := git.PlainOpen(r.LocalPath)
	if err != nil {
		return err
	}

	remote, _ := repo.Remote("origin")
	r.RemoteURL = remote.Config().URLs[0]

	if r.RemoteURL == viper.GetString("httpsUrl") {
		r.Auth = passwordAuth()
	} else {
		r.Auth = sshAuth()
	}

	err = repo.Fetch(&git.FetchOptions{
		RemoteName: "origin",
		Auth:       r.Auth,
	})

	if err != nil && err.Error() != "already up-to-date" {
		return err
	}

	//git show-ref
	references, err := repo.References()
	if err != nil {
		return err
	}

	//git tag
	tags, err := repo.Tags()
	var refFound bool

	//prioritize tags
	err = tags.ForEach(func(tag *plumbing.Reference) error {
		if tag.Name().String()[strings.LastIndex(tag.Name().String(), "/")+1:] == r.ReferenceName {
			r.ReferenceName = tag.Name().String()
			refFound = true
			return nil
		}
		return nil
	})

	//If not a referenceTag look for referenceBranch
	if !refFound {
		err = references.ForEach(func(reference *plumbing.Reference) error {
			if reference.Name().String()[strings.LastIndex(reference.Name().String(), "/")+1:] == r.ReferenceName {
				r.ReferenceName = reference.Name().String()
				refFound = true
				return nil
			}
			return nil
		})
	}

	w, err := repo.Worktree()
	if err != nil {
		return err
	}

	revision, err := repo.ResolveRevision(plumbing.Revision(r.ReferenceName))
	if err != nil {
		return err
	}

	err = w.Checkout(&git.CheckoutOptions{
		Hash:  *revision,
		Force: true,
	})
	if err != nil {
		return err
	}

	//reference not found means user provide SHA1
	if !refFound {
		return nil
	}

	// reference as it is stored in the remote only for branches
	if !strings.Contains(r.ReferenceName, "/tags/") {
		reference := r.ReferenceName[strings.LastIndex(r.ReferenceName, "/")+1:]
		r.ReferenceName = fmt.Sprintf("refs/heads/%s", reference)
	} else {
		//There is an "object not found" error for pulling refs/tags
		return nil
	}

	err = w.Pull(&git.PullOptions{
		RemoteName:    "origin",
		ReferenceName: plumbing.ReferenceName(r.ReferenceName),
		Auth:          r.Auth,
	})

	if err != nil && err.Error() != "already up-to-date" {
		return err
	}

	return nil
}

func (r *Repo) resolveShortRevision() (short string) {

	repo, err := git.PlainOpen(r.LocalPath)

	if err != nil {
		return
	}

	h, err := repo.ResolveRevision(plumbing.Revision("HEAD"))
	if err != nil {
		return
	}
	short = h.String()[:7]

	return

}

func sshAuth() transport.AuthMethod {

	s := fmt.Sprintf("%s/.ssh/id_rsa", os.Getenv("HOME"))
	sshKey, _ := ioutil.ReadFile(s)
	signer, _ := ssh.ParsePrivateKey([]byte(sshKey))
	hostKeyCallback := func(hostname string, remote net.Addr, key ssh.PublicKey) error {
		return nil
	}
	auth := &gitssh.PublicKeys{User: "git", Signer: signer, HostKeyCallbackHelper: gitssh.HostKeyCallbackHelper{
		HostKeyCallback: hostKeyCallback,
	}}
	return auth
}

func passwordAuth() transport.AuthMethod {

	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Cannot fetch system-configuration repository via SSH. Your SSH key must be setup with GitLab for this to work. Falling back to HTTPS, you will be prompted for your GitLab credentials.")
	fmt.Printf("\nEnter Username: ")
	username, _ := reader.ReadString('\n')
	fmt.Printf("Enter Password: ")
	bytePassword, err := terminal.ReadPassword(0)
	if err != nil {
		fmt.Println(err.Error())
	}
	password := string(bytePassword)

	auth := &http.BasicAuth{Username: strings.TrimSpace(username), Password: strings.TrimSpace(password)}
	return auth
}
