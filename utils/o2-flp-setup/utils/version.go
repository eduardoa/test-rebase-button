/*
 * === This file is part of ALICE O² ===
 *
 * Copyright 2020 CERN and copyright holders of ALICE O².
 * Author: Miltiadis Alexis <miltiadis.alexis@cern.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In applying this license CERN does not waive the privileges and
 * immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

package utils

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func fillVersionFromVersionFile(versionFilePath string) {
	vfContents, err := ioutil.ReadFile(versionFilePath)
	if err != nil {
		return
	}
	lines := strings.Split(string(vfContents), "\n")

	for _, line := range lines {
		if !strings.HasPrefix(line, "VERSION_") {
			continue
		}
		splitLine := strings.Split(line, ":=")
		if len(splitLine) != 2 {
			return
		}
		switch strings.TrimSpace(splitLine[0]) {
		case "VERSION_MAJOR":
			VersionMajor = strings.TrimSpace(splitLine[1])
		case "VERSION_MINOR":
			VersionMinor = strings.TrimSpace(splitLine[1])
		case "VERSION_PATCH":
			VersionPatch = strings.TrimSpace(splitLine[1])
		}
	}
}

func init() {
	// If the o2-flp-setup was built with go build directly instead of make.
	if VersionMajor == "0" &&
		VersionMinor == "0" &&
		VersionPatch == "0" &&
		Build == "" {

		exPath := getExecutableDir()
		basePath := filepath.Dir(exPath)

		versionFilePath := filepath.Join(exPath, "VERSION")

		if _, err := os.Stat(versionFilePath); err == nil {
			fillVersionFromVersionFile(versionFilePath)

		}
		r := Repo{LocalPath: filepath.Dir(basePath)}
		revisionShort := r.resolveShortRevision()

		timestamp := time.Now().Unix()

		Build = strings.Join([]string{revisionShort, fmt.Sprintf("dev%d", timestamp)}, "-")

	}

	Version = strings.Join([]string{VersionMajor, VersionMinor, VersionPatch}, ".")
	VersionBuild = strings.Join([]string{Version, Build}, "-")
}

var (
	Version      string
	VersionBuild string
	SysConfBuild string
)

var ( // Acquired from -ldflags="-X=..." in Makefile
	VersionMajor = "0"
	VersionMinor = "0"
	VersionPatch = "0"
	Build        = ""
)
