/*
 * === This file is part of ALICE O² ===
 *
 * Copyright 2019 CERN and copyright holders of ALICE O².
 * Author: Miltiadis Alexis <miltiadis.alexis@cern.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In applying this license CERN does not waive the privileges and
 * immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

package utils

import (
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/mitchellh/go-homedir"
	flag "github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gopkg.in/yaml.v3"
)

type groupVars struct {
	ProxyEnv proxyEnv `yaml:"proxy_env"`
}

type proxyEnv map[string]string

func runCmd(commandName string, commandArgs []string) error {
	if viper.GetBool("debug") {
		fmt.Printf("\nRunning: %s %s\n", commandName, strings.Join(commandArgs, " "))
	}
	cmd := exec.Command(commandName, commandArgs...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		return err
	}
	return nil
}

func createInventory() (dir string, err error) {

	dir = viper.GetString("path")
	if dir != "" {
		err = checkPath(&dir)
		if os.IsNotExist(err) {
			err = os.MkdirAll(dir, os.ModePerm)
			if err != nil {
				return
			}
		}
	} else {
		dir, err = ioutil.TempDir("", "ansible_flp_multinode_inventory")
		if err != nil {
			return
		}
	}

	hostfile := filepath.Join(dir, "hosts")

	f, err := os.OpenFile(hostfile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		return
	}

	defer f.Close()

	// Add head host
	_, err = f.WriteString(fmt.Sprintf("[head]\n%s\n\n[flps]\n", viper.GetString("head")))
	if err != nil {
		return
	}

	// Add flps hosts
	for _, node := range viper.GetStringSlice("flps") {
		_, err = f.WriteString(fmt.Sprintf("%s\n", node))
		if err != nil {
			return
		}
	}
	_, err = f.WriteString("\n[all:children]\nhead\nflps\n")
	if err != nil {
		return
	}

	groupvarPath := filepath.Join(dir, "group_vars")
	err = os.MkdirAll(groupvarPath, os.ModePerm)
	if err != nil {
		return
	}

	err = setGroupVars(filepath.Join(groupvarPath, "all"))
	if err != nil {
		return
	}

	err = setGroupVars(filepath.Join(groupvarPath, "head"))
	if err != nil {
		return
	}

	err = setGroupVars(filepath.Join(groupvarPath, "flps"))
	if err != nil {
		return
	}

	return
}

func setGroupVars(filename string) error {
	f, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		return err
	}
	defer f.Close()

	gv := groupVars{}
	gv.ProxyEnv = make(proxyEnv)

	if proxy := viper.GetString("proxy"); proxy != "" {
		gv.ProxyEnv["http_proxy"] = fmt.Sprintf("http://%s", proxy)
		gv.ProxyEnv["https_proxy"] = fmt.Sprintf("http://%s", proxy)
	}

	d, err := yaml.Marshal(&gv)
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(filename, d, 0666)
	if err != nil {
		return err
	}

	return nil
}

// Improve ansible output with unixy
func ansibleOutput() {
	os.Setenv("ANSIBLE_LOAD_CALLBACK_PLUGINS", "1")

	if viper.GetBool("verbose") {
		os.Setenv("ANSIBLE_STDOUT_CALLBACK", "yaml")
	} else if viper.GetBool("debug") {
		os.Setenv("ANSIBLE_STDOUT_CALLBACK", "debug")
	} else if os.Getenv("ANSIBLE_STDOUT_CALLBACK") == "" {
		os.Setenv("ANSIBLE_STDOUT_CALLBACK", "unixy")
	}
}

func configureAnsible() {
	ansibleCache := fmt.Sprintf("%s/.local/share/o2-flp-setup", os.Getenv("HOME"))
	os.MkdirAll(ansibleCache, os.ModePerm)
	os.Setenv("ANSIBLE_HOST_KEY_CHECKING", "False")
	os.Setenv("ANSIBLE_PIPELINING", "True")
	os.Setenv("ANSIBLE_PERSISTENT_CONNECT_TIMEOUT", "180")
	os.Setenv("ANSIBLE_CACHE_PLUGIN", "jsonfile")
	os.Setenv("ANSIBLE_CACHE_PLUGIN_CONNECTION", ansibleCache+"/ansible_cache")
	os.Setenv("ANSIBLE_SSH_CONTROL_PATH_DIR", "/tmp/.ansible/cp")

}

//SliceUniqMapString returns a slice with unique content
func SliceUniqMapString(s []string) []string {
	seen := make(map[string]struct{}, len(s))
	j := 0
	for _, v := range s {
		if _, ok := seen[v]; ok {
			continue
		}
		seen[v] = struct{}{}
		s[j] = v
		j++
	}
	return s[:j]
}

func checkRepo() (err error) {
	repo := Repo{LocalPath: localpath, RemoteURL: viper.GetString("sshUrl"), Auth: sshAuth()}

	if repo.exists() {
		return
	}

	err = repo.clone()

	if err == nil {
		return
	}

	repo.Auth = passwordAuth()
	repo.RemoteURL = viper.GetString("httpsUrl")

	err = repo.clone()
	if err != nil {
		return
	}
	return
}

func getExecutableDir() string {
	ex, err := os.Executable()
	if err != nil {
		return ""
	}
	exPath := filepath.Dir(ex)
	return exPath
}

func checkFlags(f *flag.Flag) {
	switch f.Value.Type() {
	case "bool":
		sval := f.Value.String()
		bval, _ := strconv.ParseBool(sval)
		viper.Set(f.Name, bval)

	case "string":
		sval := f.Value.String()
		viper.Set(f.Name, sval)

	case "stringSlice":
		sval := f.Value.String()
		sval = sval[1 : len(sval)-1]
		stringReader := strings.NewReader(sval)
		csvReader := csv.NewReader(stringReader)
		ssval, _ := csvReader.Read()
		viper.Set(f.Name, ssval)

	}
}

func checkPath(path *string) (err error) {

	if !filepath.IsAbs(*path) && (*path)[0] != '~' {
		return fmt.Errorf("%s is not an absolute path or in home directory", *path)
	}

	*path, err = homedir.Expand(*path) // Resolve ~ into home dir
	if err != nil {
		return
	}

	if _, err = os.Stat(*path); os.IsNotExist(err) {
		return
	}
	return
}

func ensureTrailingComma(hostnames *string) {
	if !strings.HasSuffix(*hostnames, ",") { //Add trailing ','
		*hostnames += ","
	}
}
