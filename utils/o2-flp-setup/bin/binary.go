/*
 * === This file is part of ALICE O² ===
 *
 * Copyright 2020 CERN and copyright holders of ALICE O².
 * Author: Miltiadis Alexis <miltiadis.alexis@cern.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In applying this license CERN does not waive the privileges and
 * immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/google/uuid"
)

var links []string
var s3Link = "https://system-configuration.s3.cern.ch/"

func main() {
	cwd := getWorkingDir()
	macbin := cwd + "/bin/o2-flp-setup.macos"
	linbin := cwd + "/bin/o2-flp-setup.linux"

	id := uuid.New()

	err := uploadtoS3(macbin, id)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	err = uploadtoS3(linbin, id)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	err = writeText(strings.Join(links, "\n"))
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func getWorkingDir() string {
	gwd, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return gwd
}

func uploadtoS3(name string, id uuid.UUID) (err error) {
	file, err := os.Open(name)
	if err != nil {
		return
	}

	defer file.Close()
	a := strings.Split(name, "/")
	key := "o2-flp-setup/" + id.String() + "/" + a[len(a)-1]

	sess := session.Must(session.NewSession(&aws.Config{
		Region:      aws.String("cern"),
		Credentials: credentials.NewStaticCredentials("7850d667218146ba984d20bef1ac7856", "b7da062608b04d2984321eaf8b52ee3f", ""),
	}))

	s3Svc := s3.New(sess, aws.NewConfig().WithEndpoint("https://s3.cern.ch"))

	_, err = s3Svc.PutObject(&s3.PutObjectInput{
		Bucket: aws.String("system-configuration"),
		Key:    aws.String(key),
		Body:   file,
	})
	if err != nil {
		return
	}
	links = append(links, s3Link+key)

	return
}

func writeText(keys string) (err error) {
	cwd := getWorkingDir()

	err = ioutil.WriteFile(cwd+"/binaries.txt", []byte(keys+"\n"), 0666)
	if err != nil {
		return
	}
	return
}
