/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.cern.ch/AliceO2Group/system-configuration/utils/o2-flp-setup/utils"
)

// poweroffCmd represents the poweroff command
var poweroffCmd = &cobra.Command{
	Use:   "poweroff",
	Short: "shutdown",
	Long:  `The poweroff command shuts down the specified hosts.`,
	Run: func(cmd *cobra.Command, args []string) {
		args = []string{"shutdown +1"}
		utils.AdhocOperations(cmd, "command", args)
	},
}

func init() {
	rootCmd.AddCommand(poweroffCmd)
	poweroffCmd.Flags().String("head", "", "host to deploy as head node")
	poweroffCmd.Flags().StringSlice("flps", []string{}, "hosts to deploy as FLPs (comma-separated list of hostnames)")
	poweroffCmd.Flags().String("user", "root", "user")
	poweroffCmd.Flags().Bool("debug", false, "print debug info")
	poweroffCmd.Flags().Bool("verbose", false, "ansible verbose output")
	poweroffCmd.Flags().StringP("inventory", "i", "", "specify inventory directory path")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// poweroffCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// poweroffCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
