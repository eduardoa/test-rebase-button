/*
 * === This file is part of ALICE O² ===
 *
 * Copyright 2020 CERN and copyright holders of ALICE O².
 * Author: Miltiadis Alexis <miltiadis.alexis@cern.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In applying this license CERN does not waive the privileges and
 * immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */
package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.cern.ch/AliceO2Group/system-configuration/utils/o2-flp-setup/utils"
)

// execCmd represents the exec command
var execCmd = &cobra.Command{
	Use:   "exec",
	Short: "execute",
	Long:  `The exec command executes any non-interactive shell command on the specified hosts.`,
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		utils.AdhocOperations(cmd, "command", args)
	},
}

func init() {
	rootCmd.AddCommand(execCmd)
	execCmd.Flags().String("head", "", "host to deploy as head node")
	execCmd.Flags().StringSlice("flps", []string{}, "hosts to deploy as FLPs (comma-separated list of hostnames)")
	execCmd.Flags().String("user", "root", "user")
	execCmd.Flags().Bool("debug", false, "print debug info")
	execCmd.Flags().Bool("verbose", false, "ansible verbose output")
	execCmd.Flags().StringP("inventory", "i", "", "specify inventory directory path")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// execCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// execCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
