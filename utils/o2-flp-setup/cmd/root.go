/*
 * === This file is part of ALICE O² ===
 *
 * Copyright 2019 CERN and copyright holders of ALICE O².
 * Author: Miltiadis Alexis <miltiadis.alexis@cern.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In applying this license CERN does not waive the privileges and
 * immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

package cmd

import (
	"fmt"
	"os"
	"path"

	"github.com/spf13/cobra"
	"gitlab.cern.ch/AliceO2Group/system-configuration/utils/o2-flp-setup/app"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   app.NAME,
	Short: app.PRETTY_SHORTNAME,
	Long: fmt.Sprintf(`%s is a command line utility for O²/FLP Suite deployment.
The following options are always available with any %s command.
For more information on the available commands, run %s help <command name>`, app.NAME, app.NAME, app.NAME),
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
}

func GetRootCmd() *cobra.Command { // Used for docs generator
	return rootCmd
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", fmt.Sprintf("optional configuration file for %s (default $HOME/.config/%s/settings.yaml)", app.NAME, app.NAME))

}

// initConfig reads in config file and ENV variables if set.
func initConfig() {

	viper.SetDefault("sshUrl", "git@gitlab.cern.ch:7999/AliceO2Group/system-configuration.git")
	viper.SetDefault("httpsUrl", "https://gitlab.cern.ch/AliceO2Group/system-configuration.git")
	viper.SetDefault("defaultModules", []string{"qc", "dataflow", "monitoring", "logging", "control", "consul", "extra", "trigger"})
	viper.SetDefault("additionalModules", []string{"dev", "post-installation"})
	viper.SetDefault("verbose", false)
	viper.SetDefault("debug", false)
	viper.SetDefault("user", "root")

	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in .config/o2-flp-setup directory with name "settings.yaml"
		viper.AddConfigPath(path.Join(home, ".config/"+app.NAME))
		viper.SetConfigName("settings")
	}
	viper.SetEnvPrefix("O2_FLP_SETUP") // to be checked
	viper.AutomaticEnv()               // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}

}
