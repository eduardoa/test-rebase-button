/*
 * === This file is part of ALICE O² ===
 *
 * Copyright 2019 CERN and copyright holders of ALICE O².
 * Author: Miltiadis Alexis <miltiadis.alexis@cern.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * In applying this license CERN does not waive the privileges and
 * immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.cern.ch/AliceO2Group/system-configuration/utils/o2-flp-setup/utils"
)

// diagnoseCmd represents the diagnose command
var diagnoseCmd = &cobra.Command{
	Use:     "diagnose",
	Aliases: []string{"diag", "di"},
	Short:   "run deployment diagnostics",
	Long:    `The diagnose command runs deployment diagnostics on the specified hosts.`,
	Run:     utils.Diagnose,
}

func init() {
	rootCmd.AddCommand(diagnoseCmd)
	diagnoseCmd.Flags().String("head", "", "host to deploy as head node")
	diagnoseCmd.Flags().StringSlice("flps", []string{}, "hosts to deploy as FLPs (comma-separated list of hostnames)")
	diagnoseCmd.Flags().String("user", "root", "user")
	diagnoseCmd.Flags().String("proxy", "", "proxy setting")
	diagnoseCmd.Flags().Bool("debug", false, "print debug info")
	diagnoseCmd.Flags().Bool("verbose", false, "ansible verbose output")
	diagnoseCmd.Flags().StringSliceP("extra-vars", "e", []string{}, "set Ansible variables")
	diagnoseCmd.MarkFlagRequired("head")
	diagnoseCmd.MarkFlagRequired("flps")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// diagnoseCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// diagnoseCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
