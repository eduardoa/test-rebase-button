## o2-register-foreman-boot
This tool updates LANDB so that the target host can boot from our O2 Foreman instances.  
It will prompt the user for username/password.

#### Examples
Set LANDB info to boot from our O2 LAB Foreman instance (user will be prompted for the target host)
```
o2-register-foreman-boot
```

Same as before, but providing the target host 
```
o2-register-foreman-boot –s $MACHINENAME
```


Unset LANDB info 
```
o2-register-foreman-boot –r $MACHINENAME
```

