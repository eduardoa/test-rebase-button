# ITS lab Foreman guide

This guide assumes the machines have correctly been set up in LanDB to PXEboot from our Foreman instance. It also assumes that the machines will try to PXEboot by default with no human intervention.

## Provision new machines in ITS lab

1. Boot machines to provision. They will automatically start PXEbooting the Foreman Discovery image.

2. Log into Foreman at https://aidrefsrv20.cern.ch.

3. In Foreman's left side menu, hover on "Hosts" and select "Discovered hosts". A few minutes after booting, the newly discovered machines will appear here. Make sure the location "ITS Lab" is not excluded in the location selector at the top of the page.

![](its_setup/its-newmachine1.png)

4. In this list the machines have a provisional Foreman-assigned hostname based on their MAC addresses, but the correct hostnames will be set upon provisioning. Select the machines to provision, and in the top right menu pick "Auto Provision". You will then be asked to confirm the operation. The selected hosts will disappear from the Discovered list, and the machines will reboot into Kickstart, perform an automated install and reboot again.

![](its_setup/its-newmachine2.png)

![](its_setup/its-newmachine3.png)

5. In Foreman's left side menu, hover on "Hosts" and pick "All hosts". The new machines appear in the list with correct hostnames.

![](its_setup/its-newmachine4.png)

![](its_setup/its-newmachine5.png)

6. For convenience one can filter this list by location (pick "ITS Lab" on top as shown).

![](its_setup/its-newmachine7.png)

7. Clicking on a host in this list reveals more details, including the fact that the Build state is "Pending installation".

![](its_setup/its-newmachine6.png)

8. After Kickstart/Anaconda finishes, Foreman will enqueue and run some Ansible roles. You can see the progress under "Monitor" and then "Jobs".

![](its_setup/its-newmachine8.png)

![](its_setup/its-newmachine9.png)

![](its_setup/its-newmachine10.png)

9. In a few minutes the jobs will run and finish. This will be evident in a few places in the Foreman UI, including the Jobs view ("Job invocations"), the Job details (which should show "100% Success"), and finally the host details (accessible through "All hosts"). These details should include "Build: Installed", "Configuration: Active" and "Execution: Last execution succeeded" as shown below.

![](its_setup/its-newmachine11.png)

![](its_setup/its-newmachine12.png)

![](its_setup/its-newmachine13.png)



## Reinstall existing machines in ITS lab

1. Hover on "Hosts" and then pick "All hosts". The hosts list shows up. Make sure the location filter (top) doesn't exclude "ITS Lab".

![](its_setup/its-newmachine5.png)

2. Select the machines to reinstall, then in the "Select action" menu in the top right pick "Build Hosts" and confirm when prompted. Currently it doesn't matter whether you check "Reboot now" or not, as IPMI integration isn't configured for the ITS lab.

![](its_setup/its-reinstall1.png)

![](its_setup/its-reinstall2.png)

2. The hosts are now in "build mode", which means they are ready to be rebooted into Kickstart. Reboot the machines manually now. Kickstart will perform an unattended install and reboot again into a fresh system.

3. Ansible jobs will then be scheduled and applied, same as for a newly added host (points 6-9 of the previous section).
